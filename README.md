# Backbase Project 

This is a project is created by http://start.backbase.com 

## README files

- [Backbase 6 :: Platform](platform/README.md)
- [CX 6 series without campaigns](cx6/README.md)
- [Backbase 6 :: DBS](dbs/README.md)
- [Digital Sales Flow Catalog](ds-flows/README.md)
- [Backbase 6 :: Statics](statics/README.md)
