import { Component, OnInit } from '@angular/core';
import { TaskDetailsView, TaskInteractorService } from '@backbase/case-management-ui-ang/core';
import { TaskData } from './types';

@Component({
  selector: 'bb-cm-idv-results-widget',
  templateUrl: './cm-idv-results-widget.component.html',
})
export class CmIdvResultsWidgetComponent extends TaskDetailsView implements OnInit {
  taskData: TaskData | any = {};

  readonly loading$ = this.taskInteractor.completeTaskInstanceProcessing$;

  constructor(private readonly taskInteractor: TaskInteractorService) {
    super();
  }

  ngOnInit() {
    this.taskData = this.taskInstance.taskData || {};
  }

  approve() {
    this.updateData('approved');
  }

  reject() {
    this.updateData('fail');
  }

  private updateData(decision: 'fail' | 'approved') {
    this.taskInteractor.completeTask(this.taskInstance.id || '', {
      decision,
    });
  }
}
