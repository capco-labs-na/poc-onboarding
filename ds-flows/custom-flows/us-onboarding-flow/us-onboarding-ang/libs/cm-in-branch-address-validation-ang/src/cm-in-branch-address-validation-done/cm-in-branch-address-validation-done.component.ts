import { Component, OnInit } from '@angular/core';
import { InteractionDetailsView } from '@backbase/case-management-ui-ang/core';

@Component({
  selector: 'bb-us-cm-in-branch-address-validation-step',
  template: `
    <p>Done!<p>
  `,
  styles: [],
})
export class CmInBranchAddressValidationDoneComponent extends InteractionDetailsView implements OnInit {
  constructor() {
    super();
  }

  ngOnInit() {
    this.viewClose.emit();
  }
}
