import { Component } from '@angular/core';
import { InteractionDetailsView } from '@backbase/case-management-ui-ang/core';

@Component({
  selector: 'bb-us-cm-in-branch-address-validation-form-step',
  template: `
    <bb-ono-address-journey-ang [isIDT]="true" header="Where do you live?"></bb-ono-address-journey-ang>
  `,
  styles: [],
})
export class CmInBranchAddressValidationFormComponent extends InteractionDetailsView {
  constructor() {
    super();
  }
}
