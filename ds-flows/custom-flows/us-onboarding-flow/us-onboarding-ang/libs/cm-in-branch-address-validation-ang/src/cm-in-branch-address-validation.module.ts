import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CmInBranchAddressValidationUserTaskComponent } from './cm-in-branch-address-validation-user-task/cm-in-branch-address-validation-user-task.component';
import { IdtTaskViewModule } from '@backbase/ds-shared-ang/ui';
import { CoreAngModule } from '@backbase/case-management-ui-ang/core';
import { CmInBranchAddressValidationFormComponent } from './cm-in-branch-address-validation-form/cm-in-branch-address-validation-form.component';
import { CmInBranchAddressValidationDoneComponent } from './cm-in-branch-address-validation-done/cm-in-branch-address-validation-done.component';
import { OnoAddressJourneyAngModule } from '@backbase/ono-journey-collection-ang/journeys';

@NgModule({
  declarations: [
    CmInBranchAddressValidationUserTaskComponent,
    CmInBranchAddressValidationFormComponent,
    CmInBranchAddressValidationDoneComponent,
  ],
  imports: [
    CommonModule,
    IdtTaskViewModule,
    OnoAddressJourneyAngModule,
    CoreAngModule.withConfig({
      viewMap: {
        'address-validation-idt': CmInBranchAddressValidationUserTaskComponent,
        address: CmInBranchAddressValidationFormComponent,
        done: CmInBranchAddressValidationDoneComponent,
      },
    }),
  ],
})
export class CmInBranchAddressValidationModule {}
