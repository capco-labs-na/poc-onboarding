import { CmNonResidentTaskWidgetComponent } from './cm-non-resident-task-widget.component';
import { FormBuilder } from '@angular/forms';

describe('#CmNonResidentTaskWidgetComponent', () => {
  let component: CmNonResidentTaskWidgetComponent;
  let taskInteractorMock: any;
  let fb: FormBuilder;

  const completeTaskInstance = {
    id: '1',
    taskData: { content: 'content' },
    name: '',
    key: '',
    caseKey: '',
    formKey: '',
    createdOn: '',
    dueDate: '',
  };

  const taskInstanceWithoutTaskData = { ...completeTaskInstance, taskData: {} };
  const taskInstanceWithoutId = { ...completeTaskInstance, id: '' };

  beforeEach(() => {
    taskInteractorMock = {
      completeTask: (decision: boolean) => {},
    };

    fb = new FormBuilder();

    component = new CmNonResidentTaskWidgetComponent(taskInteractorMock, fb);
  });

  it('should assign the task instance', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    expect(component.taskData).toEqual({ content: 'content' });
  });

  it("shouldn't assign the task instance", () => {
    component.taskInstance = taskInstanceWithoutTaskData;
    component.ngOnInit();
    expect(component.taskData).toEqual({});
  });

  it('should complete the task with approved', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.approve();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('1', { approved: true });
  });

  it('should complete the task with fail', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.rejectionNote.setValue('message');
    component.reject('message');
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('1', { approved: false, comment: 'message' });
  });

  it('should complete the task without id', () => {
    component.taskInstance = taskInstanceWithoutId;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.reject();
    expect(taskInteractorMock.completeTask).not.toHaveBeenCalled();
    component.rejectionNote.setValue('foo');
    component.reject('foo');
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('', { approved: false, comment: 'foo' });
    component.approve();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('', { approved: true });
  });
});
