import { Component, OnInit } from '@angular/core';
import { TaskDetailsView, TaskInteractorService } from '@backbase/case-management-ui-ang/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TaskData } from './types';

@Component({
  selector: 'bb-cm-non-resident-task-widget',
  templateUrl: './cm-non-resident-task-widget.component.html',
  styleUrls: ['./cm-non-resident-task-widget.component.scss'],
})
export class CmNonResidentTaskWidgetComponent extends TaskDetailsView implements OnInit {
  taskData: TaskData | any = {};
  readonly loading$ = this.taskInteractor.completeTaskInstanceProcessing$;
  readonly rejectionNote = this.fb.control('', Validators.required);
  readonly rejectionNoteForm = this.fb.group({
    rejectionNote: this.rejectionNote,
  });
  showRejectionNoteForm = false;

  constructor(private readonly taskInteractor: TaskInteractorService, private readonly fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.taskData = this.taskInstance.taskData || {};
  }

  approve() {
    this.updateData(true);
  }

  tryReject() {
    this.showRejectionNoteForm = true;
  }

  reject(note = '') {
    if (this.rejectionNote.invalid) return;
    this.showRejectionNoteForm = false;
    this.updateData(false, note);
  }

  onCloseDialog() {
    this.showRejectionNoteForm = false;
  }

  renderAddress(address: { [key: string]: string }) {
    const sortedWhiteList = ['numberAndStreet', 'city', 'zipCode', 'country'];
    const parts = Object.keys(address)
      .sort((a, b) => sortedWhiteList.indexOf(a) - sortedWhiteList.indexOf(b))
      .map(key => (sortedWhiteList.includes(key) && address[key]) || '')
      .filter(value => !!value)
      .join(', ');

    return address.numberAndStreet ? parts.replace(', ', ',<br>') : parts;
  }

  private updateData(decision: boolean, note = '') {
    const model = decision ? { approved: true } : { approved: false, comment: note };
    this.taskInteractor.completeTask(this.taskInstance.id || '', model);
  }
}
