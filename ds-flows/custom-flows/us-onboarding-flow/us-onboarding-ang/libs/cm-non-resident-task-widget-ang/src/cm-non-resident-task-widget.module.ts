import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { CmNonResidentTaskWidgetComponent } from './cm-non-resident-task-widget.component';
import { CoreAngModule } from '@backbase/case-management-ui-ang/core';
import {
  LoadButtonModule,
  HeaderModule,
  InputValidationMessageModule,
  ModalModule,
  TextareaModule,
} from '@backbase/ui-ang';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CmNonResidentTaskWidgetComponent],
  imports: [
    BackbaseCoreModule.withConfig({
      classMap: { CmNonResidentTaskWidgetComponent },
    }),
    CommonModule,
    CoreAngModule.withConfig({
      viewMap: {
        CmNonResidentTaskWidgetComponent,
      },
    }),
    FormsModule,
    HeaderModule,
    InputValidationMessageModule,
    LoadButtonModule,
    ModalModule,
    ReactiveFormsModule,
    TextareaModule,
  ],
  exports: [CmNonResidentTaskWidgetComponent],
  entryComponents: [CmNonResidentTaskWidgetComponent],
})
export class CmNonResidentTaskWidgetModule {}
