export interface AntiMoneyLaunderingMatch {
  isWhitelisted: boolean;
  entityType: string;
  assets: [
    {
      publicUrl: string;
      source: string;
      type: string;
    }
  ];
  media: [
    {
      date: Date;
      snippet: string;
      title: string;
      url: string;
    }
  ];
  name: string;
  sourceTypes: string[];
  sources: string[];
  matchTypes: string[];
  score: number;
}

export interface AntiMoneyLaunderingInfo {
  matchStatus: string;
  url: string;
  matches: AntiMoneyLaunderingMatch[];
}

export interface TaskData {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  rejectionCode: string;
  antiMoneyLaunderingInfo: AntiMoneyLaunderingInfo;
}
