import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import { PAGE_CONFIG } from '@backbase/foundation-ang/web-sdk';

import { CmStartCaseAngComponent } from './cm-start-case-ang.component';

describe('CmStartCaseAngComponent', () => {
  let component: CmStartCaseAngComponent;
  let fixture: ComponentFixture<CmStartCaseAngComponent>;
  let fakeFlowInteractionService: Pick<FlowInteractionService, 'init' | 'call'>;

  beforeEach(async () => {
    fakeFlowInteractionService = jasmine.createSpyObj('FlowInteractionService', {
      init: jasmine.createSpy('init').and.callThrough(),
      call: jasmine.createSpy('call').and.callThrough(),
    });

    await TestBed.configureTestingModule({
      declarations: [CmStartCaseAngComponent],
      providers: [
        { provide: FlowInteractionService, useValue: fakeFlowInteractionService },
        {
          provide: PAGE_CONFIG,
          useValue: {
            apiRoot: '/gateway/api/',
          },
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(CmStartCaseAngComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.currentStep).toBe('personal-information');
    expect(component.isModalOpen).toBeFalsy();
  });

  it('should initialise the service and open the modal', () => {
    component.openModal();
    expect(fakeFlowInteractionService.init).toHaveBeenCalled();
    expect(component.isModalOpen).toBeTruthy();
  });

  it('should go to next step', () => {
    component.goToStep('select-products');
    expect(component.currentStep).toBe('select-products');
  });

  it('should close the modal', () => {
    component.closeModal();
    expect(component.currentStep).toBe('personal-information');
    expect(component.isModalOpen).toBeFalsy();
  });
});
