import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import { PAGE_CONFIG, PageConfig } from '@backbase/foundation-ang/web-sdk';

@Component({
  selector: 'bb-us-cm-start-case-ang',
  templateUrl: './cm-start-case-ang.component.html',
  styleUrls: ['./cm-start-case-ang.component.scss'],
})
export class CmStartCaseAngComponent {
  currentStep = 'personal-information';
  isModalOpen = false;

  @Input() apiVersion = 'v2';
  @Input() servicePath = 'us-onboarding/client-api/interaction';
  @Input() deploymentPath = 'interactions';
  @Input() interactionName = 'in-branch-onboarding';
  @Input() customClassName = '';

  @Output() caseInstanceKey = new EventEmitter<string>();

  constructor(
    private flowInteractionService: FlowInteractionService,
    @Inject(PAGE_CONFIG) private readonly pageConfig: PageConfig,
  ) {}

  openModal() {
    this.flowInteractionService.init(
      {
        apiVersion: this.apiVersion,
        servicePath: this.servicePath,
        deploymentPath: this.deploymentPath,
        apiRoot: this.pageConfig.apiRoot,
      },
      this.interactionName,
    );

    this.isModalOpen = true;
  }

  goToStep(step: any) {
    if (!step) return;
    this.currentStep = step;
  }

  async finish() {
    const response = await this.flowInteractionService.call({ action: 'submit-in-branch', body: {} }).toPromise();
    if (response && response.actionErrors && response.actionErrors.length) {
      // TODO: error handling -- MT
      return;
    }
    const caseId = response.body.caseKey;
    this.caseInstanceKey.emit(caseId);
    this.closeModal();
  }

  closeModal() {
    this.isModalOpen = false;
    this.currentStep = 'personal-information';
  }
}
