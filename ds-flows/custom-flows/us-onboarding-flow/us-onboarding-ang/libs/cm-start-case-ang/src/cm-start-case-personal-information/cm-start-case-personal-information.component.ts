import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';

import { PersonalInformationFormMetadata } from './personal-information-form';

@Component({
  selector: 'bb-us-cm-start-case-personal-information',
  templateUrl: './cm-start-case-personal-information.component.html',
  styleUrls: ['./cm-start-case-personal-information.component.scss'],
  providers: [DatePipe],
})
export class CmStartCasePersonalInformationComponent implements AfterViewInit {
  form = new FormGroup({});
  model = {
    dateOfBirth: '',
  };
  fields = PersonalInformationFormMetadata;
  @Output() next = new EventEmitter();
  @Output() cancelled = new EventEmitter();

  constructor(private flowInteractionService: FlowInteractionService, private readonly datePipe: DatePipe) {}

  ngAfterViewInit() {
    this.form.markAsUntouched();
  }

  async submit() {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    const payload = {
      ...this.form.value,
      dateOfBirth: this.datePipe.transform(this.form.value.dateOfBirth, 'yyyy-MM-dd'),
    };
    const response = await this.flowInteractionService
      .call({ action: 'submit-personal-info', body: { payload } })
      .toPromise();

    if (response && response.actionErrors && response.actionErrors.length) {
      response.actionErrors.forEach((error: any) => {
        if (error.context) {
          Object.keys(error.context).forEach((key: string) => {
            if (this.form && this.form.controls[key]) {
              this.form.controls[key].setErrors({ flow: error.context[key] });
            }
          });
        }
      });
    } else {
      if (response && response.step) {
        this.next.emit(response.step.name);
      }
    }
  }

  cancel() {
    this.cancelled.emit();
  }
}
