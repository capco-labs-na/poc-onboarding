import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InputType } from '@backbase/ds-shared-ang/ui';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'bb-us-cm-start-case-product-selection',
  templateUrl: './cm-start-case-product-selection.component.html',
  styleUrls: ['./cm-start-case-product-selection.component.scss'],
})
export class CmStartCaseProductSelectionComponent implements OnInit {
  @Output() next = new EventEmitter();
  @Output() cancelled = new EventEmitter();

  fields: FormlyFieldConfig[] = [
    {
      type: InputType.RadioGroup,
      key: 'product',
    },
  ];
  form = new FormGroup({});

  constructor(private flowInteractionService: FlowInteractionService, private changeDetector: ChangeDetectorRef) {}

  async ngOnInit() {
    const response = await this.flowInteractionService.call({ action: 'get-product-list', body: {} }).toPromise();

    if (response && response.actionErrors && response.actionErrors.length) return;

    const options = response.body.map((product: any) => {
      return {
        value: product.referenceId,
        label: product.name,
      };
    });

    if (this.fields && this.fields[0] && this.fields[0].templateOptions) {
      this.fields[0].templateOptions = {
        required: true,
        options: options,
      };
      this.form.setValue({
        product: options[0].value,
      });
      this.changeDetector.detectChanges();
    }
  }

  async submit() {
    if (!this.form.value) return;

    const payload = {
      selection: [{ referenceId: this.form.value.product }],
    };

    const response = await this.flowInteractionService
      .call({ action: 'select-products', body: { payload } })
      .toPromise();

    if (response && response.actionErrors && response.actionErrors.length) {
      // TODO: error handling -- MT
      return;
    }

    this.next.emit();
  }

  cancel() {
    this.cancelled.emit();
  }
}
