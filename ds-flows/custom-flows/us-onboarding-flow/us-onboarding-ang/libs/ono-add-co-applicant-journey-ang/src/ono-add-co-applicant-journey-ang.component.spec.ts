import { OnoAddCoApplicantJourneyAngComponent, ApplicationType } from './ono-add-co-applicant-journey-ang.component';
import { of } from 'rxjs';

describe('#OnoAddCoApplicantJourneyAngComponent', () => {
  let component: OnoAddCoApplicantJourneyAngComponent;
  let flowInteractionMock: {
    call: jasmine.Spy;
    nav: { next: jasmine.Spy };
    steps: jasmine.Spy;
    navigate: jasmine.Spy;
  };

  beforeEach(() => {
    flowInteractionMock = {
      nav: { next: jasmine.createSpy('next') },
      navigate: jasmine.createSpy('navigate').and.returnValue(of({})),
      steps: jasmine.createSpy('steps').and.returnValue(of(undefined)),
      call: jasmine.createSpy('call').and.returnValue(of({})),
    };

    component = new OnoAddCoApplicantJourneyAngComponent(flowInteractionMock as any);
  });

  it('should be initialiced', () => {
    expect(component.applicationTypeEnum).toBeDefined();
    expect(component.applicationType).toBeDefined();
    expect(component.backStep$).toBeDefined();
  });

  it('should navigate to defined step', async () => {
    await component.submitAction(true);
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should set ApplicantTyp to CO_APPLICANT', async () => {
    component.addCoApplicant(ApplicationType.CO_APPLICANT);
    expect(component.applicationType).toEqual(ApplicationType.CO_APPLICANT);
  });

  it('should set ApplicantTyp to SINGLE', async () => {
    component.addCoApplicant(ApplicationType.SINGLE);
    expect(component.applicationType).toEqual(ApplicationType.SINGLE);
  });

  it('should set ApplicantTyp to NotSelected', async () => {
    component.addCoApplicant(ApplicationType.NotSelected);
    expect(component.applicationType).toEqual(ApplicationType.NotSelected);
  });

  it('should not navigate back', async () => {
    await component.navigateTo('back');
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should not navigate back', async () => {
    component.backStep$.subscribe();
    expect(flowInteractionMock.steps).toHaveBeenCalled();
  });
});
