import { Component, Input } from '@angular/core';
import { InteractionResponseDto } from '@backbase/flow-interaction-openapi-data';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export enum ApplicationType {
  NotSelected,
  CO_APPLICANT,
  SINGLE,
}

@Component({
  selector: 'bb-ono-add-co-applicant-journey-ang',
  templateUrl: './ono-add-co-applicant-journey-ang.component.html',
  styleUrls: ['./ono-add-co-applicant-journey-ang.component.scss'],
  providers: [],
})
export class OnoAddCoApplicantJourneyAngComponent {
  @Input() stepName!: string;

  backStep$: Observable<string | undefined>;

  applicationTypeEnum = ApplicationType;
  applicationType = ApplicationType.NotSelected;

  loading$ = new BehaviorSubject(false);

  constructor(private readonly _flowInteraction: FlowInteractionService) {
    this.backStep$ = _flowInteraction.steps().pipe(
      map((steps: InteractionResponseDto['steps']) => {
        return steps ? steps[this.stepName].back : '';
      }),
      catchError(error => {
        return of(undefined);
      }),
    );
  }

  async submitAction(isJointAccount: boolean) {
    this.loading$.next(true);

    const response = await this._flowInteraction
      .call({ action: 'account-type-selector', body: { payload: { isJointAccount } } })
      .toPromise();

    this.loading$.next(false);

    if (!response) {
      this.applicationType = this.applicationTypeEnum.NotSelected;
      return;
    }

    if (response.actionErrors && response.actionErrors.length !== 0) {
      this.applicationType = this.applicationTypeEnum.NotSelected;
      return;
    }

    this._flowInteraction.nav.next();
  }

  async navigateTo(step: string) {
    await this._flowInteraction.navigate(step).toPromise();
    this._flowInteraction.nav.next();
  }

  addCoApplicant(type: ApplicationType) {
    this.applicationType = type;

    switch (type) {
      case ApplicationType.CO_APPLICANT:
        this.submitAction(true);
        break;
      case ApplicationType.SINGLE:
        this.submitAction(false);
        break;
      default:
        this.submitAction(false);
    }
  }
}
