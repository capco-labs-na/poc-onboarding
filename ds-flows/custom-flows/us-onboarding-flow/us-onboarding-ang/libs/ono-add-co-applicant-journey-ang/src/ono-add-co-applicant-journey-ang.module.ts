import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoAddCoApplicantJourneyAngComponent } from './ono-add-co-applicant-journey-ang.component';
import { LoadButtonModule, ButtonModule } from '@backbase/ui-ang';
import { FormlyUiModule } from '@backbase/ds-shared-ang/ui';

@NgModule({
  declarations: [OnoAddCoApplicantJourneyAngComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    ButtonModule,
    FormlyUiModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoAddCoApplicantJourneyAngComponent },
    }),
  ],
})
export class OnoAddCoApplicantJourneyAngModule {}
