import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalStepperModule } from '@backbase/ds-ui-components-ang';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { FormlyUiModule } from '@backbase/ds-shared-ang/ui';
import { ButtonModule, HeaderModule, IconModule, LoadButtonModule } from '@backbase/ui-ang';
import { OnoAnchorJourneyAngComponent } from './ono-anchor-journey-ang.component';

@NgModule({
  declarations: [OnoAnchorJourneyAngComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    IconModule,
    ButtonModule,
    FormlyUiModule,
    ModalStepperModule,
    CdkStepperModule,
    HeaderModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoAnchorJourneyAngComponent },
    }),
  ],
})
export class OnoAnchorJourneyAngModule {}
