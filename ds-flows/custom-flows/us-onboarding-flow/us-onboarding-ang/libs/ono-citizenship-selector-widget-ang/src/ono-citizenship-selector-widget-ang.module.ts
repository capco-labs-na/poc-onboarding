import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnoCitizenshipSelectorWidgetAngComponent } from './ono-citizenship-selector-widget-ang.component';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { ButtonModule, InputRadioGroupModule, InputValidationMessageModule, LoadButtonModule } from '@backbase/ui-ang';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [OnoCitizenshipSelectorWidgetAngComponent],
  imports: [
    CommonModule,
    ButtonModule,
    LoadButtonModule,
    InputRadioGroupModule,
    ReactiveFormsModule,
    InputValidationMessageModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoCitizenshipSelectorWidgetAngComponent },
    }),
  ],
})
export class OnoCitizenshipSelectorWidgetAngModule {}
