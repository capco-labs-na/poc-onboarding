import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { map } from 'rxjs/operators';
import { ContentService } from '@backbase/content-ang';
import { ImageItem } from '@backbase/foundation-ang/web-sdk';

@Component({
  selector: 'bb-us-ono-co-applicant-welcome-journey-ang',
  templateUrl: './ono-co-applicant-welcome-journey-ang.component.html',
  styleUrls: ['./ono-co-applicant-welcome-journey-ang.component.scss'],
  providers: [ContentService],
})
export class OnoCoApplicantWelcomeJourneyAngComponent {
  @Input() getStartedUrl = '';

  coApplicantId = '';
  productImage$ = this.contentService
    .getContent<ImageItem>(`productImage`)
    .pipe(map(image => (image ? image.url : '')));

  applicants$ =
    this.activatedRoute.snapshot.queryParams.coApplicantId &&
    this.flowInteraction
      .call({
        action: 'fetch-co-applicant-data',
        body: { payload: { coApplicantId: this.activatedRoute.snapshot.queryParams.coApplicantId } },
      })
      .pipe(
        map((response: any) => {
          if (!response.body) return undefined;
          this.saveToStorage(this.activatedRoute.snapshot.queryParams.coApplicantId);
          return {
            mainApplicant: response.body.mainApplicant,
            coApplicant: response.body.coApplicant,
          };
        }),
      );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly flowInteraction: FlowInteractionService,
    private readonly contentService: ContentService,
    @Inject(DOCUMENT) private readonly document: Document,
  ) {}

  saveToStorage(coApplicantId: string) {
    sessionStorage.setItem('coApplicantId', coApplicantId);
  }

  goToOnboarding() {
    this.document.location.href = this.getStartedUrl;
  }
}
