import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoCoApplicantWelcomeJourneyAngComponent } from './ono-co-applicant-welcome-journey-ang.component';
import { ButtonModule, HeaderModule, IconModule, CollapsibleModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [OnoCoApplicantWelcomeJourneyAngComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoCoApplicantWelcomeJourneyAngComponent },
    }),
    IconModule,
    ButtonModule,
    HeaderModule,
    CollapsibleModule,
  ],
})
export class OnoCoApplicantWelcomeJourneyAngModule {}
