import { of } from 'rxjs';
import { OnoKycJourneyAngComponent } from './ono-kyc-journey-ang.component';

class FakeHelper {
  mapperFn: any;
  setFieldsConfig() {
    return this;
  }

  setPayloadMapper(mapperFn: any) {
    this.mapperFn = mapperFn;
    return this;
  }

  submit(action: string) {
    return Promise.resolve({});
  }

  setModel() {}
}

describe('#OnoKycJourneyAngComponent', () => {
  let component: OnoKycJourneyAngComponent;
  let flowMock: any;
  let helperMock: FakeHelper;
  let changeDetectionMock: any;
  let helperFactoryMock: any;
  const questionsResponse = {
    body:
      '[  {    "type": "dropdown-single-select",    "key": "occupation",    "templateOptions": {      "label": "Occupation",      "placeholder": "Please select",      "options": [        {          "label": "Clerical",          "value": "Clerical"        },        {          "label": "Consultant",          "value": "Consultant"        },        {          "label": "Government",          "value": "Government"        },        {          "label": "Homemaker",          "value": "Homemaker"        },        {          "label": "Laborer",          "value": "Laborer"        },        {          "label": "Management",          "value": "Management"        },        {          "label": "Medical services",          "value": "Medical services"        },        {          "label": "Military",          "value": "Military"        },        {          "label": "Professional",          "value": "Professional"        },        {          "label": "Retired",          "value": "Retired"        },        {          "label": "Sales",          "value": "Sales"        },        {          "label": "Service",          "value": "Service"        },        {          "label": "Student",          "value": "Student"        },        {          "label": "Teacher",          "value": "Teacher"        },        {          "label": "Technology",          "value": "Technology"        },        {          "label": "Other",          "value": "Other"        }      ]    },    "validators": {      "validation": [        "required","singleOption"      ]    }  },  {    "type": "dropdown-single-select",    "key": "sourceOfIncome",    "templateOptions": {      "label": "Primary source of income",      "placeholder": "Please select",      "options": [        {          "label": "Payroll",          "value": "Payroll"        },        {          "label": "Beneficiary of Insurance",          "value": "Beneficiary of Insurance"        },        {          "label": "Gift / Inheritance",          "value": "Gift / Inheritance"        },        {          "label": "Loan Proceeds",          "value": "Loan Proceeds"        },        {          "label": "Proceeds from investments",          "value": "Proceeds from investments"        },        {          "label": "Rental income",          "value": "Rental income"        },        {          "label": "Sale of real estate",          "value": "Sale of real estate"        },        {          "label": "Savings",          "value": "Savings"        }      ]    },    "validators": {      "validation": [        "required","singleOption"      ]    }  },  {    "type": "dropdown-single-select",    "key": "purpose",    "templateOptions": {      "label": "Purpose of account",      "placeholder": "Please select",      "options": [        {          "label": "Salary and salary compensation",          "value": "Salary and salary compensation"        },        {          "label": "Pension",          "value": "Pension"        },        {          "label": "Scholarships, student work payments",          "value": "Scholarships, student work payments"        },        {          "label": "Pocket money",          "value": "Pocket money"        },        {          "label": "Royalties",          "value": "Royalties"        },        {          "label": "Unemployment benefits",          "value": "Unemployment benefits"        },        {          "label": "Social transfers",          "value": "Social transfers"        },        {          "label": "Foster care, alimony",          "value": "Foster care, alimony"        },        {          "label": "Transfer of funds from other accounts in the United States",          "value": "Transfer of funds from other accounts in the United States"        },        {          "label": "Transfer of funds from other accounts outside the United States",          "value": "Transfer of funds from other accounts outside the United States"        }      ]    },    "validators": {      "validation": [        "required","singleOption"      ]    }  }]',
  };

  beforeEach(() => {
    flowMock = {
      nav: { next: jasmine.createSpy('next') },
      cdo: { get: () => of({}) },
    };
    changeDetectionMock = {
      markForCheck: () => {},
    };
    helperMock = new FakeHelper();
    helperFactoryMock = { getHelper: () => helperMock };
    spyOn(helperMock, 'setFieldsConfig').and.callThrough();
    spyOn(helperMock, 'setPayloadMapper').and.callThrough();

    component = new OnoKycJourneyAngComponent(flowMock, helperFactoryMock, changeDetectionMock);
  });

  it('should init the component', async () => {
    spyOn(helperMock, 'submit').and.returnValue(Promise.resolve(questionsResponse));
    spyOn(flowMock.cdo, 'get').and.returnValue(of([[{ id: '1', answers: ['a'] }]]));
    await component.ngOnInit();
    expect(helperMock.submit).toHaveBeenCalledWith('kyc-questions-loader');
  });

  it('should navigate if no errors', async () => {
    component.helper.submit = jasmine.createSpy('submit').and.returnValue(Promise.resolve({ actionErrors: [] }));
    await component.submitAction();
    expect(component.helper.submit).toHaveBeenCalled();
    expect(flowMock.nav.next).toHaveBeenCalled();
  });

  it('should not navigate if errors', async () => {
    component.helper.submit = jasmine
      .createSpy('submit')
      .and.returnValue(Promise.resolve({ actionErrors: [{ code: 'foo', message: 'bar' }] }));
    await component.submitAction();
    expect(component.helper.submit).toHaveBeenCalled();
    expect(flowMock.nav.next).not.toHaveBeenCalled();
  });

  it('should map response', () => {
    expect(helperMock.mapperFn({ foo: 'bar' })).toEqual({
      answers: [{ id: 'foo', selectedOptions: ['bar'] }],
    });
  });
});
