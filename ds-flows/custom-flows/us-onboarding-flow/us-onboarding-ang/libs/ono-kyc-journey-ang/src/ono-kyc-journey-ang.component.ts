import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormHelperFactory, FormHelper } from '@backbase/ono-common-ang';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'bb-ono-kyc-journey-ang',
  templateUrl: './ono-kyc-journey-ang.component.html',
  styles: [],
  providers: [FormHelperFactory],
})
export class OnoKycJourneyAngComponent implements OnInit {
  helper: FormHelper;
  submitted = false;

  constructor(
    private readonly _flowInteraction: FlowInteractionService,
    helperFactory: FormHelperFactory,
    private readonly _changeDetection: ChangeDetectorRef,
  ) {
    this.helper = helperFactory
      .getHelper()
      .setFieldsConfig([])
      .setPayloadMapper(formValue => this._createActionPayload(formValue));
  }

  async ngOnInit() {
    const formConfig = await this.helper.submit('kyc-questions-loader');
    if (!formConfig) {
      return;
    }

    this.helper.setFieldsConfig(JSON.parse(String(formConfig.body).replace(/\\/g, '')));
    const kycData = await this._flowInteraction.cdo
      .get<any>('kycInformation')
      .pipe(take(1))
      .toPromise();
    if (kycData && kycData.answers) {
      this.helper.setModel(this._createFormModel(kycData.answers));
    }
    this._changeDetection.markForCheck();
  }

  async submitAction() {
    this.submitted = true;
    const response = await this.helper.submit('submit-kyc-data');
    if (response && response.actionErrors && response.actionErrors.length === 0) {
      this._flowInteraction.nav.next();
    }
  }

  private _createActionPayload(formValue: any): any {
    return {
      answers: Object.keys(formValue).map(id => {
        return {
          id,
          selectedOptions: [formValue[id]],
        };
      }),
    };
  }

  private _createFormModel(cdoData: Array<Array<{ id: string; answers: [string] }>>) {
    return cdoData[0].reduce(
      (acc, item) => {
        return {
          ...acc,
          [item.id]: item.answers[0],
        };
      },
      {} as any,
    );
  }
}
