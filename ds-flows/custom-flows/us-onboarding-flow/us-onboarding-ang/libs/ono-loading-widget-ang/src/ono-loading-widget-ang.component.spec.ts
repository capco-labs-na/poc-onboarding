import { of } from 'rxjs';
import { OnoLoadingWidgetAngComponent } from './ono-loading-widget-ang.component';

describe('OnoLoadingWidgetAngComponent', () => {
  let component: OnoLoadingWidgetAngComponent;
  let flowInteractionMock: any;

  beforeEach(() => {
    flowInteractionMock = {
      nav: { next: jasmine.createSpy('next') },
      call: jasmine.createSpy('call'),
    };

    component = new OnoLoadingWidgetAngComponent(flowInteractionMock);
  });

  it('should be correctly initialized', () => {
    expect(component).toBeDefined();
  });

  it('should navigate if no errors', async () => {
    flowInteractionMock.call.and.returnValue(of({ body: {} })), await component.ngOnInit();
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should not navigate if errors', async () => {
    flowInteractionMock.call.and.returnValue(of({ actionErrors: [{ error: {} }] })), await component.ngOnInit();
    expect(flowInteractionMock.nav.next).not.toHaveBeenCalled();
  });
});
