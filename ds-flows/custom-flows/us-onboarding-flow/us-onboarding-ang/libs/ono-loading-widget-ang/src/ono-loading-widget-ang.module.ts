import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoLoadingWidgetAngComponent } from './ono-loading-widget-ang.component';
import { LoadingIndicatorModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [OnoLoadingWidgetAngComponent],
  imports: [
    CommonModule,
    LoadingIndicatorModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoLoadingWidgetAngComponent },
    }),
  ],
})
export class OnoLoadingWidgetAngModule {}
