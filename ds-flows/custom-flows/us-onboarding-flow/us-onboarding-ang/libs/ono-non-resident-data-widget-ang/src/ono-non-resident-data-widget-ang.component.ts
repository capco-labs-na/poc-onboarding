import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'bb-us-ono-non-resident-data-widget-ang',
  templateUrl: './ono-non-resident-data-widget-ang.component.html',
  styleUrls: ['./ono-non-resident-data-widget-ang.component.scss'],
})
export class OnoNonResidentDataWidgetAngComponent implements OnInit {
  countryListArray: { code: string; label: string | unknown }[] = [];

  backStep$?: Observable<string | undefined>;
  loading$ = new BehaviorSubject(false);
  submitted = false;

  form = new FormGroup({
    citizenshipCountryCode: new FormControl('', Validators.required),
    residencyAddress: new FormGroup({
      countryCode: new FormControl('', Validators.required),
      numberAndStreet: new FormControl(''),
      city: new FormControl(''),
      zipCode: new FormControl(''),
    }),
    nationalTin: new FormControl(''),
    foreignTin: new FormControl('', Validators.required),
    withholdingTaxAccepted: new FormControl('', Validators.required),
    w8benAccepted: new FormControl(false, Validators.requiredTrue),
  });

  constructor(private _flowInteraction: FlowInteractionService) {}

  ngOnInit(): void {
    this.backStep$ = this._flowInteraction
      .steps()
      .pipe(map(steps => steps && steps[this._flowInteraction.nav.currentStep.name].back));

    this._flowInteraction
      .call({ action: 'country-list', body: { payload: {} } })
      .toPromise()
      .then(response => {
        if (response && response.body) {
          const countryList = response.body;
          this.countryListArray = Object.entries(countryList).map(([key, value]) => {
            return {
              code: key,
              label: value,
            };
          });
        }
      });
  }

  submitAction() {
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.invalid) return;

    this.loading$.next(true);
    this._flowInteraction
      .call({ action: 'submit-non-resident-data', body: { payload: this.form.value } })
      .toPromise()
      .then(() => this._flowInteraction.nav.next())
      .finally(() => this.loading$.next(false));
  }

  async goBack(step: string) {
    await this._flowInteraction.navigate(step).toPromise();
    this._flowInteraction.nav.next();
  }
}
