import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoNonResidentDataWidgetAngComponent } from './ono-non-resident-data-widget-ang.component';
import {
  ButtonModule,
  DropdownSingleSelectModule,
  HeaderModule,
  InputCheckboxModule,
  InputRadioGroupModule,
  InputTextModule,
  InputValidationMessageModule,
  LoadButtonModule,
} from '@backbase/ui-ang';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [OnoNonResidentDataWidgetAngComponent],
  imports: [
    CommonModule,
    ButtonModule,
    LoadButtonModule,
    DropdownSingleSelectModule,
    ReactiveFormsModule,
    InputTextModule,
    InputValidationMessageModule,
    InputRadioGroupModule,
    InputCheckboxModule,
    HeaderModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoNonResidentDataWidgetAngComponent },
    }),
  ],
})
export class OnoNonResidentDataWidgetAngModule {}
