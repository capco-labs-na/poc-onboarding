import { OnoResponseJourneyAngComponent } from './ono-response-journey-ang.component';
import { of } from 'rxjs';

describe('#OnoResponseJourneyAngComponent', () => {
  let component: OnoResponseJourneyAngComponent;
  let flowMock: any;
  let contentService: any;
  let itemModel: any;

  beforeEach(() => {
    flowMock = {
      call: () => of({}),
      nav: { next: () => Promise.resolve() },
      cdo: { get: () => of({}) },
    };
    contentService = {
      getContent: () => of({}),
    };
    itemModel = {
      property: (property: string) => of(property),
    };
  });

  it('should display first name when the template demands it', () => {
    spyOn(contentService, 'getContent').and.returnValue(of({ content: '<h1>Congrats {firstName}!</h1>' }));
    spyOn(flowMock.cdo, 'get').and.returnValue(of('John'));
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.documentHtml$.subscribe(doc => expect(doc).toBe('<h1>Congrats John!</h1>'));
  });

  it("shouldn't display first name when the template doesn'nt demand it", () => {
    spyOn(contentService, 'getContent').and.returnValue(of({ content: '<h1>Your application is in review.</h1>' }));
    spyOn(flowMock.cdo, 'get').and.returnValue(of('John'));
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.documentHtml$.subscribe(doc => expect(doc).toBe('<h1>Your application is in review.</h1>'));
  });

  it("shouldn't display text when template isn'nt provided", () => {
    spyOn(contentService, 'getContent').and.returnValue(of({}));
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.documentHtml$.subscribe(doc => expect(doc).toBe(''));
  });

  it('should have a primary button if provided', () => {
    spyOn(itemModel, 'property').and.callThrough();
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.primaryButton$.subscribe(button => {
      expect(button).toBeTruthy();
      if (button) {
        expect(button.label).toBe('primaryButtonLabel');
        expect(button.action).toBe('primaryButtonAction');
      }
    });
  });

  it("shouldn't have a primary button if not provided", () => {
    spyOn(itemModel, 'property').and.callFake(() => of(undefined));
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.primaryButton$.subscribe(button => {
      expect(button).toBeUndefined();
    });
  });

  it('should have a secondary button if provided', () => {
    spyOn(itemModel, 'property').and.callThrough();
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.secondaryButton$.subscribe(button => {
      expect(button).toBeTruthy();
      if (button) {
        expect(button.label).toBe('secondaryButtonLabel');
        expect(button.action).toBe('secondaryButtonAction');
      }
    });
  });

  it("shouldn't have a primary button if not provided", () => {
    spyOn(itemModel, 'property').and.callFake(() => of(undefined));
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    component.secondaryButton$.subscribe(button => {
      expect(button).toBeUndefined();
    });
  });

  it('should navigate when action is submitted', async () => {
    spyOn(flowMock, 'call').and.returnValue(of({}));
    spyOn(flowMock.nav, 'next');
    component = new OnoResponseJourneyAngComponent(contentService, flowMock, itemModel);
    const action = 'fake';
    await component.submitAction(action);
    expect(flowMock.call).toHaveBeenCalledWith({ action: action, body: { payload: {} } });
    expect(flowMock.nav.next).toHaveBeenCalled();
  });
});
