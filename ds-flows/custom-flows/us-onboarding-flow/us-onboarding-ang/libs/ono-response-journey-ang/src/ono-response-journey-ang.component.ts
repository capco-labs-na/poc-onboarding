import { Component, Pipe, PipeTransform } from '@angular/core';
import { ContentService } from '@backbase/content-ang';
import { ContentItem } from '@backbase/foundation-ang/web-sdk';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { ItemModel } from '@backbase/foundation-ang/core';

@Pipe({
  name: 'replace',
})
export class ReplacePipe implements PipeTransform {
  transform(value: string, regexValue: string, replaceValue: string): any {
    return value.replace(new RegExp(regexValue, 'g'), replaceValue);
  }
}

@Component({
  selector: 'bb-ono-response-journey-ang',
  templateUrl: './ono-response-journey-ang.component.html',
  providers: [ContentService],
})
export class OnoResponseJourneyAngComponent {
  documentHtml$ = combineLatest([
    this._interactionService.cdo.get<string>('firstName'),
    this._contentService.getContent<ContentItem<any>>('textDocument'),
  ]).pipe(
    map(([firstName, textDocument]) => {
      const documentContent = textDocument ? (textDocument.content as string) : '';
      if (!documentContent) {
        return '';
      }
      return documentContent.replace(' {firstName}', firstName ? ` ${firstName}` : '');
    }),
  );

  readonly primaryButton$ = combineLatest([
    this.itemModel.property<string>('primaryButtonLabel'),
    this.itemModel.property<string>('primaryButtonAction'),
  ]).pipe(map(([label, action]) => (label && action ? { label, action } : undefined)));

  readonly secondaryButton$ = combineLatest([
    this.itemModel.property<string>('secondaryButtonLabel'),
    this.itemModel.property<string>('secondaryButtonAction'),
  ]).pipe(map(([label, action]) => (label && action ? { label, action } : undefined)));

  constructor(
    private readonly _contentService: ContentService,
    private readonly _interactionService: FlowInteractionService,
    private readonly itemModel: ItemModel,
  ) {}

  async submitAction(action: string) {
    await this._interactionService.call({ action: action, body: { payload: {} } }).toPromise();

    this._interactionService.nav.next();
  }
}
