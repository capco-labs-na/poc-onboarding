import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoResponseJourneyAngComponent, ReplacePipe } from './ono-response-journey-ang.component';
import { LoadButtonModule, TabModule, ModalModule, ButtonModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [OnoResponseJourneyAngComponent, ReplacePipe],
  imports: [
    CommonModule,
    LoadButtonModule,
    TabModule,
    ModalModule,
    ButtonModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoResponseJourneyAngComponent },
    }),
  ],
})
export class OnoResponseJourneyAngModule {}
