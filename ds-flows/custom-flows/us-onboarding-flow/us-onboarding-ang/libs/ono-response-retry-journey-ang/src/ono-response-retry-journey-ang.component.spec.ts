import { OnoResponseRetryJourneyAngComponent } from './ono-response-retry-journey-ang.component';
import { of } from 'rxjs';

describe('#OnoResponseRetryJourneyAngComponent', () => {
  let component: OnoResponseRetryJourneyAngComponent;
  let flowMock: any;
  let contentService: any;
  let sanitizerMock: any;
  let changeDetectionMock: any;

  beforeEach(() => {
    contentService = {
      getContent: () => of('<h1>Oops! Something went wrong with your identity verification</h1>'),
    };
    flowMock = {
      call: jasmine.createSpy('call'),
      nav: { next: jasmine.createSpy('next') },
      navigate: () => of({}),
    };
    sanitizerMock = {
      bypassSecurityTrustResourceUrl: jasmine
        .createSpy('bypassSecurityTrustResourceUrl', url => {
          return url;
        })
        .and.callThrough(),
    };
    changeDetectionMock = {
      markForCheck: jasmine.createSpy('markForCheck'),
    };

    component = new OnoResponseRetryJourneyAngComponent(contentService, flowMock, sanitizerMock, changeDetectionMock);
  });

  it('should get the text document', async () => [
    component.textDocument$.subscribe(td => {
      expect(td).toBeTruthy();
      expect(td).toContain('identity');
    }),
  ]);

  it(' should initialize the component', () => {
    expect(component.iFrameSaveUrl).toBe(undefined);
    expect(component.modalIsOpen).toBeFalsy();
    expect(component.transactionReference).toBe('');
  });

  it('should call interaction service', async () => {
    const payload = { action: 'identity-verification-initiation', body: { payload: {} } };
    flowMock.call.and.returnValue(of({ body: { url: 'https://www.url.com' } })), await component.submitAction();
    expect(flowMock.call).toHaveBeenCalledWith(payload);
    expect(sanitizerMock.bypassSecurityTrustResourceUrl).toHaveBeenCalledWith('https://www.url.com');
    expect(component.modalIsOpen).toBeTruthy();
    expect(component.iFrameSaveUrl).toBe('https://www.url.com');
    expect(changeDetectionMock.markForCheck).toHaveBeenCalled();
  });

  it("should 'nt open modal if interaction service doesn'nt return a response", async () => {
    const payload = { action: 'identity-verification-initiation', body: { payload: {} } };
    flowMock.call.and.returnValue(of());
    await component.submitAction();
    expect(flowMock.call).toHaveBeenCalledWith(payload);
    expect(sanitizerMock.bypassSecurityTrustResourceUrl).not.toHaveBeenCalledWith('https://www.url.com');
    expect(component.modalIsOpen).toBeFalsy();
    expect(component.iFrameSaveUrl).toBe(undefined);
    expect(changeDetectionMock.markForCheck).not.toHaveBeenCalled();
  });

  it("shouldn't open modal if interaction service doesn'nt return a response with body", async () => {
    const payload = { action: 'identity-verification-initiation', body: { payload: {} } };
    flowMock.call.and.returnValue(of({ header: 'some header' }));
    await component.submitAction();
    expect(flowMock.call).toHaveBeenCalledWith(payload);
    expect(sanitizerMock.bypassSecurityTrustResourceUrl).not.toHaveBeenCalledWith('https://www.url.com');
    expect(component.modalIsOpen).toBeFalsy();
    expect(component.iFrameSaveUrl).toBe(undefined);
    expect(changeDetectionMock.markForCheck).not.toHaveBeenCalled();
  });

  it('should close modal and navigate if the payload value is success', async () => {
    flowMock.call.and.returnValue(of({}));
    await component.closeModal({ payload: { value: 'success' }, transactionReference: 'fake transaction reference' });
    expect(component.modalIsOpen).toBe(false);
    expect(component.transactionReference).toBe('fake transaction reference');
    expect(changeDetectionMock.markForCheck).toHaveBeenCalled();

    expect(flowMock.call).toHaveBeenCalledWith({
      action: 'identity-verification-result',
      body: { payload: { verificationId: 'fake transaction reference' } },
    });
    expect(flowMock.nav.next).toHaveBeenCalled();
  });

  it("shouldn't close modal if the payload value is not success", async () => {
    component.modalIsOpen = true;
    flowMock.call.and.returnValue(of({}));
    await component.closeModal({ payload: { value: 'fail' }, transactionReference: 'fake transaction reference' });
    expect(component.modalIsOpen).toBe(true);
    expect(component.transactionReference).toBe('');
    expect(changeDetectionMock.markForCheck).not.toHaveBeenCalled();
    expect(flowMock.call).not.toHaveBeenCalledWith({
      action: 'identity-verification-result',
      body: { payload: { verificationId: 'fake transaction reference' } },
    });
    expect(flowMock.nav.next).not.toHaveBeenCalled();
  });

  it("shouldn't navigate if flow interaction service returns error", async () => {
    flowMock.call.and.returnValue(of({ actionErrors: [{ code: '1', message: 'foo' }] }));
    await component.closeModal({ payload: { value: 'success' }, transactionReference: 'fake transaction reference' });
    expect(component.modalIsOpen).toBe(false);
    expect(component.transactionReference).toBe('fake transaction reference');
    expect(changeDetectionMock.markForCheck).toHaveBeenCalled();

    expect(flowMock.call).toHaveBeenCalledWith({
      action: 'identity-verification-result',
      body: { payload: { verificationId: 'fake transaction reference' } },
    });
    expect(flowMock.nav.next).not.toHaveBeenCalled();
  });
});
