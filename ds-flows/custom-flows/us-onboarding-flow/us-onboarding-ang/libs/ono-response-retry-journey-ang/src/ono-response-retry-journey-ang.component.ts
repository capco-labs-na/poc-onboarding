import { ChangeDetectorRef, Component } from '@angular/core';
import { ContentService } from '@backbase/content-ang';
import { ContentItem } from '@backbase/foundation-ang/web-sdk';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'bb-ono-response-retry-journey-ang',
  templateUrl: './ono-response-retry-journey-ang.component.html',
  providers: [ContentService],
})
export class OnoResponseRetryJourneyAngComponent {
  readonly textDocument$ = this._contentService.getContent<ContentItem<any>>('textDocument');

  iFrameSaveUrl: SafeResourceUrl | undefined;
  modalIsOpen = false;
  transactionReference = '';

  constructor(
    private readonly _contentService: ContentService,
    private readonly _interactionService: FlowInteractionService,
    public sanitizer: DomSanitizer,
    private readonly _changeDetection: ChangeDetectorRef,
  ) {}

  async submitAction() {
    await this._interactionService
      .call({ action: 'identity-verification-initiation', body: { payload: {} } })
      .toPromise()
      .then(response => {
        if (!response || !response.body) return;

        this.iFrameSaveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(response.body.url);

        this.modalIsOpen = true;

        this._changeDetection.markForCheck();
      });
  }

  async closeModal(data: any) {
    if (data.payload.value !== 'success') return;

    this.transactionReference = data.transactionReference;
    this.modalIsOpen = false;
    this._changeDetection.markForCheck();

    const payload = { verificationId: this.transactionReference };
    const response = await this._interactionService
      .call({ action: 'identity-verification-result', body: { payload } })
      .toPromise();

    if (!response) {
      return;
    }

    if (response.actionErrors && response.actionErrors.length !== 0) {
      return;
    }

    this._interactionService.nav.next();
  }
}
