import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoResponseRetryJourneyAngComponent } from './ono-response-retry-journey-ang.component';
import { LoadButtonModule, TabModule, ModalModule, ButtonModule } from '@backbase/ui-ang';
import { JumioModalComponent } from './jumio-modal/jumio-modal.component';

@NgModule({
  declarations: [OnoResponseRetryJourneyAngComponent, JumioModalComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    TabModule,
    ModalModule,
    ButtonModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoResponseRetryJourneyAngComponent },
    }),
  ],
})
export class OnoResponseRetryJourneyAngModule {}
