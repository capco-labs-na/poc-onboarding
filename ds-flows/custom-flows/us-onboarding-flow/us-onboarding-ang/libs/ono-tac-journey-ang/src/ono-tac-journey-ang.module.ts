import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { ModalStepperModule } from '@backbase/ds-ui-components-ang';
import { OnoTacJourneyAngComponent } from './ono-tac-journey-ang.component';
import { InputCheckboxModule, IconModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [OnoTacJourneyAngComponent],
  imports: [
    CommonModule,
    ModalStepperModule,
    CdkStepperModule,
    InputCheckboxModule,
    ReactiveFormsModule,
    IconModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoTacJourneyAngComponent },
    }),
  ],
})
export class OnoTacJourneyAngModule {}
