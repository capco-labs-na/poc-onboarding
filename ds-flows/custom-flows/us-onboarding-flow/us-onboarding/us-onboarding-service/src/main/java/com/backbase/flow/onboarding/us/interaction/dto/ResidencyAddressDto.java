package com.backbase.flow.onboarding.us.interaction.dto;

import static com.backbase.flow.onboarding.us.validation.ValidationConstants.FIELD_LENGTH_MESSAGE;
import static com.backbase.flow.onboarding.us.validation.ValidationConstants.RESIDENCY_COUNTRY_CODE_NULL_MESSAGE;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResidencyAddressDto {

    @NotNull(message = RESIDENCY_COUNTRY_CODE_NULL_MESSAGE)
    private String countryCode;

    @Size(max = 100, message = FIELD_LENGTH_MESSAGE)
    private String numberAndStreet;

    @Size(max = 100, message = FIELD_LENGTH_MESSAGE)
    private String city;

    @Size(max = 100, message = FIELD_LENGTH_MESSAGE)
    private String zipCode;
}
