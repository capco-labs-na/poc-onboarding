import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {
  CaseOverviewContainerModule,
  ModalOverlayContainerModule,
  PlatformContainerModule,
  ProcessOverviewContainerModule,
  SidebarOverlayContainerModule,
} from '@backbase/case-management-ui-ang/containers';
import { CoreAngModule } from '@backbase/case-management-ui-ang/core';
import { CustomSessionTimeoutComponent, CustomSessionTimeoutModule } from '@backbase/case-management-ui-ang/session';
import {
  ApplicantsListWidgetModule,
  ApplicantsViewDefinitionConfiguration,
  CaseDataOverviewWidgetModule,
  CaseFilesetListWidgetModule,
  CaseInfoWidgetModule,
  CaseOverviewListWidgetModule,
  CaseTaskListWidgetModule,
  CaseViewDefinitionConfiguration,
  CommentsWidgetModule,
  DmnInsightsWidgetModule,
  EventLogWidgetModule,
  EventsAndTasksWidgetModule,
  HeaderWidgetModule,
  LiveInsightsWidgetModule,
  LogoutWidgetModule,
  ProcessDiagramSelectorWidgetModule,
  ProcessDiagramWidgetModule,
  ProcessInsightsCardWidgetModule,
  StatusTrackingWidgetModule,
  TaskCountSummaryWidgetModule,
  TaskDetailsWidgetModule,
  TaskOverviewListWidgetModule,
  UserDashboardWidgetModule,
} from '@backbase/case-management-ui-ang/widgets';
import { CmAmlResultsWidgetModule } from '@backbase/cm-aml-results-widget';
import { CmBusinessRelationsResultsWidgetModule } from '@backbase/cm-business-relations-results-widget';
import { CmCreateBusinessCaseWidgetModule } from '@backbase/cm-create-business-case-widget';
import { TaskDetailsViewUiModule } from '@backbase/djm-task-details-view-ui';
import { FlowInteractionContainerModule } from '@backbase/flow-interaction-sdk-ang/core';
import { BackbaseCoreModule, SessionTimeoutModule } from '@backbase/foundation-ang/core';
import { WebSdkApiModule } from '@backbase/foundation-ang/web-sdk';
import { ContainersModule } from '@backbase/universal-ang/containers';
import { ContentWidgetModule } from '@backbase/universal-ang/content';
import { HeadingWidgetModule } from '@backbase/universal-ang/heading';
import { LayoutContainerModule } from '@backbase/universal-ang/layouts';
import { NavigationSpaWidgetModule } from '@backbase/universal-ang/navigation';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApplicantsViewDefinition } from '../assets/application-data-view-definition';
import { CaseDataViewDefinition } from '../assets/case-data-view-definition';
import { environment } from '../environments/environment.prod';
import { AppComponent } from './app.component';

const coreModules = [
  BrowserModule,
  BrowserAnimationsModule,
  BackbaseCoreModule.forRoot({
    features: {
      THEME_V2: true,
    },
  }),
  CustomSessionTimeoutModule,
  SessionTimeoutModule.forRoot({
    sessionTimeoutComponentClass: CustomSessionTimeoutComponent,
    inactivityModalTime: 60,
  }),
  WebSdkApiModule,
  CoreAngModule.forRoot(),
  RouterModule.forRoot([], { initialNavigation: 'disabled', useHash: true }),
  NgbModule,
];

const layoutModules = [
  ContainersModule,
  LayoutContainerModule,
  NavigationSpaWidgetModule,
  ContentWidgetModule,
  HeadingWidgetModule,
  FlowInteractionContainerModule,
];

const uiModules = [TaskDetailsViewUiModule];

const userModules = [
  ModalOverlayContainerModule,
  CaseOverviewListWidgetModule,
  CaseOverviewListWidgetModule,
  CaseInfoWidgetModule,
  HeaderWidgetModule,
  CaseTaskListWidgetModule,
  TaskDetailsWidgetModule,
  ProcessDiagramSelectorWidgetModule,
  TaskOverviewListWidgetModule,
  CaseFilesetListWidgetModule,
  EventLogWidgetModule,
  UserDashboardWidgetModule,
  ProcessDiagramSelectorWidgetModule,
  DmnInsightsWidgetModule,
  LiveInsightsWidgetModule,
  TaskCountSummaryWidgetModule,
  ProcessOverviewContainerModule,
  PlatformContainerModule,
  CaseOverviewContainerModule,
  SidebarOverlayContainerModule,
  ProcessInsightsCardWidgetModule,
  EventLogWidgetModule,
  EventsAndTasksWidgetModule,
  ProcessDiagramWidgetModule,
  CommentsWidgetModule,
  LogoutWidgetModule,
  CmAmlResultsWidgetModule,
  CaseDataOverviewWidgetModule,
  StatusTrackingWidgetModule,
  ApplicantsListWidgetModule,
  CmBusinessRelationsResultsWidgetModule,
  CmCreateBusinessCaseWidgetModule,
];

@NgModule({
  declarations: [AppComponent],
  imports: [...coreModules, ...layoutModules, ...userModules, ...uiModules],
  providers: [
    ...(environment.mockProviders || []),
    {
      provide: CaseViewDefinitionConfiguration,
      useValue: CaseDataViewDefinition,
    },
    {
      provide: ApplicantsViewDefinitionConfiguration,
      useValue: ApplicantsViewDefinition,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
