import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { DsBackgroundContainerModule, DsStepContainerModule } from '@backbase/ds-shared-ang/containers';
import {
  FlowInteractionContainerModule,
  FlowInteractionCoreModule,
  ProtectRouteGuard,
} from '@backbase/flow-interaction-sdk-ang/core';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { WebSdkApiModule } from '@backbase/foundation-ang/web-sdk';
import { FormBuilder, OnoCommonAngModule } from '@backbase/ono-journey-collection-ang/common';
import {
  CompanyLookupJourneyAngModule,
  DocumentRequestJourneyAngModule,
  DsBusinessRelationsJourneyAngModule,
  DsSsnJourneyAngModule,
  OnoOtpVerificationJourneyAngModule,
  ProductSelectionJourneyAngModule,
  DsAddressJourneyAngModule,
  OnoIdvJumioJourneyAngModule,
} from '@backbase/ono-journey-collection-ang/journeys';
import {
  DsAnchorJourneyAngModule,
  DsCallActionAngModule,
  DsLandingJourneyAngModule,
} from '@backbase/sme-onboarding-ui-ang/journeys';
// 3rd party and backbase dependencies
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyFormBuilder, FormlyModule } from '@ngx-formly/core';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { bundlesDefinition } from './bundles-definition';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BackbaseCoreModule.forRoot({
      lazyModules: bundlesDefinition,
    }),
    BackbaseCoreModule.withConfig({
      guards: {
        canActivate: { ProtectRouteGuard },
      },
    }),
    FormlyModule.forRoot(),
    RouterModule.forRoot([], { initialNavigation: 'disabled', useHash: true }),
    WebSdkApiModule,
    FlowInteractionCoreModule.forRoot({
      steps: {
        'sme-onboarding-walkthrough': ['sme-onboarding-walkthrough'],
        'sme-onboarding-anchor': ['sme-onboarding-anchor'],
        'otp-verification': ['otp-verification'],
        'sme-onboarding-check-case': ['sme-onboarding-check-case'],
        'select-products': ['select-products'],
        'relation-type': ['business-relations', 'relation-type'],
        'update-current-user-control-person-step': ['business-relations', 'more-about-your-role'],
        'update-current-user-owner-step': ['business-relations', 'more-about-your-role'],
        'business-owners': ['business-relations', 'business-owners'],
        'control-persons': ['business-relations', 'control-persons'],
        'business-summary': ['business-relations', 'business-summary'],
        'business-structure': ['company-lookup', 'business-structure'],
        'document-request-journey': ['document-request-journey'],
        'company-lookup': ['company-lookup', 'company-lookup'],
        'business-details': ['company-lookup', 'business-details'],
        'business-address': ['company-lookup', 'business-address'],
        'business-identity': ['company-lookup', 'business-identity'],
        'identity-verified': ['identity-verified'],
        'identity-verification-initiation': ['identity-verification-initiation'],
        'check-business-relation-and-document-request': ['check-business-relation-and-document-request'],
        'sme-onboarding-personal-address': ['sme-onboarding-personal-address'],
        'sme-onboarding-ssn': ['sme-onboarding-ssn'],
        'sme-onboarding-landing': ['done'],
      },
      actionConfig: {
        'sme-onboarding-init': { updateCdo: true },
        'sme-onboarding-anchor-data': { updateCdo: true },
        'request-otp': { updateCdo: true },
        'verify-otp': { updateCdo: true },
        'sme-onboarding-check-case-exist': { updateCdo: true },
        'get-product-list': { updateCdo: true },
        'select-products': { updateCdo: true },
        'relation-type': { updateCdo: true },
        'update-current-user-control-person-step': { updateCdo: true },
        'update-current-user-owner-step': { updateCdo: true },
        'business-owners': { updateCdo: true },
        'control-persons': { updateCdo: true },
        'business-summary': { updateCdo: true },
        'business-structure': {},
        'company-lookup': {},
        'identity-verification-initiation': { updateCdo: false },
        'identity-verification-result': { updateCdo: false },
        'business-details': {},
        'business-address': { updateCdo: true },
        'business-identity': {},
        'load-document-requests': {},
        'load-document-request': {},
        'accept-terms-conditions': { updateCdo: true },
        'upload-document': {},
        'download-document': {},
        'delete-temp-document': {},
        'complete-task': { updateCdo: true },
        'submit-document-requests': { updateCdo: true },
        'sme-onboarding-personal-address': { updateCdo: true },
        'submit-ssn': { updateCdo: true },
        'sme-onboarding-landing-data': {},
        'check-business-relation-and-document-requests-conditions': {},
      },
    }),
    FlowInteractionContainerModule,
    DsAnchorJourneyAngModule,
    NgbModule,
    OnoCommonAngModule,
    DsStepContainerModule.forRoot([
      {
        label: 'Personal Details',
        name: 'sme-onboarding-walkthrough',
      },
      {
        label: 'Account verification',
        name: 'otp-verification',
      },
      {
        label: 'Choose product',
        name: 'product-selection-list',
      },
      {
        label: 'Company lookup',
        name: 'your-business',
      },
      {
        label: 'Structure',
        name: 'business-structure',
        isSubStep: true,
      },
      {
        label: 'Find your business',
        name: 'company-lookup',
        isSubStep: true,
      },
      {
        label: 'Details',
        name: 'business-details',
        isSubStep: true,
      },
      {
        label: 'Address',
        name: 'business-address',
        isSubStep: true,
      },
      {
        label: 'Identity',
        name: 'business-identity',
        isSubStep: true,
      },
      {
        label: 'Business Relations',
        name: 'business-relations',
      },
      {
        label: 'Your role',
        name: 'relation-type',
        isSubStep: true,
      },
      {
        label: 'More about your role as owner',
        name: 'update-current-user-owner-step',
        isSubStep: true,
      },
      {
        label: 'More about your role as control person',
        name: 'update-current-user-control-person-step',
        isSubStep: true,
      },
      {
        label: 'Business owners',
        name: 'business-owners',
        isSubStep: true,
      },
      {
        label: 'Control person',
        name: 'control-persons',
        isSubStep: true,
      },
      {
        label: 'Summary',
        name: 'business-summary',
        isSubStep: true,
      },
      {
        label: 'Upload documents',
        name: 'document-request-journey',
      },
      {
        label: 'ID protection',
        name: 'identity-verification-initiation',
      },
      {
        label: 'Home address',
        name: 'sme-onboarding-personal-address',
      },
      {
        label: 'Your SSN',
        name: 'sme-onboarding-ssn',
      },
    ]),
    OnoOtpVerificationJourneyAngModule,
    DsCallActionAngModule,
    DsAddressJourneyAngModule,
    DsSsnJourneyAngModule,
    DsLandingJourneyAngModule,
    DocumentRequestJourneyAngModule,
    DsBackgroundContainerModule,
    ProductSelectionJourneyAngModule,
    DsBusinessRelationsJourneyAngModule,
    CompanyLookupJourneyAngModule,
    OnoIdvJumioJourneyAngModule,
  ],
  declarations: [AppComponent],
  providers: [
    ...(environment.mockProviders || []),
    {
      provide: FormBuilder,
      useClass: FormlyFormBuilder,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
