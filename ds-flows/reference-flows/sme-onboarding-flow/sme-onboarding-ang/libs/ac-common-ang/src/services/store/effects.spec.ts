import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Action } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { hot, cold, getTestScheduler } from 'jasmine-marbles';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { ApplicationCenterStoreEffects } from './effects';

/**
 * Instance factories
 */
const anHttpResponseError = (message: string = 'httpError') => new HttpErrorResponse({ error: new Error(message) });
/**
 * Store mocks
 */
const initialState = {
  applicationCenter: {
    entities: {},
    actions: {},
  },
};
let actions$: Observable<Action>;

describe('Application Center store effects', () => {
  let interactionService: jasmine.SpyObj<FlowInteractionService>;
  let effects: ApplicationCenterStoreEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ApplicationCenterStoreEffects,
        provideMockStore({ initialState }),
        provideMockActions(() => actions$),
        {
          provide: FlowInteractionService,
          useValue: jasmine.createSpyObj('flowInteractionService', ['call']),
        },
      ],
    });
    effects = TestBed.inject(ApplicationCenterStoreEffects);
    interactionService = TestBed.inject(FlowInteractionService) as jasmine.SpyObj<FlowInteractionService>;
  });

  describe('loadDocumentRequestListEffect$', () => {
    it('should dispatch the success action when request completes', () => {
      // given
      const expected = cold('--a', {
        a: {
          type: '[Application Center] Load Document Request List Success',
          documentRequestList: [],
          customer: { userId: 'someId', firstName: 'fName', lastName: 'lName', fullName: 'fName lName' },
        },
      });
      interactionService.call.and.returnValue(
        cold('-a|', {
          a: {
            body: {
              documentRequests: [],
              customer: { userId: 'someId', firstName: 'fName', lastName: 'lName' },
            },
          },
        }),
      );
      // then
      actions$ = hot('-a-', {
        a: {
          type: '[Application Center] Load Document Request List',
        },
      });
      // expect
      expect(
        effects.loadDocumentRequestListEffect$({
          requestDelay: 10,
          scheduler: getTestScheduler(),
        }),
      ).toBeObservable(expected);
    });

    it('should dispatch the error action when request fails', () => {
      // given
      const httpError = anHttpResponseError('error');
      const expected = cold('--a', {
        a: {
          type: '[Application Center] Load Document Request List Error',
          httpError,
        },
      });
      interactionService.call.and.returnValue(throwError(httpError));
      // then
      actions$ = hot('-a-', {
        a: {
          type: '[Application Center] Load Document Request List',
        },
      });
      // expect
      expect(
        effects.loadDocumentRequestListEffect$({
          requestDelay: 10,
          scheduler: getTestScheduler(),
        }),
      ).toBeObservable(expected);
    });
  });

  describe('loadDocumentRequest$', () => {
    it('should dispatch the success action when request completes', () => {
      // given
      const expected = cold('--a', {
        a: {
          type: '[Application Center] Load Document Request Success',
          internalId: 'someId',
          comments: [],
          fileset: { id: 'someId' },
        },
      });
      interactionService.call.and.returnValue(
        cold('-a|', {
          a: {
            body: {
              comments: [],
              fileset: { id: 'someId' },
            },
          },
        }),
      );
      // then
      actions$ = hot('-a-', {
        a: {
          type: '[Application Center] Load Document Request',
          internalId: 'someId',
        },
      });
      // expect
      expect(
        effects.loadDocumentRequest$({
          requestDelay: 10,
          scheduler: getTestScheduler(),
        }),
      ).toBeObservable(expected);
    });

    it('should dispatch the error action when request fails', () => {
      // given
      const httpError = anHttpResponseError('error');
      const expected = cold('--a', {
        a: {
          type: '[Application Center] Load Document Request Error',
          httpError,
        },
      });
      interactionService.call.and.returnValue(throwError(httpError));
      // then
      actions$ = hot('-a-', {
        a: {
          type: '[Application Center] Load Document Request',
          internalId: 'someId',
        },
      });
      // expect
      expect(
        effects.loadDocumentRequest$({
          requestDelay: 10,
          scheduler: getTestScheduler(),
        }),
      ).toBeObservable(expected);
    });
  });
});
