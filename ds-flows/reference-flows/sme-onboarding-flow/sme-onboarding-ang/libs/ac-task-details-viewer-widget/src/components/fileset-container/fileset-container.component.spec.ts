import { HttpErrorResponse } from '@angular/common/http';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  AcActionService,
  DocumentRequest,
  DocumentRequestActionsState,
  DocumentRequestStatus,
  Fileset,
} from '@backbase/ac-common-ang';
import { BehaviorSubject, of } from 'rxjs';
import { FilesetContainerComponent } from './fileset-container.component';

const fileset$$ = new BehaviorSubject<Fileset>({
  id: 'someId',
  name: 'someName',
  createdBy: 'someName',
  createdAt: new Date().toISOString(),
  lastModifiedAt: new Date().toISOString(),
  files: [],
  allowedMediaTypes: [],
  maxFiles: 3,
});
const documentRequestActions$$ = new BehaviorSubject<DocumentRequestActionsState>({
  loading: { processing: false, httpError: undefined, success: false },
});
const documentRequest$$ = new BehaviorSubject<DocumentRequest>({
  internalId: 'someId',
} as DocumentRequest);

class AcActionServiceStub implements Partial<AcActionService> {
  readonly fileset$ = fileset$$;
  readonly fileDataActionsGroup$ = of({});
  readonly documentRequestActions$ = documentRequestActions$$;
  readonly documentRequest$ = documentRequest$$;

  uploadFiles = jasmine.createSpy('uploadFiles');
  deleteFile = jasmine.createSpy('deleteFile');
  downloadFile = jasmine.createSpy('downloadFile');
}

describe('FilesetContainerComponent', () => {
  let component: FilesetContainerComponent;
  let fixture: ComponentFixture<FilesetContainerComponent>;
  let acActionService: Partial<AcActionService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilesetContainerComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: AcActionService,
          useClass: AcActionServiceStub,
        },
      ],
    });

    fixture = TestBed.createComponent(FilesetContainerComponent);
    acActionService = TestBed.inject(AcActionService);
    component = fixture.componentInstance;
    component.documentRequest = {
      internalId: 'someId',
      status: DocumentRequestStatus.Open,
    } as DocumentRequest;
  });

  describe('#onDelete', () => {
    it('should delete a file when confirmed', () => {
      const deletionPendingFile = 'someFilename';
      fixture.detectChanges();
      component.deleteFile(deletionPendingFile);

      expect(acActionService.deleteFile).toHaveBeenCalled();
    });
  });

  describe('DOM error state', () => {
    const errorStateIdentifier = '*[data-role="fileset-details-http-error-state"]';

    it('should show an error state when the document request loading has failed', () => {
      documentRequestActions$$.next({
        loading: {
          processing: false,
          success: false,
          httpError: new HttpErrorResponse({ status: 500 }),
        },
      });
      fixture.detectChanges();
      const erroStateElem: DebugElement = fixture.debugElement.query(By.css(errorStateIdentifier));

      expect(erroStateElem).toBeTruthy();
    });
  });

  describe('DOM loading state', () => {
    const loadingStateIdentifier = '*[data-role="fileset-details-loading-state"]';

    it('should show the loading state when the fileset details loading request is in process', () => {
      documentRequestActions$$.next({
        loading: {
          processing: true,
          httpError: undefined,
          success: false,
        },
      });
      fixture.detectChanges();
      const loadingStateElem: DebugElement = fixture.debugElement.query(By.css(loadingStateIdentifier));

      expect(loadingStateElem).toBeTruthy();
    });
  });

  describe('DOM loaded state', () => {
    const loadedStateIdentifier = 'bb-fileset-view[data-role="fileset-details-loaded-state"]';

    it('should show the loaded state when the filesets loading request has succeded', () => {
      fileset$$.next({
        id: 'someId',
        name: 'someName',
        createdBy: 'someName',
        createdAt: new Date().toISOString(),
        lastModifiedAt: new Date().toISOString(),
        files: [],
        allowedMediaTypes: [],
        maxFiles: 3,
      });
      documentRequestActions$$.next({
        loading: {
          processing: false,
          httpError: undefined,
          success: true,
        },
      });
      fixture.detectChanges();
      const loadedStateElem: DebugElement = fixture.debugElement.query(By.css(loadedStateIdentifier));

      expect(loadedStateElem).toBeTruthy();
    });
  });
});
