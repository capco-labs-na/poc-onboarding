import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CaseDefinitionsService } from '@backbase/flow-casedata-openapi-data';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { PAGE_CONFIG } from '@backbase/foundation-ang/web-sdk';
import {
  BadgeModule,
  ButtonModule,
  HeaderModule,
  IconModule,
  ModalModule,
  NotificationService,
} from '@backbase/ui-ang';
import { of } from 'rxjs';
import { CmCreateBusinessCaseWidgetComponent } from './cm-create-business-case-widget.component';

class CaseDefinitionsServiceStub {}

const flowInteractionServiceMock: jasmine.SpyObj<any> = {
  call: jasmine.createSpy('call').and.returnValue(
    of({
      data: {
        test: 'test',
      },
    }),
  ),
  init: () => {},
  getCollection: jasmine.createSpy('getCollection').and.returnValue(
    of({
      items: [
        {
          name: 'pl',
          isoCode: 'pl',
          states: [],
        },
        {
          name: 'US',
          isoCode: 'US',
          states: [
            {
              name: 'Arizona',
              isoCode: 'US-AZ',
              code: 'AZ',
            },
          ],
        },
      ],
    }),
  ),
};

const notoficationServiceMock: jasmine.SpyObj<any> = {
  showNotification: jasmine.createSpy('showNotification').and.returnValue(of('test')),
};

describe('CmCreateBusinessCaseWidgetComponent', () => {
  let component: CmCreateBusinessCaseWidgetComponent;
  let fixture: ComponentFixture<CmCreateBusinessCaseWidgetComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [ModalModule, ButtonModule, HeaderModule, BadgeModule, IconModule],
        declarations: [CmCreateBusinessCaseWidgetComponent, CmCreateBusinessCaseWidgetComponent],
        providers: [
          {
            provide: CaseDefinitionsService,
            useClass: CaseDefinitionsServiceStub,
          },
          {
            provide: FlowInteractionService,
            useValue: flowInteractionServiceMock,
          },
          {
            provide: NotificationService,
            useValue: notoficationServiceMock,
          },
          {
            provide: PAGE_CONFIG,
            useValue: {
              apiRoot: '/gateway/api/',
            },
          },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CmCreateBusinessCaseWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('openCreateNewCaseModal should update isCreateNewCaseModalOpen value', () => {
    const openCancelRequestValue = true;
    component.openCreateNewCaseModal(openCancelRequestValue);

    component.isCreateNewCaseModalOpen$.subscribe((value) => {
      expect(value).toBeTruthy();
    });
  });

  it('goToCreatedCase should emit caseKey', () => {
    spyOn(component.caseInstanceKey, 'emit');

    component.goToCreatedCase('test');

    expect(component.caseInstanceKey.emit).toHaveBeenCalled();
  });

  it('goToStep should return undefined if there is no step', () => {
    const result = component.goToStep('');

    expect(result).toBe(undefined);
  });

  it('goToStep should update current step', () => {
    component.goToStep('test');

    expect(component.currentStep).toBe('test');
  });

  it('createNewCase should trigger interactionService init and open modal', () => {
    spyOn(component, 'openCreateNewCaseModal');
    component.createNewCase();

    expect(component.openCreateNewCaseModal).toHaveBeenCalledWith(true);
  });

  it('getCaseKey should update caseKey', () => {
    component.getCaseKey('test');

    expect(component.caseKey).toEqual('test');
  });

  it('finish should open modal and trigger notification', () => {
    spyOn(component, 'openCreateNewCaseModal');
    component.finish();

    expect(component.openCreateNewCaseModal).toHaveBeenCalledWith(false);
  });
});
