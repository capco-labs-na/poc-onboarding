import { Component, EventEmitter, Inject, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { PageConfig, PAGE_CONFIG } from '@backbase/foundation-ang/web-sdk';
import { BehaviorSubject, Observable } from 'rxjs';
import { CmCreateBusinessCaseWidgetService } from './cm-create-business-case-widget.service';
import { State } from './lib/model/country.model';
@Component({
  selector: 'bb-cm-create-business-case-widget',
  templateUrl: './cm-create-business-case-widget.component.html',
  styles: [],
})
export class CmCreateBusinessCaseWidgetComponent implements OnInit {
  @Input() apiVersion = 'v2';
  @Input() servicePath = 'sme-onboarding/client-api/interaction';
  @Input() deploymentPath = 'interactions';
  @Input() interactionName = 'in-branch-onboarding-start';
  /**
   * Emit key of a selected case instance (WA3 navigation event)
   */
  @Output() caseInstanceKey = new EventEmitter<string>();
  /**
   * Notification success template
   */
  @ViewChild('notificationSuccessTpl', { static: true })
  notificationSuccessTplRef: TemplateRef<any> | undefined;

  caseKey?: string;
  usaStates$?: Observable<State[]>;
  currentStep = 'business-informations';

  private isCreateNewCaseModalOpen = new BehaviorSubject<boolean>(false);
  isCreateNewCaseModalOpen$ = this.isCreateNewCaseModalOpen.asObservable();

  constructor(
    private readonly _flowInteractionService: FlowInteractionService,
    private readonly _widgetService: CmCreateBusinessCaseWidgetService,
    @Inject(PAGE_CONFIG) private readonly pageConfig: PageConfig,
  ) {}

  ngOnInit() {
    this.usaStates$ = this._widgetService.getUsaStates();
  }

  createNewCase() {
    this._flowInteractionService.init(
      {
        apiVersion: this.apiVersion,
        servicePath: this.servicePath,
        deploymentPath: this.deploymentPath,
        apiRoot: this.pageConfig.apiRoot,
      },
      this.interactionName,
    );

    this.openCreateNewCaseModal(true);
  }

  openCreateNewCaseModal(value: boolean) {
    this.isCreateNewCaseModalOpen.next(value);
  }

  finish() {
    this.openCreateNewCaseModal(false);
    this._widgetService.pushNotification(this.notificationSuccessTplRef, 'success');
  }

  getCaseKey(caseKey: string) {
    this.caseKey = caseKey;
  }

  goToCreatedCase(caseKey: string) {
    this.caseInstanceKey.emit(caseKey);
  }

  goToStep(step: string) {
    if (!step) return;
    this.currentStep = step;
  }
}
