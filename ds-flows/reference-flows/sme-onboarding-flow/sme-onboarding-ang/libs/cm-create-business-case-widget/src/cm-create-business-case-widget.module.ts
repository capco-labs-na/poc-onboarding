import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import {
  ButtonModule,
  DropdownSingleSelectModule,
  HeaderModule,
  IconModule,
  InputRadioGroupModule,
  InputTextModule,
  InputValidationMessageModule,
  ModalModule,
} from '@backbase/ui-ang';
import { CmCreateBusinessCaseWidgetComponent } from './cm-create-business-case-widget.component';
import { BusinessInformationsModalComponent } from './lib/business-informations-modal/business-informations-modal.component';
import { ProductSelectionModalComponent } from './lib/product-selection-modal/product-selection-modal.component';

const layoutModules = [
  ModalModule,
  ButtonModule,
  HeaderModule,
  IconModule,
  ReactiveFormsModule,
  InputTextModule,
  InputRadioGroupModule,
  DropdownSingleSelectModule,
  InputValidationMessageModule,
];

const userComponents = [
  CmCreateBusinessCaseWidgetComponent,
  BusinessInformationsModalComponent,
  ProductSelectionModalComponent,
];

@NgModule({
  declarations: [...userComponents],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { CmCreateBusinessCaseWidgetComponent },
    }),
    ...layoutModules,
  ],
})
export class CmCreateBusinessCaseWidgetModule {}
