import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import {
  ButtonModule,
  DropdownSingleSelectModule,
  InputTextModule,
  InputValidationMessageModule,
  ModalModule,
} from '@backbase/ui-ang';
import { of } from 'rxjs';
import { CmCreateBusinessCaseWidgetService } from '../../cm-create-business-case-widget.service';
import { BusinessInformationsModalComponent } from './business-informations-modal.component';

const flowInteractionServiceMock: jasmine.SpyObj<any> = {
  nav: {
    currentStep: { name: 'create-case' },
    nextStep: { name: 'select-products' },
    steps: ['create-case', 'company-lookup'],
    next: jasmine.createSpy(),
  },
  steps: jasmine.createSpy('steps').and.returnValue(
    of({
      'create-case': { back: 'done' },
      'company-lookup': { back: 'done' },
    }),
  ),
  call: jasmine.createSpy().and.returnValue(
    of({
      steps: {
        'create-case': { back: 'done' },
        'company-lookup': { back: 'create-case' },
      },
    }),
  ),
  navigate: jasmine.createSpy().and.returnValue(of({ body: [] })),
};

describe('BusinessInformationsModalComponent', () => {
  let component: BusinessInformationsModalComponent;
  let fixture: ComponentFixture<BusinessInformationsModalComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          ModalModule,
          ButtonModule,
          DropdownSingleSelectModule,
          InputTextModule,
          InputValidationMessageModule,
          ReactiveFormsModule,
        ],
        declarations: [BusinessInformationsModalComponent],
        providers: [
          {
            provide: FlowInteractionService,
            useValue: flowInteractionServiceMock,
          },
          CmCreateBusinessCaseWidgetService,
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessInformationsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onCloseDialod should close the modal', () => {
    spyOn(component.cancel, 'emit');
    component.openDialog = true;
    component.onCloseDialog();
    expect(component.cancel.emit).toHaveBeenCalled();
    expect(component.openDialog).toBeFalsy();
  });

  it('onConfirmDialog should return undefined if form is not valid', () => {
    component.businessInformationsForm.controls.email.setErrors({ flow: true });
    const result = component.onConfirmDialog();

    expect(result).toEqual(undefined);
  });

  it('onConfirmDialog should not close modal if form is not valid', () => {
    spyOn(component, 'onCloseDialog');
    component.onConfirmDialog();
    expect(component.onCloseDialog).not.toHaveBeenCalled();
  });

  it('allowToSubmit should return false if button should be disabled', () => {
    const result = component.allowToSubmit();

    expect(result).toBeFalse();
  });
});
