import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { Subscription } from 'rxjs';
import { CmCreateBusinessCaseWidgetService } from '../../cm-create-business-case-widget.service';
import { State } from '../model/country.model';

@Component({
  selector: 'bb-cm-business-informations-modal',
  templateUrl: 'business-informations-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusinessInformationsModalComponent implements OnInit, OnDestroy {
  submitted = false;

  @Input() openDialog = true;
  @Input() usaStates?: State[];
  @Output() goForward = new EventEmitter<string>();
  @Output() cancel = new EventEmitter();
  @Output() caseKey = new EventEmitter<string>();

  private readonly subs = new Subscription();

  readonly firstName = new FormControl('', [Validators.required]);
  readonly lastName = new FormControl('', [Validators.required]);
  readonly businessName = new FormControl('', [Validators.required]);
  readonly email = new FormControl('', [Validators.required]);
  readonly numberAndStreet = new FormControl('', [Validators.required]);
  readonly apt = new FormControl('');
  readonly city = new FormControl('', [Validators.required]);
  readonly state = new FormControl('', [Validators.required]);
  readonly zipCode = new FormControl('', [Validators.required]);
  readonly businessInformationsForm = new FormGroup({
    firstName: this.firstName,
    lastName: this.lastName,
    businessName: this.businessName,
    email: this.email,
    numberAndStreet: this.numberAndStreet,
    apt: this.apt,
    city: this.city,
    state: this.state,
    zipCode: this.zipCode,
  });

  constructor(
    private readonly _flowInteractionService: FlowInteractionService,
    private readonly _widgetService: CmCreateBusinessCaseWidgetService,
    private readonly cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    const formData = this._widgetService.getBusinessInformationsFormValue();
    if (formData) {
      this.businessInformationsForm.setValue(formData);
    }
  }

  onCloseDialog() {
    this.openDialog = false;
    this.cancel.emit();
    this._widgetService.setBusinessInformationsFormValue(undefined);
  }

  allowToSubmit(): boolean {
    return !this.businessInformationsForm.valid && this.submitted;
  }

  onConfirmDialog() {
    this.submitted = true;
    this.businessInformationsForm.markAllAsTouched();
    if (!this.businessInformationsForm.valid) {
      return;
    }

    const formValue = this.businessInformationsForm.value;

    this.subs.add(
      this._flowInteractionService
        .call({
          action: 'in-branch-onboarding-start',
          body: {
            payload: formValue,
          },
        })
        .subscribe((data) => {
          if (data && data.actionErrors && data.actionErrors.length) {
            data.actionErrors.forEach((error: any) => {
              if (error.context) {
                Object.keys(error.context).forEach((key: string) => {
                  if (this.businessInformationsForm && this.businessInformationsForm.controls[key]) {
                    this.businessInformationsForm.controls[key].setErrors({ flow: error.context[key] });
                    this.cd.detectChanges();
                  }
                });
              }
            });

            return;
          }
          this.caseKey.emit(data.body.caseKey);
          this._widgetService.setBusinessInformationsFormValue(formValue);
          this.goForward.emit('product-selection');
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
