export interface businessInformations {
  firstName: string;
  lastName: string;
  businessName: string;
  email: string;
  numberAndStreet: string;
  apt?: string;
  city: string;
  state: string;
  zipCode: string;
}
