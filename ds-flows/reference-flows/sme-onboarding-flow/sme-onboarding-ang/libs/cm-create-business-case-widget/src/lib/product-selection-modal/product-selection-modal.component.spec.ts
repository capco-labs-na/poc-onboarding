import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import {
  ButtonModule,
  DropdownSingleSelectModule,
  InputRadioGroupModule,
  InputTextModule,
  InputValidationMessageModule,
  ModalModule,
} from '@backbase/ui-ang';
import { of } from 'rxjs';
import { ProductSelectionModalComponent } from './product-selection-modal.component';

const flowInteractionServiceMock: jasmine.SpyObj<any> = {
  nav: {
    currentStep: { name: 'create-case' },
    nextStep: { name: 'select-products' },
    steps: ['create-case', 'company-lookup'],
    next: jasmine.createSpy(),
  },
  steps: jasmine.createSpy('steps').and.returnValue(
    of({
      'create-case': { back: 'done' },
      'company-lookup': { back: 'done' },
    }),
  ),
  call: jasmine.createSpy().and.returnValue(
    of({
      steps: {
        'create-case': { back: 'done' },
        'company-lookup': { back: 'create-case' },
      },
      data: {},
    }),
  ),
  navigate: jasmine.createSpy().and.returnValue(of({ body: [] })),
};

describe('ProductSelectionModalComponent', () => {
  let component: ProductSelectionModalComponent;
  let fixture: ComponentFixture<ProductSelectionModalComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          ModalModule,
          ButtonModule,
          DropdownSingleSelectModule,
          InputTextModule,
          InputValidationMessageModule,
          ReactiveFormsModule,
          InputRadioGroupModule,
        ],
        declarations: [ProductSelectionModalComponent],
        providers: [
          {
            provide: FlowInteractionService,
            useValue: flowInteractionServiceMock,
          },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSelectionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onCloseDialog should emit cancel, trigger onGoBack function', () => {
    spyOn(component.cancel, 'emit');
    spyOn(component, 'onGoBack');

    component.onCloseDialog();

    expect(component.cancel.emit).toHaveBeenCalled();
    expect(component.openDialog).toBeFalsy();
    expect(component.onGoBack).toHaveBeenCalled();
  });

  it('onGoBack should emit goBack', () => {
    spyOn(component.goBack, 'emit');

    component.onGoBack();

    expect(component.goBack.emit).toHaveBeenCalledWith('business-informations');
  });

  it('onConfirmDialog should return undefined if form is not valid', () => {
    const result = component.onConfirmDialog();

    expect(result).toEqual(undefined);
  });

  it('onConfirmDialog should call _flowInteractionService if form is valid', () => {
    component.productSelectionForm.setValue({
      product: 'test',
    });
    component.onConfirmDialog();

    expect(flowInteractionServiceMock.call).toHaveBeenCalled();
  });
});
