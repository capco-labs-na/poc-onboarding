import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { LoadButtonModule, IconModule, ButtonModule, HeaderModule, InputCheckboxModule } from '@backbase/ui-ang';
import { ModalStepperModule, FormlyUiModule } from '@backbase/ds-shared-ang/ui';

import { Step1Component } from './steps/step1/step1.component';
import { Step2Component } from './steps/step2/step2.component';
import { Step3Component } from './steps/step3/step3.component';
import { Step4Component } from './steps/step4/step4.component';
import { TermsAndConditionsStepComponent } from './steps/terms-and-conditions-step/terms-and-conditions-step.component';
import { DsAnchorJourneyAngComponent } from './ds-anchor-journey-ang.component';

const uiComps = [HeaderModule, LoadButtonModule, IconModule, ButtonModule, InputCheckboxModule];

@NgModule({
  imports: [
    CommonModule,
    ...uiComps,
    CdkStepperModule,
    ModalStepperModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyUiModule,
    BackbaseCoreModule.withConfig({
      classMap: { DsAnchorJourneyAngComponent },
    }),
  ],
  declarations: [
    DsAnchorJourneyAngComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    TermsAndConditionsStepComponent,
  ],
  exports: [DsAnchorJourneyAngComponent],
})
export class DsAnchorJourneyAngModule {}
