import { Component, EventEmitter, NO_ERRORS_SCHEMA, Output } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ContentService } from '@backbase/content-ang';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { FormHelper, FormHelperFactory } from '@backbase/ono-common-ang';
import { of } from 'rxjs';
import { DsAnchorJourneyAngComponent } from './ds-anchor-journey-ang.component';

@Component({
  selector: 'bb-modal-stepper',
  template: '',
})
class ModalStepperMockComponent {
  @Output() readonly complete = new EventEmitter<void>();
}

describe('DsAnchorJourneyAngComponent', () => {
  let component: DsAnchorJourneyAngComponent;
  let fixture: ComponentFixture<DsAnchorJourneyAngComponent>;

  let formHelper: jasmine.SpyObj<FormHelper>;
  let helperFactory: jasmine.SpyObj<FormHelperFactory>;
  let contentService: jasmine.SpyObj<ContentService>;
  let flowInteractionService: FlowInteractionService;

  beforeEach(() => {
    formHelper = jasmine.createSpyObj('FormHelper', {
      setFieldsConfig: undefined,
      setModel: undefined,
      setPayloadMapper: undefined,
      submit: Promise.resolve(),
    });
    formHelper.setFieldsConfig.and.callFake(() => formHelper);
    formHelper.setModel.and.callFake(() => formHelper);
    formHelper.setPayloadMapper.and.callFake(() => formHelper);

    helperFactory = jasmine.createSpyObj('FormHelperFactory', {
      getHelper: formHelper,
    });

    contentService = jasmine.createSpyObj({
      getContent: of({ content: 'some content' }),
    });

    flowInteractionService = {
      cdo: {
        get: () => of(),
      },
    } as unknown as typeof flowInteractionService;

    fixture = TestBed.configureTestingModule({
      declarations: [DsAnchorJourneyAngComponent, ModalStepperMockComponent],
      providers: [{ provide: FlowInteractionService, useValue: flowInteractionService }],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .overrideProvider(FormHelperFactory, { useValue: helperFactory })
      .overrideProvider(ContentService, { useValue: contentService })
      .createComponent(DsAnchorJourneyAngComponent);

    component = fixture.componentInstance;
  });

  describe('upon modal completion', () => {
    let modal: ModalStepperMockComponent;

    beforeEach(() => {
      spyOn(console, 'error');

      modal = fixture.debugElement.query(By.directive(ModalStepperMockComponent)).componentInstance;
    });

    it('should submit data to "sme-onboarding-init" handler', () => {
      modal.complete.emit();

      expect(formHelper.submit).toHaveBeenCalledWith('sme-onboarding-init');
    });

    it('should not log an error submit if submittion succeeded', fakeAsync(() => {
      modal.complete.emit();

      tick();

      expect(console.error).not.toHaveBeenCalled();
    }));

    it('should log an error if submittion failed', fakeAsync(() => {
      const expected = new Error(`SME Onboarding init step error: some error message`);
      const errorResponse: any = {
        actionErrors: [{ code: 'some-error-code', message: 'some error message' }],
      };

      formHelper.submit.and.returnValue(Promise.resolve(errorResponse));

      modal.complete.emit();

      tick();

      expect(console.error).toHaveBeenCalledWith(expected);
    }));
  });
});
