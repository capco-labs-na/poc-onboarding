import { Component, Input, OnChanges, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ItemModel } from '@backbase/foundation-ang/core';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'bb-terms-and-conditions-step',
  templateUrl: 'terms-and-conditions-step.component.html',
  styleUrls: ['terms-and-conditions-step.component.scss'],
})
export class TermsAndConditionsStepComponent implements OnChanges {
  @Input() complete = false;
  readonly confirmCheckbox = new FormControl(this.complete);
  @Output() readonly completeChange = this.confirmCheckbox.valueChanges;

  readonly links$ = combineLatest([
    this.widgetModel.properties.pipe(map((props) => props.tncTermsConditionsLinkText)),
    this.widgetModel.properties.pipe(map((props) => props.tncTermsConditionsLinkUrl)),
    this.widgetModel.properties.pipe(map((props) => props.tncPrivacyStatementLinkText)),
    this.widgetModel.properties.pipe(map((props) => props.tncPrivacyStatementLinkUrl)),
  ]).pipe(
    map(([tncLinkText, tncLink, privacyLinkText, privacyLink]) => ({
      tncLinkText,
      tncLink,
      privacyLinkText,
      privacyLink,
    })),
  );

  constructor(private readonly widgetModel: ItemModel) {}

  ngOnChanges(): void {
    this.confirmCheckbox.setValue(this.complete);
  }
}
