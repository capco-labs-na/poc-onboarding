# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2.0.1 [Unreleased]

### Changed

- linter and karma coverage

## 2.0.0

### Changed

- Angular 11 upgrade

## 1.0.1

### Fixed

- run unit tests without transpilling all ts files

## 1.0.0

### Added

- widget created
