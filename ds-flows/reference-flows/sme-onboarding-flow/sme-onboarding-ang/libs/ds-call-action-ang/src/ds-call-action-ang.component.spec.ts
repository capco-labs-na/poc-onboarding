import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { FormHelperFactory } from '@backbase/ono-common-ang';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { DsCallActionAngComponent } from '@backbase/ds-call-action-ang';
import { ItemModel } from '@backbase/foundation-ang/core';

describe('DsCallActionAngComponent', () => {
  const createComponent = (actionName: string) => {
    const interactionServiceMock = {
      nav: jasmine.createSpyObj(['next']),
    };
    const model = jasmine.createSpyObj({
      property: of(actionName),
    });

    const formHelperMock = {
      setPayloadMapper: () => formHelperMock,
      submit: jasmine.createSpy(),
    };

    const fixture = TestBed.configureTestingModule({
      declarations: [DsCallActionAngComponent],
      providers: [
        { provide: FlowInteractionService, useValue: interactionServiceMock },
        { provide: ItemModel, useValue: model },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .overrideComponent(DsCallActionAngComponent, {
        set: {
          providers: [{ provide: FormHelperFactory, useValue: { getHelper: () => formHelperMock } }],
        },
      })
      .createComponent(DsCallActionAngComponent);

    fixture.detectChanges();

    return { interactionServiceMock, formHelperMock };
  };

  it('should call an action hanlder', fakeAsync(() => {
    const { formHelperMock } = createComponent('some-action-name');

    tick();

    expect(formHelperMock.submit).toHaveBeenCalledWith('some-action-name');
  }));

  it('should navigate next after successfully calling of the action handler', fakeAsync(() => {
    const { interactionServiceMock, formHelperMock } = createComponent('some-action-name');

    tick();

    expect(formHelperMock.submit).toHaveBeenCalledBefore(interactionServiceMock.nav.next);
    expect(interactionServiceMock.nav.next).toHaveBeenCalled();
  }));

  it('should print an error to the console if an action handler is not set', fakeAsync(() => {
    spyOn(console, 'error');

    createComponent('');

    tick();

    expect(console.error).toHaveBeenCalledWith('"action" property must be set');
  }));
});
