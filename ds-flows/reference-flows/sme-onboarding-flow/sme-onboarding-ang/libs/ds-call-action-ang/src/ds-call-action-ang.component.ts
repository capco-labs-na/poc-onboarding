import { Component, OnInit } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { ItemModel } from '@backbase/foundation-ang/core';
import { FormHelperFactory } from '@backbase/ono-common-ang';
import { take } from 'rxjs/operators';

@Component({
  selector: 'bb-ds-call-action-ang',
  template: `
    <bb-loading-indicator-ui data-role="check-existing-case-loading" loaderSize="lg"></bb-loading-indicator-ui>
  `,
  providers: [FormHelperFactory],
})
export class DsCallActionAngComponent implements OnInit {
  private readonly actionNamePromise = this.model.property<string>('action').pipe(take(1)).toPromise();
  private readonly formHelper = this.helperFactory.getHelper().setPayloadMapper(() => ({}));

  constructor(
    private readonly interactionService: FlowInteractionService,
    private helperFactory: FormHelperFactory,
    private readonly model: ItemModel,
  ) {}

  async ngOnInit(): Promise<void> {
    const actionName = await this.actionNamePromise;

    if (!actionName) {
      console.error('"action" property must be set');

      return;
    }

    await this.formHelper.submit(actionName);
    this.interactionService.nav.next();
  }
}
