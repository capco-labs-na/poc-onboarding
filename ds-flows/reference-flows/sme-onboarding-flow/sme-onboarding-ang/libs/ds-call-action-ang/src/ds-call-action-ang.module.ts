import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { LoadingIndicatorModule } from '@backbase/ui-ang';
import { DsCallActionAngComponent } from './ds-call-action-ang.component';

@NgModule({
  declarations: [DsCallActionAngComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { DsCallActionAngComponent },
    }),
    LoadingIndicatorModule,
  ],
})
export class DsCallActionAngModule {}
