# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.1 [Unreleased]

### Changed

- linter and karma coverage

## 1.0.0

### Changed

- Angular 11 upgrade
- Interaction SDK upgrade to 3.0.0