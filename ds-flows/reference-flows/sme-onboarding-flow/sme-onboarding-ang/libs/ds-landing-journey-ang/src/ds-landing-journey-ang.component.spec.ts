import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { ContentService } from '@backbase/content-ang';
import { DsLandingJourneyAngComponent } from './ds-landing-journey-ang.component';
import { ItemModel } from '@backbase/foundation-ang/core';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';

describe('DsLandingJourneyAngComponent', () => {
  let fixture: ComponentFixture<DsLandingJourneyAngComponent>;
  let component: DsLandingJourneyAngComponent;

  let content$: BehaviorSubject<{ content: string } | undefined>;
  let linkNavigateTo$: Subject<string>;
  let handlerResponse$: Subject<{ body?: { email: string } }>;

  beforeEach(() => {
    content$ = new BehaviorSubject<{ content: string } | undefined>({ content: 'some content and {email}' });
    linkNavigateTo$ = new Subject<string>();
    handlerResponse$ = new BehaviorSubject<{ body?: { email: string } }>({ body: { email: 'some@email.com' } });

    const contentService = jasmine.createSpyObj('ContentService', {
      getContent: content$,
    });
    const itemModel = jasmine.createSpyObj('ItemModel', {
      property: linkNavigateTo$,
    });
    const flowInteractionService = {
      call: () => handlerResponse$,
    };

    fixture = TestBed.configureTestingModule({
      declarations: [DsLandingJourneyAngComponent],
      providers: [
        { provide: ItemModel, useValue: itemModel },
        { provide: FlowInteractionService, useValue: flowInteractionService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .overrideProvider(ContentService, { useValue: contentService })
      .createComponent(DsLandingJourneyAngComponent);

    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('textDocument', () => {
    it('should be shown', () => {
      expect(fixture.nativeElement.textContent).toBe('some content and some@email.com');
    });

    describe('if contentService returns undefined', () => {
      it('should not cause an error', () => {
        expect(() => content$.next(undefined)).not.toThrow();
      });

      it('should display an empty string', () => {
        content$.next(undefined);

        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('');
      });
    });

    describe('if contentService fails', () => {
      it('should not cause an error', () => {
        expect(() => content$.error(new Error('some error'))).not.toThrow();
      });

      it('should display an empty string', () => {
        content$.error(new Error('some error'));

        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('');
      });
    });

    describe('if the handler fails', () => {
      it('should not cause an error', () => {
        expect(() => handlerResponse$.error(new Error('some error'))).not.toThrow();
      });

      it('should display the content without email', () => {
        handlerResponse$.error(new Error('some error'));

        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('some content and the email you provided');
      });
    });

    describe('if the handler does not return body', () => {
      it('should not cause an error', () => {
        expect(() => handlerResponse$.next({})).not.toThrow();
      });

      it('should display the content without email', () => {
        handlerResponse$.next({});

        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('some content and the email you provided');
      });
    });
  });

  describe('button', () => {
    describe('if linkNavigateTo input is not set', () => {
      it('should be hidden', () => {
        const button = fixture.nativeElement.querySelector('bb-load-button-ui');

        expect(button).toBeNull();
      });
    });

    describe('if linkNavigateTo input is set', () => {
      let button: HTMLElement;

      beforeEach(() => {
        linkNavigateTo$.next('/some-link');

        fixture.detectChanges();

        button = fixture.nativeElement.querySelector('bb-load-button-ui');
      });

      it('should be visible', () => {
        expect(button).not.toBeNull();
      });

      it('should navigate to the provided link', () => {
        const location = jasmine.createSpyObj('Location', ['assign']);

        // The simplest and working way to mock location
        // it's impossible to mock Angular's DOCUMENT (error _doc.querySelectorAll is not a function)
        // it's impossible to spy on Location.assign
        // it's impossible to use @ng-web-apis/common's WINDOW since ngc can't find Window type
        (component as any).document = { location };

        button.click();

        expect(location.assign).toHaveBeenCalledWith('/some-link?id=');
      });
    });
  });
});
