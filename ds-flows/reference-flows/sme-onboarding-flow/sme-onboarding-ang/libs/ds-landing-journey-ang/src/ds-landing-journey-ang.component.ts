import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ContentService } from '@backbase/content-ang';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import { ItemModel } from '@backbase/foundation-ang/core';
import { ContentItem } from '@backbase/foundation-ang/web-sdk';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, map, publishReplay, refCount, takeUntil } from 'rxjs/operators';

interface LandingJourneyCDO {
  email: string;
  caseId: string;
}

@Component({
  selector: 'bb-ds-landing-journey-ang',
  templateUrl: 'ds-landing-journey-ang.component.html',
  styleUrls: ['ds-landing-journey-ang.component.scss'],
  providers: [ContentService],
})
export class DsLandingJourneyAngComponent implements OnInit, OnDestroy {
  readonly linkNavigateTo$ = this.model.property<string>('linkNavigateTo');
  private userLandingCaseData$ = this.fetchUserLandingCaseData();
  private caseId = '';
  readonly documentHtml$ = combineLatest([this.fetchContent(), this.userLandingCaseData$]).pipe(
    map(([content, caseData]) => content.replace('{email}', caseData?.email || 'the email you provided')),
  );
  private onDestroy$ = new Subject();

  constructor(
    private readonly contentService: ContentService,
    private readonly model: ItemModel,
    private readonly flowInteractionService: FlowInteractionService,
    // Metadata can't contain ambient type
    // The same approach can be found in Angular sources
    // https://github.com/angular/angular/blob/master/packages/common/src/location/platform_location.ts#L115
    @Inject(DOCUMENT) private readonly document: any,
  ) {}

  ngOnInit(): void {
    // Get Case Id
    this.userLandingCaseData$.pipe(takeUntil(this.onDestroy$)).subscribe({
      next: (data) => {
        this.caseId = data?.caseId || '';
      },
    });
  }

  onButtonClick(linkNavigateTo: string): void {
    this.document.location.assign(`${linkNavigateTo}?id=${this.caseId}`);
  }

  private fetchContent(): Observable<string> {
    return this.contentService.getContent<ContentItem<string>>('textDocument').pipe(
      map((x) => x?.content || ''),
      catchError(() => of('')),
    );
  }

  private fetchUserLandingCaseData(): Observable<LandingJourneyCDO | undefined> {
    return this.flowInteractionService
      .call({
        action: 'sme-onboarding-landing-data',
        body: {},
      })
      .pipe(
        map((response) => response.body),
        publishReplay(1),
        refCount(),
        catchError(() => of(undefined)),
      );
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    // Clearing the userCaseData
    this.userLandingCaseData$ = of(undefined);
  }
}
