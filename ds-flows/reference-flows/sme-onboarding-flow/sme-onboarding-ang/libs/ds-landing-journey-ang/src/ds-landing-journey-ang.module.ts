import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { DsLandingJourneyAngComponent } from './ds-landing-journey-ang.component';
import { LoadButtonModule, TabModule, ModalModule, ButtonModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [DsLandingJourneyAngComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    TabModule,
    ModalModule,
    ButtonModule,
    BackbaseCoreModule.withConfig({
      classMap: { DsLandingJourneyAngComponent },
    }),
  ],
  exports: [DsLandingJourneyAngComponent],
})
export class DsLandingJourneyAngModule {}
