== SME Onboarding

== Run service locally without external dependencies
=== Prerequisites:

* Java 11
* Maven

=== Run service
Run the following command inside the path `sme-onboarding-service`.

[source]
----
mvn clean spring-boot:run -Dspring-boot.run.profiles=local
----

== Postman collections formatting

To reduce the risk of potential git merge conflicts and to make the review process of changed Postman collection
files easier the JSON files has to be formatted before committing them. To check if the Postman collection is
properly formatted before commit one may use `mvn formatter:validate` goal which will fail with appropriate message
if some file(s) needs to be formatted. If so, running `mvn formatter:format` goal will perform the formatting.

NOTE: If the changed Postman collection file will not be formatted as described and committed as is the CI build will
most likely fail during `mvn verify` phase.
