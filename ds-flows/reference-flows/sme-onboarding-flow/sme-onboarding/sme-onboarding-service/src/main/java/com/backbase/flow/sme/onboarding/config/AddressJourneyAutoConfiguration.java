package com.backbase.flow.sme.onboarding.config;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.mapper.JourneyMapper;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import java.util.List;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AddressJourneyAutoConfiguration {

    @SuppressWarnings("rawtypes")
    @Bean
    public JourneyMapper<Map> addressValidationJourneyMapper(
        List<JourneyWriter<Map>> writerList, List<JourneyReader<Map>> readerList, CaseDataService caseDataService
    ) {
        return new JourneyMapper<>(Map.class, readerList, writerList, caseDataService);
    }
}
