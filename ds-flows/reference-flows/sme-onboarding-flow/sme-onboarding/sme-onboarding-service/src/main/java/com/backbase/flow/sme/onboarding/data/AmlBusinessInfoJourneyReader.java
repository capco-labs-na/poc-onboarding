package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmlBusinessInfoJourneyReader implements JourneyReader<AntiMoneyLaunderingInfo> {

    private final DataMapper dataMapper;

    @Override
    public AntiMoneyLaunderingInfo read(String subject, String context, Case caseInstance) {
        return dataMapper.toAntiMoneyLaunderingInfo(
            caseInstance.getCaseData(SmeCaseDefinition.class).getCompanyLookupInfo().getBusinessDetailsInfo());
    }

    @Override
    public boolean canApply(String context, Case singleCase) {
        return ProcessConstants.BPM_AML_TYPE_BUSINESS.equalsIgnoreCase(context);
    }
}
