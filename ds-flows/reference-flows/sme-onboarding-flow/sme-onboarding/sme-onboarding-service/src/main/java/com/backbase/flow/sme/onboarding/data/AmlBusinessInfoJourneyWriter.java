package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmlBusinessInfoJourneyWriter implements JourneyWriter<AntiMoneyLaunderingInfo> {

    private final DataMapper dataMapper;
    private final CaseDataService caseDataService;

    @Override
    public Case write(AntiMoneyLaunderingInfo amlInfo, String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);

        caseDefinition.getCompanyLookupInfo().getBusinessDetailsInfo()
            .setAntiMoneyLaunderingInfo(dataMapper.toAmlInfo(amlInfo));

        caseInstance.setCaseData(caseDefinition);

        return caseDataService.updateCase(caseInstance);
    }

    @Override
    public boolean canApply(String context, Case singleCase) {
        return ProcessConstants.BPM_AML_TYPE_BUSINESS.equalsIgnoreCase(context);
    }
}
