package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.service.aml.casedata.AmlPersonApplicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmlPersonApplicantJourneyReader implements JourneyReader<AmlPersonApplicant> {

    private final DataMapper dataMapper;

    @Override
    public AmlPersonApplicant read(String subject, String context, Case caseInstance) {
        return dataMapper.toAmlPersonApplicant(
            caseInstance.getCaseData(SmeCaseDefinition.class).getApplicants()
                .stream().filter(applicant -> applicant.getId().equals(subject)).findFirst().orElseThrow()
        );
    }

    @Override
    public boolean canApply(String context, Case singleCase) {
        return ProcessConstants.BPM_AML_TYPE_PERSON.equalsIgnoreCase(context);
    }
}
