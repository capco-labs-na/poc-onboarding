package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmlPersonInfoJourneyReader implements JourneyReader<AntiMoneyLaunderingInfo> {

    private final DataMapper dataMapper;

    @Override
    public AntiMoneyLaunderingInfo read(String subject, String context, Case caseInstance) {
        var applicant = caseInstance.getCaseData(SmeCaseDefinition.class).getApplicants()
            .stream().filter(a -> a.getId().equals(subject)).findFirst().orElseThrow();
        return dataMapper.toAntiMoneyLaunderingInfo(applicant);
    }

    @Override
    public boolean canApply(String context, Case singleCase) {
        return (ProcessConstants.BPM_AML_TYPE_PERSON).equalsIgnoreCase(context);
    }
}
