package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.ADDRESS;
import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.BUSINESS_ADDRESS_CONTEXT;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class BusinessAddressJourneyReader implements JourneyReader<Map> {

    private final ObjectMapper objectMapper;

    @Override
    public Map read(String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var businessAddress = caseDefinition.getCompanyLookupInfo().getBusinessAddressInfo();
        return Collections.singletonMap(ADDRESS, this.objectMapper.convertValue(businessAddress, Map.class));
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(BUSINESS_ADDRESS_CONTEXT);
    }


}
