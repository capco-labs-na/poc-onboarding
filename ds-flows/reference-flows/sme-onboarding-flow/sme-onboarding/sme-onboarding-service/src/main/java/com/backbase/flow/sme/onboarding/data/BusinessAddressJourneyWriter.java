package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.BUSINESS_ADDRESS_CONTEXT;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.sme.onboarding.casedata.Address;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class BusinessAddressJourneyWriter implements JourneyWriter<Map> {

    private final ObjectMapper objectMapper;
    private final CaseDataService caseDataService;

    @Override
    public Case write(Map journeyData, String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        final var businessAddress = objectMapper.convertValue(journeyData, Address.class);
        caseDefinition.getCompanyLookupInfo().setBusinessAddressInfo(businessAddress);
        caseInstance.setCaseData(caseDefinition);
        return caseDataService.updateCase(caseInstance);
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(BUSINESS_ADDRESS_CONTEXT);
    }
}
