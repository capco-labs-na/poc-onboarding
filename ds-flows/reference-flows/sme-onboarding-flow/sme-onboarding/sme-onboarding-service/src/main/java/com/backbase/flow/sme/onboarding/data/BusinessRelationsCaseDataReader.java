package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.businessrelations.casedata.BusinessRelationsCaseData;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.interaction.model.PersonsBusinessRelationsDto;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BusinessRelationsCaseDataReader implements JourneyReader<BusinessRelationsCaseData> {

    private final DataMapper dataMapper;

    @Override
    public BusinessRelationsCaseData read(String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var businessRelationsDto = new PersonsBusinessRelationsDto();
        businessRelationsDto.setBusinessRelations(caseDefinition.getBusinessRelations());
        return dataMapper.toBusinessRelationsCaseData(businessRelationsDto);
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return true;
    }
}
