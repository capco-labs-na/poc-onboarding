package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.service.businessrelations.platform.BusinessRelationsCurrentUserData;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BusinessRelationsJourneyReader implements JourneyReader<BusinessRelationsCurrentUserData> {

    private final DataMapper dataMapper;

    @Override
    public BusinessRelationsCurrentUserData read(String subject, String context, Case caseInstance) {
        var smeCaseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var applicant = smeCaseDefinition.getApplicants()
            .stream().filter(Applicant::getIsRegistrar).findFirst().orElseThrow();
        return dataMapper.toBusinessRelationsCurrentUserData(applicant);
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return true;
    }
}
