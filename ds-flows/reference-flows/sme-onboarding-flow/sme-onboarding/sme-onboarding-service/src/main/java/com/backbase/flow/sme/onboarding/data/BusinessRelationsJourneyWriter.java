package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.businessrelations.casedata.BusinessRelationsCaseData;
import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.interaction.model.PersonsBusinessRelationsDto;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BusinessRelationsJourneyWriter implements JourneyWriter<BusinessRelationsCaseData> {

    private final CaseDataService caseDataService;
    private final DataMapper dataMapper;

    @Override
    public Case write(
        BusinessRelationsCaseData businessRelationsCaseData, String subject, String context, Case caseInstance
    ) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var personsBusinessRelationsDto = dataMapper.toSmeBusinessRelationsCaseData(businessRelationsCaseData);
        synchronizeApplicants(caseDefinition, personsBusinessRelationsDto);
        caseDefinition.setBusinessRelations(personsBusinessRelationsDto.getBusinessRelations());
        caseInstance.setCaseData(caseDefinition);
        return caseDataService.updateCase(caseInstance);
    }

    private void synchronizeApplicants(
        SmeCaseDefinition caseDefinition, PersonsBusinessRelationsDto personsBusinessRelationsDto
    ) {
        if (!personsBusinessRelationsDto.getBusinessPersons().isEmpty()) {
            var applicants = setBusinessPersonIdForRegistrar(caseDefinition, personsBusinessRelationsDto);
            updateApplicants(personsBusinessRelationsDto, applicants);
            caseDefinition.setApplicants(new ArrayList<>(applicants.values()));
        }
    }

    private Map<String, Applicant> setBusinessPersonIdForRegistrar(
        SmeCaseDefinition caseDefinition, PersonsBusinessRelationsDto personsBusinessRelationsDto
    ) {
        Map<String, Applicant> applicants = new HashMap<>();
        personsBusinessRelationsDto.getBusinessPersons().forEach(businessPerson ->
            caseDefinition.getApplicants().forEach(applicant -> {
                if (applicant.getId() == null
                    && Boolean.TRUE.equals(businessPerson.getCurrentUser())
                    && Boolean.TRUE.equals(applicant.getIsRegistrar())) {
                    applicant.setId(businessPerson.getId());
                }
                applicants.put(applicant.getId(), applicant);
            })
        );
        return applicants;
    }

    private void updateApplicants(
        PersonsBusinessRelationsDto personsBusinessRelationsDto, Map<String, Applicant> applicants
    ) {
        personsBusinessRelationsDto.getBusinessPersons().forEach(businessPerson -> {
            if (applicants.containsKey(businessPerson.getId())) {
                dataMapper.updateApplicant(applicants.get(businessPerson.getId()), businessPerson);
            } else {
                var newApplicant = dataMapper.toApplicant(businessPerson);
                newApplicant.setIsRegistrar(false);
                applicants.put(businessPerson.getId(), newApplicant);
            }
        });
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return true;
    }
}
