package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.service.companylookup.casedata.CompanyLookupDefinition;
import com.backbase.flow.service.companylookup.casedata.CompanyLookupInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class CompanyLookupJourneyReader implements JourneyReader<CompanyLookupInfo> {

    @Override
    public CompanyLookupInfo read(String subject, String context, Case caseToRead) {
        log.debug("Reading Company Lookup data for subject:{} and context: {}", subject, context);
        final var caseData = caseToRead.getCaseData(CompanyLookupDefinition.class);
        return caseData.getCompanyLookupInfo();
    }

    @Override
    public boolean canApply(String context, Case caseToRead) {
        return true;
    }
}
