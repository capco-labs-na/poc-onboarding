package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.service.companylookup.casedata.CompanyLookupDefinition;
import com.backbase.flow.service.companylookup.casedata.CompanyLookupInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CompanyLookupJourneyWriter implements JourneyWriter<CompanyLookupInfo> {

    private final CaseDataService caseDataService;

    @Override
    public Case write(CompanyLookupInfo journeyData, String subject, String context, Case caseToWrite) {

        final var caseData = caseToWrite.getCaseData(CompanyLookupDefinition.class);

        caseData.setCompanyLookupInfo(journeyData);

        caseToWrite.setCaseData(caseData);

        if (caseData.getCompanyLookupInfo() != null
            && caseData.getCompanyLookupInfo().getBusinessDetailsInfo() != null
            && caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getLegalName() != null) {
            caseToWrite.setTitle(caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getLegalName());
        }

        return caseDataService.updateCase(caseToWrite);
    }

    @Override
    public boolean canApply(String subject, Case caseToWrite) {
        return true;
    }
}
