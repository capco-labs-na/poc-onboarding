package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.integration.service.data.DocumentRequestData;
import com.backbase.flow.integration.service.data.MapperConstants;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.interaction.model.ApplicantDocumentRequestDto;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DocumentRequestDataReader implements JourneyReader<DocumentRequestData> {

    private final DataMapper dataMapper;

    @Override
    public DocumentRequestData read(String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);

        var applicant = caseDefinition.getApplicants().stream().filter(Applicant::getIsRegistrar)
            .findFirst().orElseThrow();

        var documentRequestData = dataMapper.toDocumentRequestData(new ApplicantDocumentRequestDto(
            applicant, caseDefinition.getCompanyLookupInfo().getBusinessDetailsInfo().getDocumentRequests()
        ));

        documentRequestData.getCustomer()
            .setFullName(caseDefinition.getCompanyLookupInfo().getBusinessDetailsInfo().getLegalName());

        return documentRequestData;
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(MapperConstants.CONTEXT);
    }
}
