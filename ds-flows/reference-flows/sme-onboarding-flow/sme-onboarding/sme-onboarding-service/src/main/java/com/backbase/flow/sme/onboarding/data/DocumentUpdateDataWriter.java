package com.backbase.flow.sme.onboarding.data;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.integration.service.data.DocumentUpdateData;
import com.backbase.flow.integration.service.data.MapperConstants;
import com.backbase.flow.integration.service.exception.DocumentRequestNotFoundException;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DocumentUpdateDataWriter implements JourneyWriter<DocumentUpdateData> {

    private final DataMapper dataMapper;
    private final CaseDataService caseDataService;

    @Override
    public Case write(DocumentUpdateData updateData, String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);

        var request = caseDefinition.getCompanyLookupInfo().getBusinessDetailsInfo().getDocumentRequests()
            .stream()
            .filter(d -> d.getInternalId().equalsIgnoreCase(updateData.getInternalId()))
            .findFirst().orElseThrow(() -> new DocumentRequestNotFoundException(updateData.getInternalId()));

        request.setProcessInstanceId(updateData.getProcessInstanceId());
        request.setDeadline(updateData.getDeadline());
        request.setFilesetName(updateData.getFilesetName());
        request.setStatus(dataMapper.toStatus(updateData.getStatus()));

        caseInstance.setCaseData(caseDefinition);

        return caseDataService.updateCase(caseInstance);
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(MapperConstants.CONTEXT);
    }
}
