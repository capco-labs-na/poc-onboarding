package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.service.ServiceItemOneTimePasswordAutoconfiguration.OTP_MAPPER_CONTEXT;
import static com.backbase.flow.service.mapper.MapperConstants.EMAIL_FIELD;
import static com.backbase.flow.service.mapper.MapperConstants.PHONE_NUMBER_FIELD;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class OtpJourneyReader implements JourneyReader<Map> {

    @Override
    public Map read(String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var applicant = caseDefinition.getApplicants().stream().filter(Applicant::getIsRegistrar)
            .findFirst().orElseThrow();
        var response = new HashMap<String, String>();
        response.put(PHONE_NUMBER_FIELD, applicant.getPhoneNumber());
        response.put(EMAIL_FIELD, applicant.getEmail());
        return response;
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(OTP_MAPPER_CONTEXT);
    }
}
