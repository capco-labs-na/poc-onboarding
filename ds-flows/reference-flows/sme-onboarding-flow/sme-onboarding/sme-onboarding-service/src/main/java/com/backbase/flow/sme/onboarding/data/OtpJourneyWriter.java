package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.service.ServiceItemOneTimePasswordAutoconfiguration.OTP_MAPPER_CONTEXT;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.service.mapper.MapperConstants;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.util.Map;
import java.util.Objects;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class OtpJourneyWriter implements JourneyWriter<Map> {

    private final CaseDataService caseDataService;

    @Override
    public Case write(Map journeyData, String subject, String context, Case caseInstance) {
        var caseDefinition = caseInstance.getCaseData(SmeCaseDefinition.class);
        var applicant = caseDefinition.getApplicants().stream().filter(Applicant::getIsRegistrar)
            .findFirst().orElseThrow();
        var emailVerified = (Boolean) journeyData.get(MapperConstants.EMAIL_VERIFIED_FIELD);
        if (Objects.nonNull(emailVerified)) {
            applicant.setEmail((String) journeyData.get(MapperConstants.EMAIL_FIELD));
            applicant.setEmailVerified(emailVerified);
        }
        var phoneNumberVerified = (Boolean) journeyData.get(MapperConstants.PHONE_NUMBER_VERIFIED_FIELD);
        if (Objects.nonNull(phoneNumberVerified)) {
            applicant.setPhoneNumber((String) journeyData.get(MapperConstants.PHONE_NUMBER_FIELD));
            applicant.setPhoneNumberVerified(phoneNumberVerified);
        }
        caseInstance.setCaseData(caseDefinition);
        return caseDataService.updateCase(caseInstance);
    }

    @Override
    public boolean canApply(String context, Case caseInstance) {
        return context.equalsIgnoreCase(OTP_MAPPER_CONTEXT);
    }
}
