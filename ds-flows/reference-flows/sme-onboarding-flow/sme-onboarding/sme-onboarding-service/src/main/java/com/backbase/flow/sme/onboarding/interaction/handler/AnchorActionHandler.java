package com.backbase.flow.sme.onboarding.interaction.handler;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.REGISTRAR;

import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.casedata.Applicant.RelationType;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.error.SmeErrors;
import com.backbase.flow.sme.onboarding.interaction.model.AnchorRequestDto;
import com.backbase.flow.sme.onboarding.mapper.DataMapper;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("sme-onboarding-anchor-data")
public class AnchorActionHandler extends ValidationActionHandler<AnchorRequestDto, Void> {

    private final DataMapper dataMapper;

    @Override
    protected ActionResult<Void> handleValidData(
        AnchorRequestDto payload, SmeCaseDefinition smeCase, InteractionContext context
    ) {
        if (Objects.isNull(payload)) {
            return new ActionResult<>(null, SmeErrors.INPUT_DATA_INVALID);
        }

        var applicant = dataMapper.toRegistrar(payload);

        applicant.setIsRegistrar(true);
        applicant.setRole(REGISTRAR);
        applicant.setOwnershipPercentage(0d);
        applicant.setRelationType(RelationType.OWNER);

        smeCase.getApplicants().add(applicant);

        context.savePreliminaryCaseData(smeCase);

        if (ageVerified(payload.getDateOfBirth(), context)) {
            return new ActionResult<>(null);
        }

        return new ActionResult<>(null, SmeErrors.INPUT_AGE_INVALID.withContext(
            Map.of("dateOfBirth", "Customer's age should be between 18 and 100 years old.")
        ));
    }

    private boolean ageVerified(final String dateOfBirth, final InteractionContext context) {
        final int age = Period.between(LocalDate.parse(dateOfBirth), LocalDate.now()).getYears();

        final List<Map<String, Object>> response = context
            .checkDecisionTable("age-verification", null, Collections.singletonMap("age", age));

        return (boolean) response.get(0).get("age-verified");
    }

}
