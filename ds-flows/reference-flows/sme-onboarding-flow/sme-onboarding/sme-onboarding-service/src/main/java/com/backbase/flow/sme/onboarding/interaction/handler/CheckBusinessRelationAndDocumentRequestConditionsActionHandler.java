package com.backbase.flow.sme.onboarding.interaction.handler;

import static com.backbase.flow.sme.onboarding.constants.ProcessConstants.DMN_DECIDED_ON_BUSINESS_RELATION_REQUIRED;
import static com.backbase.flow.sme.onboarding.constants.ProcessConstants.DMN_DOCUMENT_REQUIRED;
import static com.backbase.flow.sme.onboarding.constants.ProcessConstants.DMN_OUTPUT_DOCUMENT_REQUIRED;
import static java.util.Objects.isNull;

import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.BusinessStructureInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.error.SmeErrors;
import com.backbase.flow.sme.onboarding.interaction.model.BusinessRelationAndDocumentRequestConditionsResponseDto;
import com.backbase.flow.sme.onboarding.interaction.utils.SmeUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component("check-business-relation-and-document-requests-conditions")
public class CheckBusinessRelationAndDocumentRequestConditionsActionHandler extends
    ValidationActionHandler<Void, BusinessRelationAndDocumentRequestConditionsResponseDto> {

    @Override
    protected ActionResult<BusinessRelationAndDocumentRequestConditionsResponseDto> handleValidData(
        Void payload, SmeCaseDefinition smeCase, InteractionContext context
    ) {
        var isBusinessRelationRequired = isBusinessRelationRequired(smeCase, context);

        if (!isBusinessRelationRequired) {
            smeCase.setBusinessRelationsState(SmeCaseDefinition.BusinessRelationsState.NOT_REQUIRED);
            var registrar = updateRegistrar(smeCase);
            if (registrar.isEmpty()) {
                return new ActionResult<>(null, SmeErrors.INPUT_REGISTRAR_INVALID);
            }
        }
        var isDocumentRequired = isDocumentRequired(context);
        smeCase.setDocumentRequired(isDocumentRequired);
        smeCase.setBusinessRelationRequired(isBusinessRelationRequired);
        context.saveCaseDataToReadCaseData(smeCase);

        return new ActionResult<>(
            new BusinessRelationAndDocumentRequestConditionsResponseDto(
                isBusinessRelationRequired,
                isDocumentRequired
            ));
    }

    private Optional<Applicant> updateRegistrar(SmeCaseDefinition smeCase) {
        var registrarApplicant = SmeUtils.validateAndReturnRegistrar(smeCase);
        registrarApplicant
            .filter(registrar -> isNull(registrar.getId()))
            .ifPresent(registrar -> registrar.setId(UUID.randomUUID().toString()));
        return registrarApplicant;
    }

    private boolean isBusinessRelationRequired(SmeCaseDefinition smeCase, InteractionContext context) {
        var businessStructureInfo = smeCase.getCompanyLookupInfo().getBusinessStructureInfo();
        var outputs = context.checkDecisionTable(ProcessConstants.DMN_DECIDED_ON_BUSINESS_RELATION, null,
            prepareInputs(businessStructureInfo));
        return (boolean) outputs.get(0).get(DMN_DECIDED_ON_BUSINESS_RELATION_REQUIRED);
    }

    private boolean isDocumentRequired(InteractionContext context) {
        var documentRequired = context.checkDecisionTable(context.getCaseKey(),
            DMN_DOCUMENT_REQUIRED, null, new HashMap<>());
        return (boolean) documentRequired.get(0).get(DMN_OUTPUT_DOCUMENT_REQUIRED);
    }

    private Map<String, Object> prepareInputs(BusinessStructureInfo businessStructureInfo) {
        var businessType = businessStructureInfo.getType().value();
        var businessSubType =
            businessStructureInfo.getSubtype() == null ? null : businessStructureInfo.getSubtype().value();

        var inputs = new HashMap<String, Object>();
        inputs.put(ProcessConstants.DMN_DECIDED_ON_BUSINESS_RELATION_BUSINESS_STRUCTURE, businessType);
        inputs.put(ProcessConstants.DMN_DECIDED_ON_BUSINESS_RELATION_BUSINESS_SUBTYPE, businessSubType);
        return inputs;
    }
}
