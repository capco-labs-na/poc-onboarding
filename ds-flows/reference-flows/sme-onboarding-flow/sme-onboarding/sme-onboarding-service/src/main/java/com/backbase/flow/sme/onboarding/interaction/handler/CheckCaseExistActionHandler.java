package com.backbase.flow.sme.onboarding.interaction.handler;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.cases.CaseFilterRequest;
import com.backbase.flow.casedata.definition.CaseDefinitionId;
import com.backbase.flow.iam.FlowSecurityContext;
import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.process.service.FlowProcessService;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.credential.CredentialService;
import com.backbase.flow.sme.onboarding.error.SmeErrors;
import com.backbase.flow.sme.onboarding.interaction.model.CaseResponseDto;
import com.backbase.flow.sme.onboarding.interaction.utils.SmeUtils;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("sme-onboarding-check-case-exist")
public class CheckCaseExistActionHandler extends ValidationActionHandler<Void, CaseResponseDto> {

    private final CaseDataService caseDataService;
    private final FlowSecurityContext flowSecurityContext;
    private final FlowProcessService processService;
    private final CredentialService credentialChecker;

    @Override
    protected ActionResult<CaseResponseDto> handleValidData(
        Void payload, SmeCaseDefinition smeCase, InteractionContext context
    ) {
        var applicant = SmeUtils.validateAndReturnRegistrar(smeCase);
        if (applicant.isEmpty()) {
            return new ActionResult<>(null, SmeErrors.INPUT_REGISTRAR_INVALID);
        }
        var response = new CaseResponseDto();
        var caseFilterRequest = new CaseFilterRequest();
        caseFilterRequest.setPropertyName(CaseDefinitionConstants.EMAIL_INDEX);
        caseFilterRequest.setPropertyValues(Collections.singletonList(applicant.get().getEmail()));
        var cases = flowSecurityContext.runWithoutAuthorization(() -> caseDataService.filterCasesByCaseDefinitionId(
            new CaseDefinitionId(CaseDefinitionConstants.CASE_DEFINITION_KEY),
            caseFilterRequest
        ));
        boolean caseDetailsNotMatch = true;
        for (Case caseFromCaseStore : cases) {
            var smeCaseDefinition = caseFromCaseStore.getCaseData(SmeCaseDefinition.class);
            var storedApplicant = SmeUtils.validateAndReturnRegistrar(smeCaseDefinition);
            if (storedApplicant.isEmpty()) {
                return new ActionResult<>(null, SmeErrors.INPUT_REGISTRAR_INVALID);
            }
            if (detailsExistenceCheckInCase(storedApplicant.get(), applicant.get())) {
                caseDetailsNotMatch = false;
            } else {
                caseDataService.archiveCase(caseFromCaseStore.getKey());
            }
        }
        if (cases.isEmpty() || caseDetailsNotMatch) {
            context.promoteCase();
            response.setFirstName(applicant.get().getFirstName());
            response.setLastName(applicant.get().getLastName());
            flowSecurityContext.runWithoutAuthorization(() -> processService.startProcess(
                context.getCaseKey(), ProcessConstants.BPM_DATA_GATHERING)
            );
        } else {
            response.setCaseExist(true);
            response.setIdentityCredentialExist(credentialChecker.exists(applicant.get().getEmail()));
        }

        return new ActionResult<>(response);
    }

    private boolean detailsExistenceCheckInCase(Applicant storedApplicant, Applicant applicant) {
        return StringUtils.equals(storedApplicant.getFirstName(), applicant.getFirstName())
            && StringUtils.equals(storedApplicant.getLastName(), applicant.getLastName())
            && StringUtils.equals(storedApplicant.getDateOfBirth().toString(), applicant.getDateOfBirth().toString())
            && StringUtils.equals(storedApplicant.getPhoneNumber(), applicant.getPhoneNumber());
    }
}
