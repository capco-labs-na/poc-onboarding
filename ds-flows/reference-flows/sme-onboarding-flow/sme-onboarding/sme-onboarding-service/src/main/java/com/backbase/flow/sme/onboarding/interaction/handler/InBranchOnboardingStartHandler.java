package com.backbase.flow.sme.onboarding.interaction.handler;

import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.casedata.Address;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.interaction.model.CaseDataDto;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

@Component("in-branch-onboarding-start")
public class InBranchOnboardingStartHandler extends
    ValidationActionHandler<InBranchOnboardingStartHandler.InBranchApplicantDto, CaseDataDto> {

    @Override
    protected ActionResult<CaseDataDto> handleValidData(
        InBranchApplicantDto applicant, SmeCaseDefinition caseDefinition, InteractionContext context
    ) {
        mapToCaseDefinition(caseDefinition, applicant);
        context.savePreliminaryCaseData(caseDefinition);
        return ActionResult.success(new CaseDataDto(context.getCaseKey()));
    }

    private void mapToCaseDefinition(SmeCaseDefinition caseDefinition, InBranchApplicantDto applicant) {
        caseDefinition.setApplicants(List.of(
            new Applicant()
                .withFirstName(applicant.getFirstName())
                .withLastName(applicant.getLastName())
                .withEmail(applicant.getEmail())
                .withPersonalAddress(new Address()
                    .withCity(applicant.getCity())
                    .withNumberAndStreet(applicant.getNumberAndStreet())
                    .withApt(applicant.getApt())
                    .withState(applicant.getState())
                    .withZipCode(applicant.getZipCode())
                )));
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class InBranchApplicantDto {

        String firstName;
        String lastName;
        String email;
        String businessName;
        String numberAndStreet;
        String apt;
        String city;
        String state;
        String zipCode;
    }
}
