package com.backbase.flow.sme.onboarding.interaction.hook;

import com.backbase.flow.iam.FlowSecurityContext;
import com.backbase.flow.interaction.engine.action.ActionHandlerHook;
import com.backbase.flow.interaction.engine.action.ActionHookResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.process.service.FlowTaskService;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.TaskService;
import org.springframework.stereotype.Component;

/**
 * This hook runs after the company look up journey, it tries to complete the company details user task if it is not
 * completed by the CSR.
 */
@Component("company-details-hook")
@RequiredArgsConstructor
public class CompanyDetailsActionHandlerHook implements ActionHandlerHook {

    private final FlowSecurityContext flowSecurityContext;
    private final TaskService taskService;
    private final FlowTaskService flowTaskService;

    @Override
    public ActionHookResult executeHook(final InteractionContext context) {
        var completed = new AtomicBoolean(true);
        flowSecurityContext.runWithoutAuthorization(
            () -> Optional.ofNullable(
                taskService.createTaskQuery()
                    .processDefinitionKey(ProcessConstants.BPM_KYB)
                    .taskDefinitionKey(ProcessConstants.COMPANY_DETAILS_USER_TASK_ID)
                    .processVariableValueEquals(ProcessConstants.PROCESS_VARIABLE_CASE_KEY,
                        context.getCaseKey().toString())
                    .singleResult()
            ).ifPresentOrElse(
                task -> flowTaskService.completeTask(task.getId(), Collections.emptyMap(), null),
                () -> completed.set(false))
        );
        return new ActionHookResult(completed.get(), null);
    }

    @Override
    @SuppressWarnings("squid:S5738")
    public boolean execute(InteractionContext context) {
        throw new UnsupportedOperationException();
    }
}
