package com.backbase.flow.sme.onboarding.interaction.hook;

import com.backbase.flow.interaction.engine.action.ActionHandlerHook;
import com.backbase.flow.interaction.engine.action.ActionHookResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("update-businessRelation-state")
public class UpdateBusinessRelationStateHook implements ActionHandlerHook {

    @Override
    public ActionHookResult executeHook(InteractionContext interactionContext) {
        var smeCaseDefinition = interactionContext.getCaseData(SmeCaseDefinition.class);
        smeCaseDefinition.setBusinessRelationsState(SmeCaseDefinition.BusinessRelationsState.FINISHED);
        interactionContext.saveCaseDataToReadCaseData(smeCaseDefinition);
        return new ActionHookResult(true, null);
    }

    @Override
    @SuppressWarnings("squid:S5738")
    public boolean execute(InteractionContext context) {
        throw new UnsupportedOperationException();
    }
}
