package com.backbase.flow.sme.onboarding.interaction.model;

import com.backbase.flow.sme.onboarding.validation.DateFormat;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AnchorRequestDto {

    String firstName;
    String lastName;
    @DateFormat
    String dateOfBirth;
    String emailAddress;
}
