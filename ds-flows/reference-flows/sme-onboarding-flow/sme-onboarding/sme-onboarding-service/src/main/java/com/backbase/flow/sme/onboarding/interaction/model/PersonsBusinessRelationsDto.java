package com.backbase.flow.sme.onboarding.interaction.model;

import com.backbase.flow.businessrelations.casedata.BusinessPerson;
import com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class PersonsBusinessRelationsDto {

    private BusinessRelationsCaseData businessRelations;
    private List<BusinessPerson> businessPersons = new ArrayList<>();

}
