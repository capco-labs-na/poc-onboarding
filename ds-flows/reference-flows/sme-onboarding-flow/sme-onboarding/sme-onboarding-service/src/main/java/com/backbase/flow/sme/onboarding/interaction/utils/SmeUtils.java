package com.backbase.flow.sme.onboarding.interaction.utils;

import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import lombok.experimental.UtilityClass;

@UtilityClass
public class SmeUtils {

    public static Optional<Applicant> validateAndReturnRegistrar(SmeCaseDefinition smeCase) {
        return Optional.ofNullable(smeCase.getApplicants())
            .stream()
            .flatMap(Collection::stream)
            .filter(Objects::nonNull)
            .filter(applicant -> Optional.ofNullable(applicant.getIsRegistrar()).orElse(false))
            .findFirst();
    }
}
