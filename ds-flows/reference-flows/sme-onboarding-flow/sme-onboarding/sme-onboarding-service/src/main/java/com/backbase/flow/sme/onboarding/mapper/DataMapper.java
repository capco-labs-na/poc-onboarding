package com.backbase.flow.sme.onboarding.mapper;

import com.backbase.flow.businessrelations.casedata.BusinessPerson;
import com.backbase.flow.businessrelations.casedata.BusinessRelationsCaseData;
import com.backbase.flow.casedata.api.model.FlowMilestoneDto;
import com.backbase.flow.casedata.tracking.CaseMilestone;
import com.backbase.flow.integration.service.data.DocumentRequestData;
import com.backbase.flow.service.aml.casedata.AmlBusinessApplicant;
import com.backbase.flow.service.aml.casedata.AmlPersonApplicant;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.service.businessrelations.platform.BusinessRelationsCurrentUserData;
import com.backbase.flow.sme.onboarding.casedata.AmlInfo;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.BusinessDetails;
import com.backbase.flow.sme.onboarding.casedata.TermsAndConditions;
import com.backbase.flow.sme.onboarding.event.TermsAndConditionsApprovedEvent;
import com.backbase.flow.sme.onboarding.interaction.model.AnchorRequestDto;
import com.backbase.flow.sme.onboarding.interaction.model.ApplicantDocumentRequestDto;
import com.backbase.flow.sme.onboarding.interaction.model.PersonsBusinessRelationsDto;
import com.backbase.flow.sme.onboarding.interaction.model.TermsConditionsRequestDto;
import java.util.UUID;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface DataMapper {

    @Mapping(target = "email", source = "emailAddress")
    @Mapping(target = "dateOfBirth", source = "dateOfBirth", dateFormat = "yyyy-MM-dd")
    Applicant toRegistrar(AnchorRequestDto request);

    @Mapping(target = "acceptanceDate", expression = "java(java.time.OffsetDateTime.now())")
    TermsAndConditions toTermsAndConditions(TermsConditionsRequestDto payload);

    default TermsAndConditionsApprovedEvent toTermsAndConditionsEvent(UUID caseKey) {
        return new TermsAndConditionsApprovedEvent(caseKey);
    }

    @Mapping(target = "customer", source = "applicant")
    @Mapping(target = "customer.userId", source = "applicant.id")
    @Mapping(target = "customer.emailAddress", source = "applicant.email")
    DocumentRequestData toDocumentRequestData(ApplicantDocumentRequestDto requestDto);

    com.backbase.flow.sme.onboarding.casedata.DocumentRequest.Status toStatus(
        com.backbase.flow.integration.service.data.DocumentRequest.Status status);

    @Mapping(target = "amlBusinessApplicant.businessName", source = "dba")
    @Mapping(target = "reviewApproved", source = "antiMoneyLaunderingInfo.reviewApproved")
    @Mapping(target = "reviewNeeded", source = "antiMoneyLaunderingInfo.reviewNeeded")
    @Mapping(target = "reviewApprovedReason", source = "antiMoneyLaunderingInfo.reviewApprovedReason")
    @Mapping(target = "reviewDeclinedComment", source = "antiMoneyLaunderingInfo.reviewDeclinedComment")
    @Mapping(target = "status", source = "antiMoneyLaunderingInfo.status")
    @Mapping(target = "amlResult", source = "antiMoneyLaunderingInfo.amlResult")
    AntiMoneyLaunderingInfo toAntiMoneyLaunderingInfo(BusinessDetails businessDetails);

    @Mapping(target = "amlPersonApplicant.fullName", expression = "java(applicant.getFirstName() + \" \" "
        + "+ applicant.getLastName())")
    @Mapping(target = "amlPersonApplicant.yearOfBirth", expression = "java(getYob(applicant))")
    @Mapping(target = "reviewApproved", source = "antiMoneyLaunderingInfo.reviewApproved")
    @Mapping(target = "reviewNeeded", source = "antiMoneyLaunderingInfo.reviewNeeded")
    @Mapping(target = "reviewApprovedReason", source = "antiMoneyLaunderingInfo.reviewApprovedReason")
    @Mapping(target = "reviewDeclinedComment", source = "antiMoneyLaunderingInfo.reviewDeclinedComment")
    @Mapping(target = "status", source = "antiMoneyLaunderingInfo.status")
    @Mapping(target = "amlResult", source = "antiMoneyLaunderingInfo.amlResult")
    AntiMoneyLaunderingInfo toAntiMoneyLaunderingInfo(Applicant applicant);

    AmlInfo toAmlInfo(AntiMoneyLaunderingInfo antiMoneyLaunderingInfo);

    @Mapping(target = "businessName", source = "dba")
    AmlBusinessApplicant toAmlBusinessApplicant(BusinessDetails businessDetails);

    @Mapping(target = "epic", source = "caseMilestoneId.epic")
    @Mapping(target = "name", source = "caseMilestoneId.name")
    FlowMilestoneDto toFlowMilestoneDto(CaseMilestone caseMilestone);

    @Mapping(target = "fullName", expression = "java(applicant.getFirstName() + \" \" + applicant.getLastName())")
    @Mapping(target = "yearOfBirth", expression = "java(getYob(applicant))")
    AmlPersonApplicant toAmlPersonApplicant(Applicant applicant);

    @Mapping(target = "email", source = "applicant.email")
    @Mapping(target = "firstName", source = "applicant.firstName")
    @Mapping(target = "lastName", source = "applicant.lastName")
    @Mapping(target = "phoneNumber", source = "applicant.phoneNumber")
    BusinessRelationsCurrentUserData toBusinessRelationsCurrentUserData(Applicant applicant);

    @Mapping(target = "businessRelations.status", source = "status")
    @Mapping(target = "businessRelations.businessRelationType", source = "businessRelationType")
    @Mapping(target = "businessRelations.reviewInformation", source = "reviewInformation")
    @Mapping(target = "businessRelations.businessPersons", source = "businessPersons")
    @Mapping(target = "businessRelations.processInstanceId", source = "processInstanceId")
    PersonsBusinessRelationsDto toSmeBusinessRelationsCaseData(
        BusinessRelationsCaseData businessRelationsCaseData);

    @Mapping(target = "businessRelationType", source = "businessRelations.businessRelationType")
    @Mapping(target = "reviewInformation", source = "businessRelations.reviewInformation")
    @Mapping(target = "status", source = "businessRelations.status")
    @Mapping(target = "businessPersons", source = "businessRelations.businessPersons")
    @Mapping(target = "processInstanceId", source = "businessRelations.processInstanceId")
    BusinessRelationsCaseData toBusinessRelationsCaseData(
        PersonsBusinessRelationsDto applicantsBusinessRelationsDto);

    @Mapping(target = "phoneNumber", source = "phone")
    @Mapping(target = "isRegistrar", source = "currentUser")
    Applicant toApplicant(BusinessPerson businessPerson);

    @Mapping(target = "phoneNumber", source = "phone")
    void updateApplicant(@MappingTarget Applicant applicant, BusinessPerson businessPerson);

    default Integer getYob(Applicant applicant) {
        return applicant.getDateOfBirth() != null ? applicant.getDateOfBirth().getYear() : null;
    }
}
