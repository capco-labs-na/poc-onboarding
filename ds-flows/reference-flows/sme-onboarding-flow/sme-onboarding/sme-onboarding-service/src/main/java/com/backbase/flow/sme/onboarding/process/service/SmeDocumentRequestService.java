package com.backbase.flow.sme.onboarding.process.service;

import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.sme.onboarding.casedata.DocumentRequest;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.util.Objects;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SmeDocumentRequestService {

    private final CaseDataService caseDataService;

    public boolean allDocsUploaded(UUID caseKey) {
        var caseInstance = caseDataService.getCaseByKey(caseKey);
        var caseData = caseInstance.getCaseData(SmeCaseDefinition.class);
        return caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getDocumentRequests().stream()
            .filter(Objects::nonNull)
            .map(DocumentRequest::getStatus)
            .noneMatch(DocumentRequest.Status.OPEN::equals);
    }
}
