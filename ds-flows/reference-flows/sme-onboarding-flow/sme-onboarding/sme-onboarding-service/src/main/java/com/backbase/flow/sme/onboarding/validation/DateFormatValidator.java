package com.backbase.flow.sme.onboarding.validation;

import java.time.LocalDate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true;
        }

        try {
            LocalDate.parse(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
