package com.backbase.flow.sme.onboarding.validation;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailFormatValidator implements ConstraintValidator<EmailFormat, String> {

    private static final String EMAIL_REGEX_PATTERN =
        "^([ ]*[A-Za-z0-9]+[-A-Za-z0-9+.#_]*)@([A-Za-z0-9]+[-A-Za-z0-9#_]*)\\.([A-Za-z0-9\\-]+[.]?[A-Za-z0-9]+[ ]*)$";

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true;
        }

        return Pattern.compile(EMAIL_REGEX_PATTERN).matcher(s).matches();
    }
}
