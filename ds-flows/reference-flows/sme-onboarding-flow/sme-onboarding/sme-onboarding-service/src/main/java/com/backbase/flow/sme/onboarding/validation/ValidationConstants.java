package com.backbase.flow.sme.onboarding.validation;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidationConstants {

    public static final String EIN_REGEX_PATTERN =
        "^(0[1-6]||1[0-6]|2[0-7]|[345]\\d|[68][0-8]|7[1-7]|9[0-58-9])-?\\d{7}$";

    /**
     * A valid Social Security Number cannot: • Contain all zeroes in any specific group (e.g 000-##-####, ###-00-####,
     * or ###-##-0000) • Begin with ‘666’. • Begin with any value from ‘900-999′ • Be ‘078-05-1120′ (due to the
     * Woolworth’s Wallet Fiasco) • Be ‘219-09-9999′ (as it appeared in an advertisement for the Social Security
     * Administration)
     */
    public static final String SSN_REGEX_PATTERN =
        "^(?!219099999|078051120)(?!666|000|9\\d{2})\\d{3}(?!00)\\d{2}(?!0{4})\\d{4}$";

    public static final String EIN_VALIDATION_MESSAGE = "Please provide a valid EIN.";
    public static final String KNOW_NAME_VALIDATION_MESSAGE = "knownName may not exceed {max} characters.";
    public static final String SSN_VALIDATION_MESSAGE = "Please provide a valid SSN.";

}
