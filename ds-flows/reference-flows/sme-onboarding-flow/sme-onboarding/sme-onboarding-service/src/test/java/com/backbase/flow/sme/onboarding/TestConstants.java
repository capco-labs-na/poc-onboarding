package com.backbase.flow.sme.onboarding;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestConstants {

    public final static String CASE_DEFINITION_ID = "sme";
    public final static String INTERACTION_SME_ONBOARDING = "sme-onboarding";
    public final static String INTERACTION_IN_BRANCH_ONBOARDING_START = "in-branch-onboarding-start";
    public final static String INTERACTION_SOLE_PROPRIETORSHIP_APPLICATION_CENTER = "sme-application-center";

    public final static String DATA_GATHERING_DMN_ID = "required-docs";

    public static final String TRANSPORTATION_AND_WAREHOUSING_GENERAL = "48";
    public static final String FINANCE_AND_INSURANCE = "52";
    public static final String MINING_QUARRYING_OIL_AND_GAS_EXTRACTION = "21";

    public final static String ACTION_URL =
        "/client-api/interaction/v2/interactions/{interactionName}/actions/{actionName}";
    public final static String ACTION_SOLE_PROPRIETORSHIP_INIT = "sme-onboarding-init";
    public final static String ACTION_SOLE_PROPRIETORSHIP_ANCHOR = "sme-onboarding-anchor-data";
    public final static String ACTION_SOLE_PROPRIETORSHIP_CHECK_CASE_EXIST = "sme-onboarding-check-case-exist";
    public final static String STEP_SOLE_PROPRIETORSHIP_CHECK_CASE_EXIST = "sme-onboarding-check-case";

    public final static String APPLICANT_ONBOARDING_PENDING_EVENT = "ApplicantOnboardingPendingEvent";
    public final static String APPLICANT_ONBOARDING_FINISHED_EVENT = "ApplicantOnboardingFinishedEvent";
    public final static String AML_PENDING_EVENT = "AmlPendingEvent";
    public final static String AML_SUCCEED_EVENT = "AmlSucceedEvent";
    public final static String APPLICANT_ACTION_NOT_REQUIRED_EVENT = "ApplicantActionNotRequiredEvent";
    public final static String KYC_PENDING = "KYCPending";
    public final static String KYC_COMPLETED = "KYCCompleted";

    public final static String EVENT_NAME_KYB_NOT_REQUIRED = "KYBNotRequired";
    public final static String EVENT_NAME_KYB_COMPLETED = "KYBComplete";

    public final static String ACTION_SSN = "submit-ssn";
    public final static String STEP_NAME_SSN = "sme-onboarding-ssn";
    public final static String ACTION_SME_ON_BOARDING_ADDRESS = "submit-address";
    public final static String STEP_NAME_SME_ON_BOARDING_PERSONAL_ADDRESS = "sme-onboarding-personal-address";
    public final static String STEP_NAME_SME_ON_BOARDING_LANDING = "sme-onboarding-landing";
    public final static String ACTION_SME_ON_BOARDING_LANDING = "sme-onboarding-landing-data";
    public final static String STEP_NAME_SME_ON_BOARDING_DOCUMENT_REQUEST_JOURNEY = "document-request-journey";
    public final static String ACTION_SME_ON_BOARDING_SUBMIT_DOCUMENT_REQUESTS = "submit-document-requests";
    public final static String INTERACTION_APPLICATION_CENTER = "sme-application-center";
    public final static String ACTION_APPLICATION_CENTER_INIT = "application-center-init";
    public final static String ACTION_APPLICATION_CENTER_SEND_REGISTRAR_DETAILS = "send-registrar-details";
    public final static String ACTION_GET_MILESTONES = "get-milestones";
    public final static String STEP_APPLICATION_CENTER_SEND_REGISTRAR_DETAILS = "sme-application-center";
    public final static String ACTION_SME_ON_BOARDING_GET_PRODUCT_LIST = "get-product-list";
    public final static String STEP_NAME_SME_ON_BOARDING_GET_PRODUCT_LIST = "select-products";
    public final static String ACTION_STEP_SME_ON_BOARDING_SELECT_PRODUCTS = "select-products";
    public final static String ACTION_FETCH_OTP_EMAIL = "fetch-otp-email";
    public final static String CHECK_BUSINESS_RELATION_CONDITIONS =
        "check-business-relation-and-document-requests-conditions";
    public final static String IN_BRANCH_ONBOARDING_START = "in-branch-onboarding-start";

}
