package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import org.junit.jupiter.api.Test;

class AmlBusinessApplicantJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withBusiness_returnTrue() {
        boolean canApply = amlBusinessApplicantJourneyReader
            .canApply(ProcessConstants.BPM_AML_TYPE_BUSINESS, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidData_mapToNewModel() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().businessKnownName("legal")
            .build();

        var caseInstance = startCase(caseDefinition);
        var applicant = amlBusinessApplicantJourneyReader
            .read(null, ProcessConstants.BPM_AML_TYPE_BUSINESS, caseInstance);

        assertNotNull(applicant);
        assertEquals("legal", applicant.getBusinessName());
    }

}
