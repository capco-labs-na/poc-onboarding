package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.AmlInfoBuilder;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.AmlInfo.Status;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import org.junit.jupiter.api.Test;

class AmlBusinessInfoJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withBusiness_returnTrue() {
        boolean canApply = amlBusinessInfoJourneyReader
            .canApply(ProcessConstants.BPM_AML_TYPE_BUSINESS, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidData_mapToNewModel() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().businessKnownName("legal")
            .businessAmlInfo(AmlInfoBuilder.getInstance().reviewNeeded(true).requestDate("2020/05/07")
                .matchStatus("FAILED").reviewDeclinedComment("Oops").caseStatus(Status.FAILED).buildAmlInfo())
            .build();

        var caseInstance = startCase(caseDefinition);
        var info = amlBusinessInfoJourneyReader
            .read(null, ProcessConstants.BPM_AML_TYPE_BUSINESS, caseInstance);

        assertNotNull(info);
        assertTrue(info.getReviewNeeded());
        assertEquals("legal", info.getAmlBusinessApplicant().getBusinessName());
        assertEquals("2020/05/07", info.getAmlResult().getRequestDate());
        assertEquals("FAILED", info.getAmlResult().getMatchStatus());
        assertEquals("Oops", info.getReviewDeclinedComment());
        assertEquals(AntiMoneyLaunderingInfo.Status.FAILED, info.getStatus());
    }
}
