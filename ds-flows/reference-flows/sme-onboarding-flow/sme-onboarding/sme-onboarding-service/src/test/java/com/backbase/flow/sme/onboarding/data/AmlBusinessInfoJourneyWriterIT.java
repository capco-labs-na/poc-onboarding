package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo.Status;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.AmlInfoBuilder;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.AmlInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import org.junit.jupiter.api.Test;

class AmlBusinessInfoJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withBusiness_returnTrue() {
        boolean canApply = amlBusinessInfoJourneyWriter
            .canApply(ProcessConstants.BPM_AML_TYPE_BUSINESS, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void write_withValidData_save() {
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .businessKnownName("legal")
            .build();

        var amlInfo = AmlInfoBuilder.getInstance().reviewNeeded(true).requestDate("2020/05/07")
            .matchStatus("FAILED").reviewDeclinedComment("Oops").dtoStatus(Status.FAILED)
            .buildAntiMoneyLaunderingInfo();

        var caseInstance = startCase(caseDefinition);

        var caseData = amlBusinessInfoJourneyWriter
            .write(amlInfo, null, ProcessConstants.BPM_AML_TYPE_BUSINESS, caseInstance)
            .getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertNotNull(caseData.getCompanyLookupInfo());
        assertNotNull(caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getAntiMoneyLaunderingInfo());
        assertTrue(caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getAntiMoneyLaunderingInfo()
            .getReviewNeeded());
        assertEquals("legal", caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getDba());
        assertEquals("2020/05/07", caseData.getCompanyLookupInfo().getBusinessDetailsInfo()
            .getAntiMoneyLaunderingInfo().getAmlResult().getRequestDate());
        assertEquals("FAILED", caseData.getCompanyLookupInfo().getBusinessDetailsInfo()
            .getAntiMoneyLaunderingInfo().getAmlResult().getMatchStatus());
        assertEquals("Oops", caseData.getCompanyLookupInfo().getBusinessDetailsInfo()
            .getAntiMoneyLaunderingInfo().getReviewDeclinedComment());
        assertEquals(AmlInfo.Status.FAILED, caseData.getCompanyLookupInfo().getBusinessDetailsInfo()
            .getAntiMoneyLaunderingInfo().getStatus());

    }
}
