package com.backbase.flow.sme.onboarding.data;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class AmlPersonApplicantJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withPerson_returnTrue() {
        boolean canApply = amlPersonApplicantJourneyReader
            .canApply(ProcessConstants.BPM_AML_TYPE_PERSON, null);

        assertThat(canApply).isTrue();
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidData_mapToNewModel() {
        var userId = UUID.randomUUID().toString();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena")
            .userId(userId)
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .build();

        var caseInstance = startCase(caseDefinition);
        var applicant = amlPersonApplicantJourneyReader
            .read(userId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance);

        assertNotNull(applicant);
        assertThat(applicant.getFullName()).isEqualTo("Boy Sena");
        assertThat(applicant.getYearOfBirth()).isEqualTo(1970);
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidMinimumData_mapToNewModel() {
        var userId = UUID.randomUUID().toString();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena")
            .userId(userId)
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .build();

        var caseInstance = startCase(caseDefinition);
        var applicant = amlPersonApplicantJourneyReader
            .read(userId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance);

        assertThat(applicant).isNotNull();
        assertThat(applicant.getFullName()).isEqualTo("Boy Sena");
        assertThat(applicant.getYearOfBirth()).isNull();
    }


    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidApplicant_throwsException() {
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena")
            .userId(UUID.randomUUID().toString())
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .build();

        var randomId = UUID.randomUUID().toString();
        var caseInstance = startCase(caseDefinition);

        assertThatThrownBy(() -> amlPersonApplicantJourneyReader
            .read(randomId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance))
            .isInstanceOf(NoSuchElementException.class);
    }

}
