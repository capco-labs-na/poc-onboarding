package com.backbase.flow.sme.onboarding.data;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.AmlInfoBuilder;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.AmlInfo.Status;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class AmlPersonInfoJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withPerson_returnTrue() {
        boolean canApply = amlPersonInfoJourneyReader
            .canApply(ProcessConstants.BPM_AML_TYPE_PERSON, null);

        assertThat(canApply).isTrue();
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidData_mapToNewModel() {
        var userId = UUID.randomUUID().toString();
        var personalAmlInfo = AmlInfoBuilder.getInstance()
            .reviewNeeded(true)
            .requestDate("2020/05/07")
            .matchStatus("FAILED")
            .reviewDeclinedComment("Oops")
            .caseStatus(Status.FAILED)
            .buildAmlInfo();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena").userId(userId)
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .personalAmlInfo(personalAmlInfo)
            .build();

        var caseInstance = startCase(caseDefinition);
        var info = amlPersonInfoJourneyReader
            .read(userId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance);

        assertThat(info).isNotNull();
        assertThat(info.getReviewNeeded()).isTrue();
        assertThat(info.getAmlPersonApplicant().getFullName()).isEqualTo("Boy Sena");
        assertThat(info.getAmlPersonApplicant().getYearOfBirth()).isEqualTo(1970);
        assertThat(info.getAmlResult().getRequestDate()).isEqualTo("2020/05/07");
        assertThat(info.getAmlResult().getMatchStatus()).isEqualTo("FAILED");
        assertThat(info.getReviewDeclinedComment()).isEqualTo("Oops");
        assertThat(info.getStatus()).isEqualTo(AntiMoneyLaunderingInfo.Status.FAILED);
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidMinimumData_mapToNewModel() {
        var userId = UUID.randomUUID().toString();
        var personalAmlInfo = AmlInfoBuilder.getInstance()
            .reviewNeeded(true)
            .requestDate("2020/05/07")
            .matchStatus("FAILED")
            .reviewDeclinedComment("Oops")
            .caseStatus(Status.FAILED)
            .buildAmlInfo();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena")
            .userId(userId)
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .personalAmlInfo(personalAmlInfo)
            .build();

        var caseInstance = startCase(caseDefinition);
        var info = amlPersonInfoJourneyReader
            .read(userId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance);

        assertThat(info).isNotNull();
        assertThat(info.getReviewNeeded()).isTrue();
        assertThat(info.getAmlPersonApplicant().getFullName()).isEqualTo("Boy Sena");
        assertThat(info.getAmlPersonApplicant().getYearOfBirth()).isNull();
        assertThat(info.getAmlResult().getRequestDate()).isEqualTo("2020/05/07");
        assertThat(info.getAmlResult().getMatchStatus()).isEqualTo("FAILED");
        assertThat(info.getReviewDeclinedComment()).isEqualTo("Oops");
        assertThat(info.getStatus()).isEqualTo(AntiMoneyLaunderingInfo.Status.FAILED);
    }

    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidApplicant_throwsException() {
        var personalAmlInfo = AmlInfoBuilder.getInstance()
            .reviewNeeded(true)
            .requestDate("2020/05/07")
            .matchStatus("FAILED")
            .reviewDeclinedComment("Oops")
            .caseStatus(Status.FAILED)
            .buildAmlInfo();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy")
            .lastName("Sena")
            .userId(UUID.randomUUID().toString())
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .personalAmlInfo(personalAmlInfo)
            .build();

        var caseInstance = startCase(caseDefinition);
        var randomId = UUID.randomUUID().toString();

        assertThatThrownBy(() -> amlPersonInfoJourneyReader
            .read(randomId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance))
            .isInstanceOf(NoSuchElementException.class);
    }
}
