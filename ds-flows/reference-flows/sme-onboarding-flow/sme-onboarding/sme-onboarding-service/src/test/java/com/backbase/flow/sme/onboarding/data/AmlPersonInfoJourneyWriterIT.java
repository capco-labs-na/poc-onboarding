package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.service.aml.casedata.AntiMoneyLaunderingInfo.Status;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.AmlInfoBuilder;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.AmlInfo;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class AmlPersonInfoJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withPerson_returnTrue() {
        boolean canApply = amlPersonInfoJourneyWriter
            .canApply(ProcessConstants.BPM_AML_TYPE_PERSON, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void write_withValidData_save() {
        var userId = UUID.randomUUID().toString();
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy").lastName("Sena").userId(userId)
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .build();

        var amlInfo = AmlInfoBuilder.getInstance().reviewNeeded(true).requestDate("2020/05/07")
            .matchStatus("FAILED").reviewDeclinedComment("Oops").dtoStatus(Status.FAILED)
            .buildAntiMoneyLaunderingInfo();

        var caseInstance = startCase(caseDefinition);

        var caseData = amlPersonInfoJourneyWriter
            .write(amlInfo, userId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance)
            .getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertNotNull(caseData.getApplicants().get(0));
        assertNotNull(caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo());
        assertTrue(caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo().getReviewNeeded());
        assertEquals("Boy", caseData.getApplicants().get(0).getFirstName());
        assertEquals("Sena", caseData.getApplicants().get(0).getLastName());
        assertEquals("2020/05/07",
            caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo().getAmlResult().getRequestDate());
        assertEquals("FAILED",
            caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo().getAmlResult().getMatchStatus());
        assertEquals("Oops", caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo().getReviewDeclinedComment());
        assertEquals(AmlInfo.Status.FAILED, caseData.getApplicants().get(0).getAntiMoneyLaunderingInfo().getStatus());

    }

    @Test
    @WithFlowAnonymousUser
    void write_whenInvalidApplicant_throwsException() {
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy").lastName("Sena").userId(UUID.randomUUID().toString())
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid")
            .build();

        var amlInfo = AmlInfoBuilder.getInstance().reviewNeeded(true).requestDate("2020/05/07")
            .matchStatus("FAILED").reviewDeclinedComment("Oops").dtoStatus(Status.FAILED)
            .buildAntiMoneyLaunderingInfo();

        var caseInstance = startCase(caseDefinition);
        var randomId = UUID.randomUUID().toString();

        assertThrows(NoSuchElementException.class, () -> amlPersonInfoJourneyWriter
            .write(amlInfo, randomId, ProcessConstants.BPM_AML_TYPE_PERSON, caseInstance));
    }
}
