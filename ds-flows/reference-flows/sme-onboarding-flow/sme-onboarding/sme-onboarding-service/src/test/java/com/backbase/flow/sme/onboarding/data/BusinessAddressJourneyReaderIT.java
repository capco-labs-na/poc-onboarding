package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.BUSINESS_ADDRESS_CONTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Address;
import java.util.LinkedHashMap;
import org.junit.jupiter.api.Test;

class BusinessAddressJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_readAddress_shouldMappedCorrectly() {
        var address = new Address()
            .withNumberAndStreet("12 Saint James street")
            .withApt("2A")
            .withZipCode("12345")
            .withCity("Albany")
            .withState("NY");
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .businessAddress(address)
            .build();
        var caseInstance = startCase(caseDefinition);
        var caseData = businessAddressJourneyReader.read("address", BUSINESS_ADDRESS_CONTEXT, caseInstance);
        var addressMap = (LinkedHashMap) caseData.get("address");
        assertNotNull(addressMap);
        assertEquals(address.getNumberAndStreet(), addressMap.get("numberAndStreet"));
        assertEquals(address.getApt(), addressMap.get("apt"));
        assertEquals(address.getCity(), addressMap.get("city"));
        assertEquals(address.getZipCode(), addressMap.get("zipCode"));
        assertEquals(address.getState(), addressMap.get("state"));
    }
}
