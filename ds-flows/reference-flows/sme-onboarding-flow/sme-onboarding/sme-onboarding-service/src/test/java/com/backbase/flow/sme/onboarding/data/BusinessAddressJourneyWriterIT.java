package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.BUSINESS_ADDRESS_CONTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Address;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

class BusinessAddressJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_writeAddress_shouldUpdatedCorrectly() {
        var address = new Address()
            .withNumberAndStreet("12 Saint James street")
            .withApt("2A")
            .withZipCode("12345")
            .withCity("Albany")
            .withState("NY");
        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .businessAddress(address)
            .build();
        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put("numberAndStreet", address.getNumberAndStreet());
        journeyMap.put("apt", address.getApt());
        journeyMap.put("zipCode", address.getZipCode());
        journeyMap.put("state", address.getState());
        journeyMap.put("city", address.getCity());
        var resultCase = businessAddressJourneyWriter
            .write(journeyMap, "address", BUSINESS_ADDRESS_CONTEXT, caseInstance);
        var caseData = resultCase.getCaseData(SmeCaseDefinition.class);
        assertNotNull(caseData);
        assertNotNull(caseData.getCompanyLookupInfo().getBusinessAddressInfo());
        assertEquals(address.getNumberAndStreet(),
            caseData.getCompanyLookupInfo().getBusinessAddressInfo().getNumberAndStreet());
        assertEquals(address.getApt(), caseData.getCompanyLookupInfo().getBusinessAddressInfo().getApt());
        assertEquals(address.getCity(), caseData.getCompanyLookupInfo().getBusinessAddressInfo().getCity());
        assertEquals(address.getZipCode(), caseData.getCompanyLookupInfo().getBusinessAddressInfo().getZipCode());
        assertEquals(address.getState(), caseData.getCompanyLookupInfo().getBusinessAddressInfo().getState());
    }
}
