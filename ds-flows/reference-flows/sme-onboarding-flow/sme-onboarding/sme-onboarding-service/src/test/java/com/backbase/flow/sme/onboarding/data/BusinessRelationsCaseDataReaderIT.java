package com.backbase.flow.sme.onboarding.data;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Applicant.RelationType;
import com.backbase.flow.sme.onboarding.casedata.BusinessPerson;
import com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData;
import com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData.Status;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class BusinessRelationsCaseDataReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_readBusinessRelationsCaseData_shouldBeMappedCorrectly() {
        //given
        var businessPerson = createBusinessPerson();
        var smeBusinessRelationsCaseData = new BusinessRelationsCaseData()
            .withBusinessRelationType(BusinessRelationsCaseData.BusinessRelationType.OWNER)
            .withStatus(Status.NEW)
            .withBusinessPersons(List.of(businessPerson));

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("John")
            .lastName("Doe")
            .email("john.doe@backbase.com")
            .dateOfBirth(LocalDate.now().minusYears(20))
            .relationType(RelationType.OWNER)
            .ownershipPercentage(30.0)
            .role("CEO")
            .businessRelations(smeBusinessRelationsCaseData).build();

        var caseInstance = startCase(caseDefinition);

        //when
        var businessRelationsCaseData =
            businessRelationsCaseDataReader.read(null, null, caseInstance);

        //then
        assertThat(businessRelationsCaseData).isNotNull();
        assertThat(Status.NEW).usingRecursiveComparison().isEqualTo(businessRelationsCaseData.getStatus());
        assertThat(businessPerson.getFirstName())
            .isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getFirstName());
        assertThat(businessPerson.getLastName())
            .isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getLastName());
        assertThat(businessPerson.getEmail())
            .isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getEmail());
        assertThat(businessPerson.getRelationType()).usingRecursiveComparison()
            .isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getRelationType());
        assertThat(businessPerson.getRole()).isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getRole());
        assertThat(businessPerson.getOwnershipPercentage())
            .isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getOwnershipPercentage());
        assertThat(businessPerson.getId()).isEqualTo(businessRelationsCaseData.getBusinessPersons().get(0).getId());
    }

    @Test
    @WithFlowAnonymousUser
    void canApply_validInput_returnTrue() {
        assertTrue(businessRelationsCaseDataReader
            .canApply("", null));
    }

    private BusinessPerson createBusinessPerson() {
        return new BusinessPerson()
            .withFirstName("John")
            .withLastName("Doe")
            .withEmail("john.doe@backbase.com")
            .withRelationType(BusinessPerson.RelationType.OWNER)
            .withRole("CEO")
            .withOwnershipPercentage(30.0)
            .withId(UUID.randomUUID().toString());
    }
}
