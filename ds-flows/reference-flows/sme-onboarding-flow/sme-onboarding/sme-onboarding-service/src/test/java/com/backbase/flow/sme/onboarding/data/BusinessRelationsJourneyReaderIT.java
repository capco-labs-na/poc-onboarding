package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class BusinessRelationsJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_readBusinessRelationsCaseData_shouldBeMappedCorrectly() {

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy").lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid").phoneNumber("+123456789").isRegistrar(true)
            .build();

        var caseInstance = startCase(caseDefinition);

        var businessRelationsCurrentUserData = businessRelationsJourneyReader.
            read(null, null, caseInstance);

        assertNotNull(businessRelationsCurrentUserData);
        assertEquals("Boy", businessRelationsCurrentUserData.getFirstName());
        assertEquals("Sena", businessRelationsCurrentUserData.getLastName());
        assertEquals("my-email@test.invalid", businessRelationsCurrentUserData.getEmail());
        assertEquals("+123456789", businessRelationsCurrentUserData.getPhoneNumber());
    }

    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidRegistrar_throwsException() {

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("Boy").lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .email("my-email@test.invalid").phoneNumber("+123456789")
            .build();

        var caseInstance = startCase(caseDefinition);

        assertThrows(NullPointerException.class, () -> {
            businessRelationsJourneyReader.
                read(null, null, caseInstance);
        });
    }

    @Test
    @WithFlowAnonymousUser
    void canApply_validInput_returnTrue() {
        assertTrue(businessRelationsJourneyReader
            .canApply("", null));
    }
}
