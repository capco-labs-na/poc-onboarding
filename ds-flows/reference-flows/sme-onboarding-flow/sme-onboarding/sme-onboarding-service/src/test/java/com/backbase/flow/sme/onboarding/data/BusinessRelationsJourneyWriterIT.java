package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.businessrelations.casedata.BusinessPerson;
import com.backbase.flow.businessrelations.casedata.BusinessRelationsCaseData;
import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Applicant.RelationType;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class BusinessRelationsJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void write_writeBusinessRelations_shouldUpdateCorrectly() {

        var businessRelationsCaseData = new BusinessRelationsCaseData()
            .withBusinessRelationType(BusinessRelationsCaseData.BusinessRelationType.CONTROL_PERSON)
            .withStatus(BusinessRelationsCaseData.Status.NEW);
        var userId = UUID.randomUUID().toString();
        var businessPerson = new BusinessPerson()
            .withId(userId)
            .withFirstName("abc")
            .withLastName("dba")
            .withEmail("my-emaail@test.invalid")
            .withRelationType(BusinessPerson.RelationType.OWNER)
            .withRole("CEO")
            .withOwnershipPercentage(30.0);
        businessRelationsCaseData.setBusinessPersons(List.of(businessPerson));

        var smeBusinessRelationsCaseData = new com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData();
        smeBusinessRelationsCaseData.setBusinessRelationType(
            com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData.BusinessRelationType.OWNER);

        var caseDefinition =
            SmeCaseDefBuilder.getInstance().userId(userId).firstName("abc").lastName("dba")
                .email("my-emaail@test.invalid")
                .dateOfBirth(LocalDate.now().minusYears(20)).businessRelations(smeBusinessRelationsCaseData)
                .status(com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData.Status.NEW).build();

        var caseInstance = startCase(caseDefinition);

        var smeCaseDataResult = businessRelationsJourneyWriter
            .write(businessRelationsCaseData, null, null, caseInstance);

        var caseData = smeCaseDataResult.getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertEquals(businessRelationsCaseData.getStatus().toString(),
            caseData.getBusinessRelations().getStatus().toString());
        assertEquals(businessRelationsCaseData.getBusinessRelationType().toString(),
            caseData.getBusinessRelations().getBusinessRelationType().toString());
        assertEquals(businessRelationsCaseData.getBusinessPersons().size(), caseData.getApplicants().size());
        assertEquals(businessRelationsCaseData.getBusinessPersons().get(0).getFirstName(),
            caseData.getApplicants().get(0).getFirstName());
        assertEquals(businessRelationsCaseData.getBusinessPersons().get(0).getLastName(),
            caseData.getApplicants().get(0).getLastName());
        assertEquals(businessRelationsCaseData.getBusinessPersons().get(0).getEmail(),
            caseData.getApplicants().get(0).getEmail());
        assertEquals(businessRelationsCaseData.getBusinessPersons().get(0).getId(),
            caseData.getApplicants().get(0).getId());
        assertEquals("CEO", caseData.getApplicants().get(0).getRole());
        assertEquals(30.0, caseData.getApplicants().get(0).getOwnershipPercentage());
        assertEquals(RelationType.OWNER, caseData.getApplicants().get(0).getRelationType());
    }

    @Test
    @WithFlowAnonymousUser
    void write_writeBusinessRelationsTwoPerson_shouldUpdateCorrectly() {

        var businessRelationsCaseData = new BusinessRelationsCaseData()
            .withBusinessRelationType(BusinessRelationsCaseData.BusinessRelationType.CONTROL_PERSON)
            .withStatus(BusinessRelationsCaseData.Status.NEW);
        var businessPerson1 = new BusinessPerson()
            .withFirstName("abc")
            .withLastName("dba")
            .withEmail("my-emaail@test.invalid")
            .withRelationType(BusinessPerson.RelationType.OWNER)
            .withRole("CEO")
            .withCurrentUser(true)
            .withOwnershipPercentage(30.0);
        var userId2 = UUID.randomUUID().toString();
        var businessPerson2 = new BusinessPerson()
            .withId(userId2)
            .withFirstName("uty")
            .withLastName("vcx")
            .withEmail("second-email@test.invalid")
            .withRelationType(BusinessPerson.RelationType.CONTROL_PERSON)
            .withRole("CTO")
            .withOwnershipPercentage(70.0);
        businessRelationsCaseData.setBusinessPersons(List.of(businessPerson1, businessPerson2));

        var smeBusinessRelationsCaseData
            = new com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData();
        smeBusinessRelationsCaseData.setBusinessRelationType(
            com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData.BusinessRelationType.OWNER);

        var caseDefinition =
            SmeCaseDefBuilder.getInstance().firstName("abc").lastName("dba").email("my-emaail@test.invalid")
                .dateOfBirth(LocalDate.now().minusYears(20)).isRegistrar(true)
                .businessRelations(smeBusinessRelationsCaseData)
                .status(com.backbase.flow.sme.onboarding.casedata.BusinessRelationsCaseData.Status.NEW).build();

        var caseInstance = startCase(caseDefinition);

        var smeCaseDataResult = businessRelationsJourneyWriter
            .write(businessRelationsCaseData, null, null, caseInstance);

        var caseData = smeCaseDataResult.getCaseData(SmeCaseDefinition.class);

        var businessPersonOwner = businessRelationsCaseData.getBusinessPersons().stream()
            .filter(person -> person.getFirstName().equals("abc")).findFirst().get();
        var businessPersonControl = businessRelationsCaseData.getBusinessPersons().stream()
            .filter(person -> person.getFirstName().equals("uty")).findFirst().get();

        var applicantOwner = caseData.getApplicants().stream()
            .filter(applicant -> applicant.getFirstName().equals("abc")).findFirst().get();
        var applicantControl = caseData.getApplicants().stream()
            .filter(applicant -> applicant.getFirstName().equals("uty")).findFirst().get();

        assertNotNull(caseData);
        assertEquals(businessRelationsCaseData.getStatus().toString(),
            caseData.getBusinessRelations().getStatus().toString());
        assertEquals(businessRelationsCaseData.getBusinessRelationType().toString(),
            caseData.getBusinessRelations().getBusinessRelationType().toString());
        assertEquals(businessRelationsCaseData.getBusinessPersons().size(), caseData.getApplicants().size());

        assertEquals(businessPersonOwner.getFirstName(), applicantOwner.getFirstName());
        assertEquals(businessPersonOwner.getLastName(), applicantOwner.getLastName());
        assertEquals(businessPersonOwner.getEmail(), applicantOwner.getEmail());
        assertEquals(businessPersonOwner.getId(), applicantOwner.getId());
        assertEquals(businessPersonOwner.getRole(), applicantOwner.getRole());
        assertEquals(businessPersonOwner.getOwnershipPercentage(), applicantOwner.getOwnershipPercentage());
        assertEquals(businessPersonOwner.getRelationType().toString(), applicantOwner.getRelationType().toString());

        assertEquals(businessPersonControl.getFirstName(), applicantControl.getFirstName());
        assertEquals(businessPersonControl.getLastName(), applicantControl.getLastName());
        assertEquals(businessPersonControl.getEmail(), applicantControl.getEmail());
        assertEquals(businessPersonControl.getId(), applicantControl.getId());
        assertEquals(businessPersonControl.getRole(), applicantControl.getRole());
        assertEquals(businessPersonControl.getOwnershipPercentage(), applicantControl.getOwnershipPercentage());
        assertEquals(businessPersonControl.getRelationType().toString(), applicantControl.getRelationType().toString());
    }

    @Test
    @WithFlowAnonymousUser
    void canApply_validInput_returnTrue() {
        assertTrue(businessRelationsJourneyWriter
            .canApply("", null));
    }
}
