package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.integration.service.data.MapperConstants;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.DocumentRequest;
import com.backbase.flow.sme.onboarding.casedata.DocumentRequest.Status;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class DocumentRequestDataReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_readDocumentRequests_shouldMappedCorrectly() {

        var documentRequest = new DocumentRequest()
            .withDocumentType("DBA")
            .withFilesetName("fileset")
            .withStatus(Status.OPEN)
            .withGroupId("groupId")
            .withInternalId("internalId")
            .withDeadline(OffsetDateTime.now().plusDays(7));

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .documentRequests(List.of(documentRequest))
            .firstName("John")
            .lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .userId("my-user-id")
            .email("my-email@test.invalid")
            .isRegistrar(true)
            .soleProp()
            .build();

        var caseInstance = startCase(caseDefinition);

        var data = documentRequestDataReader.read("documents", MapperConstants.CONTEXT, caseInstance);

        assertNotNull(data);

        assertEquals(1, data.getDocumentRequests().size());
        assertEquals(documentRequest.getDocumentType(), data.getDocumentRequests().get(0).getDocumentType());
        assertEquals(documentRequest.getFilesetName(), data.getDocumentRequests().get(0).getFilesetName());
        assertEquals(documentRequest.getStatus().value(), data.getDocumentRequests().get(0).getStatus().value());
        assertEquals(documentRequest.getGroupId(), data.getDocumentRequests().get(0).getGroupId());
        assertEquals(documentRequest.getInternalId(), data.getDocumentRequests().get(0).getInternalId());
        assertEquals(documentRequest.getDeadline(), data.getDocumentRequests().get(0).getDeadline());

        assertEquals("my-user-id", data.getCustomer().getUserId());
        assertEquals("my-email@test.invalid", data.getCustomer().getEmailAddress());
    }

    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidRegistrar_throwsException() {
        var documentRequest = new DocumentRequest()
            .withDocumentType("DBA")
            .withFilesetName("fileset")
            .withStatus(Status.OPEN)
            .withGroupId("groupId")
            .withInternalId("internalId")
            .withDeadline(OffsetDateTime.now().plusDays(7));

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .documentRequests(List.of(documentRequest))
            .firstName("John")
            .lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .userId("my-user-id")
            .email("my-email@test.invalid")
            .soleProp()
            .build();

        var caseInstance = startCase(caseDefinition);

        assertThrows(NullPointerException.class,
            () -> documentRequestDataReader.read("documents", MapperConstants.CONTEXT, caseInstance));
    }
}
