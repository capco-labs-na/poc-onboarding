package com.backbase.flow.sme.onboarding.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.integration.service.data.DocumentUpdateData;
import com.backbase.flow.integration.service.data.MapperConstants;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.DocumentRequest;
import com.backbase.flow.sme.onboarding.casedata.DocumentRequest.Status;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.OffsetDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class DocumentUpdateDataWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void write_updateADocumentRequest_shouldReturnUpdatedVersion() {
        var documentRequest = new DocumentRequest()
            .withDocumentType("DBA")
            .withInternalId("internalId")
            .withStatus(Status.OPEN)
            .withDeadline(OffsetDateTime.now().plusDays(7))
            .withFilesetName("fileset")
            .withProcessInstanceId("processInstanceId");

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .documentRequests(List.of(documentRequest))
            .build();

        var caseInstance = startCase(caseDefinition);

        var newDeadLine = OffsetDateTime.now().plusDays(10);
        var newStatus = com.backbase.flow.integration.service.data.DocumentRequest.Status.APPROVED;

        var documentUpdateData = new DocumentUpdateData(
            documentRequest.getInternalId(),
            newStatus,
            newDeadLine,
            documentRequest.getFilesetName(),
            documentRequest.getProcessInstanceId()
        );

        var resultCase = documentUpdateDataWriter
            .write(documentUpdateData, "documents", MapperConstants.CONTEXT, caseInstance);

        var caseData = resultCase.getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertEquals(newStatus.value(),
            caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getDocumentRequests().get(0).getStatus().value());
        assertEquals(newDeadLine,
            caseData.getCompanyLookupInfo().getBusinessDetailsInfo().getDocumentRequests().get(0).getDeadline());
    }

    @Test
    @WithFlowAnonymousUser
    void canApply_validInput_returnTrue() {
        assertTrue(documentUpdateDataWriter
            .canApply(MapperConstants.CONTEXT, null));
    }

    @Test
    @WithFlowAnonymousUser
    void canApply_invalidInput_returnFalse() {
        assertFalse(documentUpdateDataWriter
            .canApply("", null));
    }
}
