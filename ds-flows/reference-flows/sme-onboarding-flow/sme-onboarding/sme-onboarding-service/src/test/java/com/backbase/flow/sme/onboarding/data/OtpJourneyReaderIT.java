package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.service.ServiceItemOneTimePasswordAutoconfiguration.OTP_MAPPER_CONTEXT;
import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.OTP_JOURNEY_EMAIL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class OtpJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withBusiness_returnTrue() {
        boolean canApply = otpJourneyReader.canApply(OTP_MAPPER_CONTEXT, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void read_withValidData_mapToNewModel() {
        var emailAddress = "my-email@test.invalid";

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("John")
            .lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .userId("my-user-id")
            .email(emailAddress)
            .isRegistrar(true)
            .soleProp()
            .build();

        var caseInstance = startCase(caseDefinition);

        var caseData = otpJourneyReader.read(null, OTP_MAPPER_CONTEXT, caseInstance);

        assertNotNull(caseData);
        assertEquals(emailAddress, caseData.get(OTP_JOURNEY_EMAIL));
    }

    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidRegistrar_throwsException() {
        var emailAddress = "my-email@test.invalid";

        var caseDefinition = SmeCaseDefBuilder.getInstance()
            .firstName("John")
            .lastName("Sena")
            .dateOfBirth(LocalDate.parse("1970-01-01"))
            .userId("my-user-id")
            .email(emailAddress)
            .soleProp()
            .build();

        var caseInstance = startCase(caseDefinition);

        assertThrows(NullPointerException.class, () -> {
            otpJourneyReader.read(null, OTP_MAPPER_CONTEXT, caseInstance);
        });
    }
}
