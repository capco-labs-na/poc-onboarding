package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.service.ServiceItemOneTimePasswordAutoconfiguration.OTP_MAPPER_CONTEXT;
import static com.backbase.flow.service.mapper.MapperConstants.EMAIL_FIELD;
import static com.backbase.flow.service.mapper.MapperConstants.EMAIL_VERIFIED_FIELD;
import static com.backbase.flow.service.mapper.MapperConstants.PHONE_NUMBER_FIELD;
import static com.backbase.flow.service.mapper.MapperConstants.PHONE_NUMBER_VERIFIED_FIELD;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

class OtpJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void canApply_withBusiness_returnTrue() {
        boolean canApply = otpJourneyWriter.canApply(OTP_MAPPER_CONTEXT, null);

        assertTrue(canApply);
    }

    @Test
    @WithFlowAnonymousUser
    void write_withValidData_save() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("test").lastName("test")
            .dateOfBirth(LocalDate.now()).email("test@email.invalid").phoneNumber("1234567890").isRegistrar(true)
            .soleProp().build();

        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put(EMAIL_VERIFIED_FIELD, false);
        journeyMap.put(EMAIL_FIELD, "test@email.invalid");
        journeyMap.put(PHONE_NUMBER_VERIFIED_FIELD, true);
        journeyMap.put(PHONE_NUMBER_FIELD, "1234567890");

        var caseData = otpJourneyWriter
            .write(journeyMap, null, OTP_MAPPER_CONTEXT, caseInstance)
            .getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertNotNull(caseData.getApplicants().get(0).getPhoneNumberVerified());
        assertNotNull(caseData.getApplicants().get(0).getEmailVerified());
        assertTrue(caseData.getApplicants().get(0).getPhoneNumberVerified());
        assertFalse(caseData.getApplicants().get(0).getEmailVerified());
    }

    @Test
    @WithFlowAnonymousUser
    void write_withEmailVerified_save() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("test").lastName("test")
            .dateOfBirth(LocalDate.now()).email("test@email.invalid").phoneNumber("1234567890").isRegistrar(true)
            .soleProp().build();

        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put(EMAIL_VERIFIED_FIELD, false);
        journeyMap.put(EMAIL_FIELD, "test@email.invalid");

        var caseData = otpJourneyWriter
            .write(journeyMap, null, OTP_MAPPER_CONTEXT, caseInstance)
            .getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertNotNull(caseData.getApplicants().get(0).getEmailVerified());
        assertFalse(caseData.getApplicants().get(0).getEmailVerified());
    }

    @Test
    @WithFlowAnonymousUser
    void write_withPhoneNumberVerified_save() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("test").lastName("test")
            .dateOfBirth(LocalDate.now()).email("test@email.invalid").phoneNumber("1234567890").isRegistrar(true)
            .soleProp().build();

        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put(PHONE_NUMBER_VERIFIED_FIELD, true);
        journeyMap.put(PHONE_NUMBER_FIELD, "1234567890");

        var caseData = otpJourneyWriter
            .write(journeyMap, null, OTP_MAPPER_CONTEXT, caseInstance)
            .getCaseData(SmeCaseDefinition.class);

        assertNotNull(caseData);
        assertNotNull(caseData.getApplicants().get(0).getPhoneNumberVerified());
        assertTrue(caseData.getApplicants().get(0).getPhoneNumberVerified());
    }

    @Test
    @WithFlowAnonymousUser
    void write_whenInvalidRegistrar_throwsException() {
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("test").lastName("test")
            .dateOfBirth(LocalDate.now()).email("test@email.invalid").phoneNumber("1234567890").soleProp().build();

        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put(EMAIL_VERIFIED_FIELD, false);
        journeyMap.put(PHONE_NUMBER_VERIFIED_FIELD, true);

        assertThrows(NullPointerException.class, () -> otpJourneyWriter
            .write(journeyMap, null, OTP_MAPPER_CONTEXT, caseInstance));
    }
}
