package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.PERSONAL_ADDRESS_CONTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Address;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import org.junit.jupiter.api.Test;

class PersonalAddressJourneyReaderIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void reader_readPersonalAddress_shouldMappedCorrectly() {
        var personalAddress = new Address()
            .withState("AZ")
            .withCity("Phoenix")
            .withNumberAndStreet("17 E Jefferson St")
            .withApt("41")
            .withZipCode("22222");
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("b").lastName("b")
            .dateOfBirth(LocalDate.now()).email("test@bb.com").isRegistrar(true).personalAddress(personalAddress)
            .soleProp().build();
        var caseInstance = startCase(caseDefinition);
        var caseData = personalAddressJourneyReader.read("address", PERSONAL_ADDRESS_CONTEXT, caseInstance);
        var addressMap = (LinkedHashMap) caseData.get("address");
        assertNotNull(addressMap);
        assertEquals(personalAddress.getState(), addressMap.get("state"));
        assertEquals(personalAddress.getCity(), addressMap.get("city"));
        assertEquals(personalAddress.getNumberAndStreet(), addressMap.get("numberAndStreet"));
        assertEquals(personalAddress.getApt(), addressMap.get("apt"));
        assertEquals(personalAddress.getZipCode(), addressMap.get("zipCode"));
    }

    @Test
    @WithFlowAnonymousUser
    void read_whenInvalidRegistrar_throwsException() {
        var personalAddress = new Address()
            .withState("AZ")
            .withCity("Phoenix")
            .withNumberAndStreet("17 E Jefferson St")
            .withApt("41")
            .withZipCode("22222");
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("b").lastName("b")
            .dateOfBirth(LocalDate.now()).email("test@bb.com").personalAddress(personalAddress)
            .soleProp().build();
        var caseInstance = startCase(caseDefinition);

        assertThrows(NullPointerException.class,
            () -> personalAddressJourneyReader.read("address", PERSONAL_ADDRESS_CONTEXT, caseInstance));
    }

}
