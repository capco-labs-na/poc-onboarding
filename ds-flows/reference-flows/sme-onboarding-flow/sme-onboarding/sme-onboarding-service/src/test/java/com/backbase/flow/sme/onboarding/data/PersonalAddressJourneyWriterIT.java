package com.backbase.flow.sme.onboarding.data;

import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.PERSONAL_ADDRESS_CONTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Address;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

class PersonalAddressJourneyWriterIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void read_writeAddress_shouldUpdatedCorrectly() {
        var personalAddress = new Address()
            .withState("AZ")
            .withCity("Phoenix")
            .withNumberAndStreet("17 E Jefferson St")
            .withApt("41")
            .withZipCode("22222");
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("b").lastName("b")
            .dateOfBirth(LocalDate.now()).email("test@bb.com").isRegistrar(true).personalAddress(personalAddress)
            .soleProp().build();
        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put("state", personalAddress.getState());
        journeyMap.put("city", personalAddress.getCity());
        journeyMap.put("numberAndStreet", personalAddress.getNumberAndStreet());
        journeyMap.put("apt", personalAddress.getApt());
        journeyMap.put("zipCode", personalAddress.getZipCode());
        var resultCase = personalAddressJourneyWriter
            .write(journeyMap, "address", PERSONAL_ADDRESS_CONTEXT, caseInstance);
        var caseData = resultCase.getCaseData(SmeCaseDefinition.class);
        assertNotNull(caseData);
        assertNotNull(caseData.getApplicants().get(0).getPersonalAddress());
        assertEquals(personalAddress.getState(), caseData.getApplicants().get(0).getPersonalAddress().getState());
        assertEquals(personalAddress.getCity(), caseData.getApplicants().get(0).getPersonalAddress().getCity());
        assertEquals(personalAddress.getNumberAndStreet(),
            caseData.getApplicants().get(0).getPersonalAddress().getNumberAndStreet());
        assertEquals(personalAddress.getApt(), caseData.getApplicants().get(0).getPersonalAddress().getApt());
        assertEquals(personalAddress.getZipCode(), caseData.getApplicants().get(0).getPersonalAddress().getZipCode());
    }

    @Test
    @WithFlowAnonymousUser
    void write_whenInvalidRegistrar_throwsException() {
        var personalAddress = new Address()
            .withState("AZ")
            .withCity("Phoenix")
            .withNumberAndStreet("17 E Jefferson St")
            .withApt("41")
            .withZipCode("22222");
        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("b").lastName("b")
            .dateOfBirth(LocalDate.now()).email("test@bb.com").personalAddress(personalAddress)
            .soleProp().build();
        var caseInstance = startCase(caseDefinition);
        Map<String, Object> journeyMap = new HashMap<>();
        journeyMap.put("state", personalAddress.getState());
        journeyMap.put("city", personalAddress.getCity());
        journeyMap.put("numberAndStreet", personalAddress.getNumberAndStreet());
        journeyMap.put("apt", personalAddress.getApt());
        journeyMap.put("zipCode", personalAddress.getZipCode());

        assertThrows(NullPointerException.class, () -> personalAddressJourneyWriter
            .write(journeyMap, "address", PERSONAL_ADDRESS_CONTEXT, caseInstance));
    }
}
