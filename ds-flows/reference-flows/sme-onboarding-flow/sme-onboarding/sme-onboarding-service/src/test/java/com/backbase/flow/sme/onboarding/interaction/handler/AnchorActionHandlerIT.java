package com.backbase.flow.sme.onboarding.interaction.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.interaction.api.model.InteractionRequestDto;
import com.backbase.flow.interaction.engine.data.model.InteractionInstance;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.TestConstants;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.interaction.model.AnchorRequestDto;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;
import javax.servlet.http.Cookie;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class AnchorActionHandlerIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withValidData_shouldSave() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(), "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var anchorRequestDto = AnchorRequestDto.builder()
            .firstName("b")
            .lastName("b")
            .dateOfBirth("1970-01-01")
            .emailAddress("test@bb.com")
            .build();
        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(anchorRequestDto);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors").isEmpty())
            .andExpect(jsonPath("$.step.name", Matchers.is("otp-verification")));

        var smeCase = getCaseData(interactionInstance.getCaseKey());
        var applicant = smeCase.getApplicants().stream().filter(Applicant::getIsRegistrar).findFirst().get();

        assertThat(smeCase).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo()).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo().getBusinessDetailsInfo()).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo().getBusinessStructureInfo()).isNotNull();
        assertThat(applicant).isNotNull();
        assertThat(applicant.getFirstName()).isEqualTo("b");
        assertThat(applicant.getLastName()).isEqualTo("b");
        assertThat(applicant.getDateOfBirth()).isEqualTo(LocalDate.parse("1970-01-01"));
        assertThat(applicant.getEmail()).isEqualTo("test@bb.com");
        assertTrue(applicant.getIsRegistrar());
    }

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withPreviousData_shouldOverride() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().firstName("b").lastName("b").dateOfBirth(LocalDate.now())
                .email("test@bb.com").build(), "sme-onboarding-anchor", UUID.randomUUID().toString());

        var anchorRequestDto = AnchorRequestDto.builder()
            .firstName("b2")
            .lastName("b2")
            .dateOfBirth("1970-01-01")
            .emailAddress("test2@bb.com")
            .build();
        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(anchorRequestDto);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors").isEmpty())
            .andExpect(jsonPath("$.step.name", Matchers.is("otp-verification")));

        var smeCase = getCaseData(interactionInstance.getCaseKey());
        var applicant = smeCase.getApplicants().stream()
            .filter(a -> Objects.nonNull(a.getIsRegistrar()))
            .filter(Applicant::getIsRegistrar).findFirst().get();

        assertThat(smeCase).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo()).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo().getBusinessDetailsInfo()).isNotNull();
        assertThat(smeCase.getCompanyLookupInfo().getBusinessStructureInfo()).isNotNull();
        assertThat(applicant).isNotNull();
        assertThat(applicant.getFirstName()).isEqualTo("b2");
        assertThat(applicant.getLastName()).isEqualTo("b2");
        assertThat(applicant.getDateOfBirth()).isEqualTo(LocalDate.parse("1970-01-01"));
        assertThat(applicant.getEmail()).isEqualTo("test2@bb.com");
    }

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withUnknownInteraction_shouldReturnNotFound() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(), "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(UUID.randomUUID().toString());
        interactionRequestDto.setPayload(null);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isNotFound());
    }


    @Test
    @WithFlowAnonymousUser
    void postInteraction_withBlankPayload_shouldReturnActionErrors() throws Exception {
        InteractionInstance interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(), "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(null);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors[0].message", Matchers.is("Input payload is invalid.")));
    }

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withInvalidPayload_shouldReturnActionErrors() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(),
            "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var anchorRequestDto = AnchorRequestDto.builder()
            .dateOfBirth("1970-01/01")
            .emailAddress("test/bb.com")
            .build();
        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(anchorRequestDto);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors", hasSize(4)))
            .andExpect(jsonPath("$.actionErrors[*].context.firstName").value(contains("The value should not be null.")))
            .andExpect(jsonPath("$.actionErrors[*].context.lastName").value(contains("The value should not be null.")))
            .andExpect(jsonPath("$.actionErrors[*].context.dateOfBirth")
                .value(contains("Invalid date (format). Date should be in format yyyy-MM-dd")))
            .andExpect(
                jsonPath("$.actionErrors[*].context.emailAddress")
                    .value(contains("Please enter a valid email address.")));
    }

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withInvalidTooYoungAge_shouldReturnActionErrors() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(), "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var anchorRequestDto = AnchorRequestDto.builder()
            .firstName("b")
            .lastName("b")
            .dateOfBirth(LocalDate.now().minusYears(17).toString())
            .emailAddress("test@bb.com")
            .build();
        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(anchorRequestDto);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors[0].key", Matchers.is("SME_003")))
            .andExpect(jsonPath("$.actionErrors[0].message", Matchers.is("Anchor data submission failed.")))
            .andExpect(jsonPath("$.actionErrors[*].context.dateOfBirth")
                .value(contains("Customer's age should be between 18 and 100 years old.")));
    }

    @Test
    @WithFlowAnonymousUser
    void postInteraction_withInvalidTooOldAge_shouldReturnActionErrors() throws Exception {
        var interactionInstance = createInteractionInstance(
            SmeCaseDefBuilder.getInstance().build(), "sme-onboarding-anchor",
            UUID.randomUUID().toString());

        var anchorRequestDto = AnchorRequestDto.builder()
            .firstName("b")
            .lastName("b")
            .dateOfBirth(LocalDate.now().minusYears(101).toString())
            .emailAddress("test@bb.com")
            .build();
        var interactionRequestDto = new InteractionRequestDto();
        interactionRequestDto.setInteractionId(interactionInstance.getId().toString());
        interactionRequestDto.setPayload(anchorRequestDto);

        mockMvc.perform(post(
            TestConstants.ACTION_URL, TestConstants.INTERACTION_SME_ONBOARDING,
            TestConstants.ACTION_SOLE_PROPRIETORSHIP_ANCHOR)
            .cookie(new Cookie("anonymousUserId", interactionInstance.getUserId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(interactionRequestDto)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.interactionId").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors").isNotEmpty())
            .andExpect(jsonPath("$.actionErrors[0].key", Matchers.is("SME_003")))
            .andExpect(jsonPath("$.actionErrors[0].message", Matchers.is("Anchor data submission failed.")))
            .andExpect(jsonPath("$.actionErrors[*].context.dateOfBirth")
                .value(contains("Customer's age should be between 18 and 100 years old.")));
    }
}
