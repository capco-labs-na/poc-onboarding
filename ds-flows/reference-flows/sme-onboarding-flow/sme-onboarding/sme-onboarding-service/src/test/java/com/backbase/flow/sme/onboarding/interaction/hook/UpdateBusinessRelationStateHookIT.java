package com.backbase.flow.sme.onboarding.interaction.hook;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class UpdateBusinessRelationStateHookIT extends BaseIntegrationIT {

    private final InteractionContext interactionContext = mock(InteractionContext.class);
    private UpdateBusinessRelationStateHook updateBusinessRelationStateHook = new UpdateBusinessRelationStateHook();

    @Test
    @WithFlowAnonymousUser
    void execute_withValidData_shouldSave() {

        var caseDefinition = SmeCaseDefBuilder.getInstance().firstName("b").lastName("b")
            .dateOfBirth(LocalDate.parse("1970-01-01")).email("test@bb.com").phoneNumber("5555555").isRegistrar(true)
            .soleProp().businessRelationsState(SmeCaseDefinition.BusinessRelationsState.NOT_DECIDED).build();
        when(interactionContext.getCaseData(SmeCaseDefinition.class)).thenReturn(caseDefinition);
        updateBusinessRelationStateHook.executeHook(interactionContext);
        assertThat(caseDefinition.getBusinessRelationsState().toString())
            .hasToString(SmeCaseDefinition.BusinessRelationsState.FINISHED.toString());
    }
}
