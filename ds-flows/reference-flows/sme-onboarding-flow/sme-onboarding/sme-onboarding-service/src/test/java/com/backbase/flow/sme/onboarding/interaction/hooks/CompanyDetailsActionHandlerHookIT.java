package com.backbase.flow.sme.onboarding.interaction.hooks;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.interaction.hook.CompanyDetailsActionHandlerHook;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CompanyDetailsActionHandlerHookIT extends BaseIntegrationIT {

    private CompanyDetailsActionHandlerHook companyDetailsActionHandlerHook;
    private InteractionContext context;
    private UUID caseKey;

    @BeforeEach
    public void before() {
        companyDetailsActionHandlerHook = new CompanyDetailsActionHandlerHook(flowSecurityContext, taskService,
            flowTaskService);

        context = mock(InteractionContext.class);

        var caseInstance = startCase(SmeCaseDefBuilder.getInstance().soleProp()
            .firstName("John").lastName("Doe").email("john@email.invalid").isRegistrar(true)
            .businessLegalName("legal").businessKnownName("potential_match").businessOperationState("AR").build());

        caseKey = caseInstance.getKey();
        flowProcessService.startProcess(caseInstance.getKey(), ProcessConstants.BPM_KYB);

        when(context.getCaseKey()).thenReturn(caseKey);
    }

    @Test
    @WithFlowAnonymousUser
    void execute_withNotCompletedUserTask_returnTrue() {
        assertTrue(companyDetailsActionHandlerHook.executeHook(context).isContinueWithHookEvaluation());
    }

    @Test
    @WithFlowAnonymousUser
    void execute_withCompletedUserTask_returnFalse() {
        completeUserTask(caseKey.toString(), ProcessConstants.BPM_KYB, ProcessConstants.COMPANY_DETAILS_USER_TASK_ID);

        assertFalse(companyDetailsActionHandlerHook.executeHook(context).isContinueWithHookEvaluation());
    }
}
