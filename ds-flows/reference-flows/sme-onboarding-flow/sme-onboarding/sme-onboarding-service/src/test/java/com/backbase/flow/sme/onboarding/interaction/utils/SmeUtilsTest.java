package com.backbase.flow.sme.onboarding.interaction.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@SuppressWarnings("rawtypes")
class SmeUtilsTest {

    @Test
    void validateAndReturnRegistrar_whenRegistrarSet_returnRegistrar() {
        var dob = LocalDate.now();
        var smeCase = new SmeCaseDefinition()
            .withApplicants(
                List.of(new Applicant()
                    .withFirstName("Name")
                    .withLastName("LastName")
                    .withDateOfBirth(LocalDate.now())
                    .withIsRegistrar(true)
                    .withEmail("test@gmail.com")
                )
            );

        var applicant = SmeUtils.validateAndReturnRegistrar(smeCase);

        assertSoftly(softly -> {
            softly.assertThat(applicant)
                .isNotNull();
            softly.assertThat(applicant.isPresent())
                .isTrue();
            var appl = applicant.get();
            softly.assertThat(appl.getFirstName())
                .isEqualTo("Name");
            softly.assertThat(appl.getLastName())
                .isEqualTo("LastName");
            softly.assertThat(appl.getDateOfBirth())
                .isEqualTo(LocalDate.now());
            softly.assertThat(appl.getEmail())
                .isEqualTo("test@gmail.com");
            softly.assertThat(appl.getIsRegistrar())
                .isTrue();
        });
    }

    @ParameterizedTest
    @MethodSource("invalidSmeApplicants")
    void validateAndReturnRegistrar_whenRegistrarNotSet_returnEmptyOptional(SmeCaseDefinition smeCase) {
        var applicant = SmeUtils.validateAndReturnRegistrar(smeCase);

        assertThat(applicant)
            .isEmpty();
    }

    private static Stream<Arguments> invalidSmeApplicants() {
        return Stream.of(
            Arguments.of(new SmeCaseDefinition()),
            Arguments.of(new SmeCaseDefinition().withApplicants(null)),
            Arguments.of(new SmeCaseDefinition().withApplicants(Collections.emptyList())),
            Arguments.of(new SmeCaseDefinition().withApplicants(Collections.singletonList(null))),
            Arguments.of(new SmeCaseDefinition().withApplicants(List.of(new Applicant()))),
            Arguments.of(new SmeCaseDefinition().withApplicants(List.of(new Applicant().withIsRegistrar(null)))),
            Arguments.of(new SmeCaseDefinition().withApplicants(List.of(new Applicant().withIsRegistrar(false))))
        );

    }
}
