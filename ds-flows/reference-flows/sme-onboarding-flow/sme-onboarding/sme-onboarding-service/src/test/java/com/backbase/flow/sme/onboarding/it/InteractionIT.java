package com.backbase.flow.sme.onboarding.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.DateUtil.parseDatetime;

import com.backbase.flow.integration.domain.companylookup.dto.BusinessStructureDto;
import com.backbase.flow.interaction.api.model.InteractionRequestDto;
import com.backbase.flow.interaction.api.model.InteractionResponseDto;
import com.backbase.flow.service.companylookup.domain.BusinessDetailsDto;
import com.backbase.flow.service.dto.OtpChannel;
import com.backbase.flow.service.dto.OtpDto;
import com.backbase.flow.service.productselection.interaction.dto.SelectProductRequest;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.TestConstants;
import com.backbase.flow.sme.onboarding.casedata.Applicant;
import com.backbase.flow.sme.onboarding.casedata.BusinessStructureInfo;
import com.backbase.flow.sme.onboarding.casedata.CompanyLookupInfo;
import com.backbase.flow.sme.onboarding.casedata.ProductSelection;
import com.backbase.flow.sme.onboarding.casedata.SelectedProduct;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import com.backbase.flow.sme.onboarding.interaction.model.AnchorRequestDto;
import com.backbase.flow.sme.onboarding.interaction.model.TermsConditionsRequestDto;
import com.backbase.flow.sme.onboarding.interaction.model.TermsConditionsResponseDto;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.Setter;

abstract class InteractionIT extends BaseIntegrationIT {

    private static final String FIRST_NAME = "John";
    private static final String LAST_NAME = "Doe";
    private static final LocalDate DATE_OF_BIRTH = LocalDate.now().minusYears(21);
    private static final String EMAIL = "john@doe.com";
    private static final String OTP_CHANNEL = OtpChannel.EMAIL.toString();

    private static final String PRODUCT_ITEM_REFERENCE_ID = "product-1";
    private static final String PRODUCT_ITEM_NAME = "Business Account 1";
    private static final SelectProductRequest.ProductItem PRODUCT_ITEM = new SelectProductRequest.ProductItem(
        PRODUCT_ITEM_REFERENCE_ID, PRODUCT_ITEM_NAME, null);

    private static final String LEGAL_NAME = "BackBase";
    private static final String STATE = "AL";
    private static final Date DATE_ESTABLISHED = Date.from(
        LocalDate.now().minusYears(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    private static final BusinessStructureDto SOLE_PROPRIETORSHIP =
        new BusinessStructureDto(BusinessStructureInfo.Type.SOLE_PROPRIETORSHIP.value(), List.of());
    private static final BusinessStructureDto PARTNERSHIP =
        new BusinessStructureDto(BusinessStructureInfo.Type.PARTNERSHIP.value(), List.of(
            BusinessStructureInfo.Subtype.LIMITED.value(),
            BusinessStructureInfo.Subtype.GENERAL.value(),
            BusinessStructureInfo.Subtype.JOINT_VENTURE.value()));
    private static final BusinessStructureDto LLC =
        new BusinessStructureDto(BusinessStructureInfo.Type.LLC.value(), List.of(
            BusinessStructureInfo.Subtype.SINGLE_MEMBER.value(),
            BusinessStructureInfo.Subtype.MULTIPLE_MEMBER.value()));
    private static final BusinessStructureDto CORPORATION =
        new BusinessStructureDto(BusinessStructureInfo.Type.CORPORATION.value(), List.of(
            BusinessStructureInfo.Subtype.S_CORP.value(),
            BusinessStructureInfo.Subtype.C_CORP.value(),
            BusinessStructureInfo.Subtype.B_CORP.value(),
            BusinessStructureInfo.Subtype.NON_PROFIT.value(),
            BusinessStructureInfo.Subtype.CLOSE_CORP.value()));

    private static final Map<BusinessStructureInfo.Type, BusinessStructureDto> BUSINESS_STRUCTURES = Map.of(
        BusinessStructureInfo.Type.SOLE_PROPRIETORSHIP, SOLE_PROPRIETORSHIP,
        BusinessStructureInfo.Type.PARTNERSHIP, PARTNERSHIP,
        BusinessStructureInfo.Type.LLC, LLC,
        BusinessStructureInfo.Type.CORPORATION, CORPORATION);

    @Setter
    protected UUID caseKey;

    @Setter
    protected String interactionId;

    abstract List<StepAction> happyPath();

    abstract String interactionName();

    protected InteractionResponseDto performWalkthroughRequest(String expectedStep) {
        var interactionResponse = action("sme-onboarding-init", interactionId, new TermsConditionsRequestDto(true));
        checkInteractionResponse(interactionResponse, expectedStep);
        setInteractionId(interactionResponse.getInteractionId());
        return interactionResponse;
    }

    protected void checkWalkthrough(InteractionResponseDto interactionResponse) {
        var onboardingInitResponse = mapToObject(interactionResponse.getBody(), TermsConditionsResponseDto.class);
        setCaseKey(UUID.fromString(onboardingInitResponse.getCaseKey()));
        assertThat(onboardingInitResponse.isAccepted()).isTrue();
        assertThat(onboardingInitResponse.isAnchorDataReceived()).isFalse();
    }

    protected void performAnchorDataRequest(String expectedStep) {
        performAnchorDataRequest(FIRST_NAME, expectedStep);
    }

    private void performAnchorDataRequest(String firstName, String expectedStep) {
        var anchorDataRequest = AnchorRequestDto.builder()
            .firstName(firstName)
            .lastName(LAST_NAME)
            .dateOfBirth(DATE_OF_BIRTH.toString())
            .emailAddress(EMAIL)
            .build();
        var interactionResponse = action("sme-onboarding-anchor-data", interactionId, anchorDataRequest);
        checkInteractionResponse(interactionResponse, expectedStep);
    }

    protected void checkAnchorData() {
        assertThat(getCaseData(caseKey).getApplicants())
            .hasSize(1)
            .first()
            .usingComparator(
                Comparator.comparing(Applicant::getFirstName)
                    .thenComparing(Applicant::getLastName)
                    .thenComparing(Applicant::getDateOfBirth)
                    .thenComparing(Applicant::getEmail)
            )
            .isEqualTo(
                new Applicant()
                    .withFirstName(FIRST_NAME)
                    .withLastName(LAST_NAME)
                    .withDateOfBirth(DATE_OF_BIRTH)
                    .withEmail(EMAIL)
            );
    }

    protected void performAvailableOtpChannelsRequest() {
        var interactionResponse = action("available-otp-channels", interactionId, null);
        checkInteractionResponse(interactionResponse, "otp-verification");
        //noinspection unchecked
        var channels = (List<String>) interactionResponse.getBody();
        assertThat(channels).contains(OTP_CHANNEL);
    }

    protected void performFetchOtpDataRequest() {
        var interactionResponse = action("fetch-otp-email", interactionId, null);
        checkInteractionResponse(interactionResponse, "otp-verification");
        var applicant = mapToObject(interactionResponse.getBody(), Applicant.class);
        assertThat(applicant.getEmail()).isEqualTo(EMAIL);
    }

    protected void performRequestOtpRequest() {
        var otpRequest = OtpDto.builder()
            .channel(OTP_CHANNEL)
            .email(EMAIL)
            .build();
        var interactionResponse = action("request-otp", interactionId, otpRequest);
        checkInteractionResponse(interactionResponse, "otp-verification");
    }

    protected void performVerifyOtpRequest(String expectedStep) {
        var otpRequest = OtpDto.builder()
            .channel(OTP_CHANNEL)
            .email(EMAIL)
            .otp("123456")
            .build();
        var interactionResponse = action("verify-otp", interactionId, otpRequest);
        checkInteractionResponse(interactionResponse, expectedStep);
    }

    protected void checkOtp() {
        assertThat(getCaseData(caseKey).getApplicants())
            .hasSize(1)
            .first()
            .usingComparator(
                Comparator.comparing(Applicant::getEmail)
                    .thenComparing(Applicant::getEmailVerified)
            )
            .isEqualTo(
                new Applicant()
                    .withEmail(EMAIL)
                    .withEmailVerified(true)
            );
    }

    protected void performCheckCaseRequest(String expectedStep) {
        var interactionResponse = action("sme-onboarding-check-case-exist", interactionId, null);
        checkInteractionResponse(interactionResponse, expectedStep);
    }

    protected void checkCheckCase() {
        checkAnchorData();
    }

    protected void performGetProductList() {
        var getProductListRequest = new InteractionRequestDto();
        var interactionResponse = action("get-product-list", interactionId, getProductListRequest);
        checkInteractionResponse(interactionResponse, "select-products");
        setInteractionId(interactionResponse.getInteractionId());
    }

    protected void performProductSelectionRequest(String expectedStep) {
        var productSelectionRequest = new SelectProductRequest();
        productSelectionRequest.setSelection(List.of(PRODUCT_ITEM));
        var interactionResponse = action("select-products", interactionId, productSelectionRequest);
        checkInteractionResponse(interactionResponse, expectedStep);
    }

    protected void checkProductSelection() {
        assertThat(getCaseData(caseKey).getProductSelection())
            .isNotNull()
            .extracting(ProductSelection::getSelectedProducts)
            .asList()
            .hasSize(1)
            .first()
            .isEqualTo(
                new SelectedProduct()
                    .withReferenceId(PRODUCT_ITEM.getReferenceId())
                    .withProductName(PRODUCT_ITEM.getProductName()
                    )
            );
    }

    protected void performGetBusinessStructuresList() {
        var collectionResponse = getCollectionItems("business-structures", HashMap.class);
        checkBusinessStructureCollections(mapToList(collectionResponse, BusinessStructureDto.class));
    }

    protected void checkBusinessStructureCollections(List<BusinessStructureDto> businessStructures) {
        assertThat(businessStructures)
            .hasSize(4)
            .containsExactlyInAnyOrderElementsOf(BUSINESS_STRUCTURES.values());
        assertThat(businessStructures).containsExactlyInAnyOrderElementsOf(BUSINESS_STRUCTURES.values());
    }

    protected void performSoleProprietorshipBusinessStructureRequest(String expectedStep) {
        var businessStructureRequest = BUSINESS_STRUCTURES.get(BusinessStructureInfo.Type.SOLE_PROPRIETORSHIP);
        new BusinessStructureDto(BusinessStructureInfo.Type.SOLE_PROPRIETORSHIP.value(), null);
        var interactionResponse = action("business-structure", interactionId, businessStructureRequest);
        checkInteractionResponse(interactionResponse, expectedStep);
        setInteractionId(interactionResponse.getInteractionId());
    }

    protected void checkCompanyLookupWillBeNotExecuted() {
        assertThat(getCaseData(caseKey).getCompanyLookupInfo())
            .isNotNull()
            .extracting(info -> info.getBusinessStructureInfo().getType(), CompanyLookupInfo::getPerformCompanyLookup)
            .isEqualTo(List.of(BusinessStructureInfo.Type.SOLE_PROPRIETORSHIP, false));
    }

    protected void performBusinessDetailsRequest(String expectedStep) {
        var caseData = getCaseData(caseKey);
        var businessStructureInfo = caseData.getCompanyLookupInfo().getBusinessStructureInfo();
        var businessDetailsRequest =
            new BusinessDetailsDto(toCompanyLookupBusinessStructureInfo(businessStructureInfo), LEGAL_NAME, null, null,
                DATE_ESTABLISHED, STATE);
        var interactionResponse = action("business-details", interactionId, businessDetailsRequest);
        checkInteractionResponse(interactionResponse, expectedStep);
        setInteractionId(interactionResponse.getInteractionId());
    }

    private com.backbase.flow.service.companylookup.casedata.BusinessStructureInfo toCompanyLookupBusinessStructureInfo(
        BusinessStructureInfo businessStructureInfo) {
        var companyLookupBusinessStructureInfo =
            new com.backbase.flow.service.companylookup.casedata.BusinessStructureInfo();
        companyLookupBusinessStructureInfo.setType(
            businessStructureInfo.getType() != null ? businessStructureInfo.getType().value() : null);
        companyLookupBusinessStructureInfo.setSubtype(
            businessStructureInfo.getSubtype() != null ? businessStructureInfo.getSubtype().value() : null);
        return companyLookupBusinessStructureInfo;
    }

    protected void checkBusinessDetails() {
        var caseData = getCaseData(caseKey);
        var businessDetailsInfo = caseData.getCompanyLookupInfo().getBusinessDetailsInfo();
        assertThat(businessDetailsInfo.getLegalName()).isEqualTo(LEGAL_NAME);
        assertThat(parseDatetime(businessDetailsInfo.getDateEstablished())).isEqualToIgnoringMinutes(DATE_ESTABLISHED);
        assertThat(businessDetailsInfo.getStateOperatingIn()).isEqualTo(STATE);
    }

    protected void performBusinessAddressRequest(String expectedStep) {
    }

    protected void checkBusinessAddress() {
        var caseData = getCaseData(caseKey);
    }

    protected InteractionResponseDto action(String action, String interactionId, Object payload) {
        return interaction(
            TestConstants.INTERACTION_SME_ONBOARDING, action, interactionId, payload);
    }

    protected void checkProcessState(String processState) {
        var process = historyService.createHistoricProcessInstanceQuery()
            .processDefinitionKey(ProcessConstants.BPM_DATA_GATHERING)
            .variableValueEquals(ProcessConstants.PROCESS_VARIABLE_CASE_KEY, caseKey.toString())
            .singleResult();
        assertThat(process.getState()).isEqualTo(processState);
    }

    protected void happyPathUntil(Step step) {
        happyPath().stream()
            .takeWhile(stepAction -> !step.equals(stepAction.getStep()))
            .map(StepAction::getAction)
            .forEach(Runnable::run);
    }
}
