package com.backbase.flow.sme.onboarding.it;

import java.util.List;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.junit.jupiter.api.Test;

class SmeOnboardingIT extends InteractionIT {

    @Test
    void smeOnboarding_Soleprop_fullHappyPath() {
        happyPathUntil(Step.DONE);
        checkProcessState(HistoricProcessInstance.STATE_ACTIVE);
    }

    @Override
    List<StepAction> happyPath() {
        return List.of(
            new StepAction(Step.WALKTHROUGH, () -> {
                var onboardingInitResponse = performWalkthroughRequest("sme-onboarding-anchor");
                checkWalkthrough(onboardingInitResponse);
            }),
            new StepAction(Step.ANCHOR_DATA, () -> {
                performAnchorDataRequest("otp-verification");
                checkAnchorData();
            }),
            new StepAction(Step.OTP, () -> {
                performAvailableOtpChannelsRequest();
                performFetchOtpDataRequest();
                performRequestOtpRequest();
                performVerifyOtpRequest("sme-onboarding-check-case");
                checkOtp();
            }),
            new StepAction(Step.CHECK_CASE, () -> {
                performCheckCaseRequest("select-products");
                checkCheckCase();
            }),
            new StepAction(Step.PRODUCT_SELECTION, () -> {
                performGetProductList();
                performProductSelectionRequest("business-structure");
                checkProductSelection();
            }),
            new StepAction(Step.BUSINESS_STRUCTURE, () -> {
                performGetBusinessStructuresList();
                performSoleProprietorshipBusinessStructureRequest("business-details");
                checkCompanyLookupWillBeNotExecuted();
            }),
            new StepAction(Step.BUSINESS_DETAILS, () -> {
                performBusinessDetailsRequest("business-address");
                checkBusinessDetails();
            }),
            new StepAction(Step.BUSINESS_ADDRESS, () -> {
                performBusinessAddressRequest("business-identity");
                checkBusinessAddress();
            })
        );
    }

    @Override
    String interactionName() {
        return "sme-onboarding";
    }
}
