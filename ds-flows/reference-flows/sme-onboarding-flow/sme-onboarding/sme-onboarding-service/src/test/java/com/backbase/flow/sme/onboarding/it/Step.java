package com.backbase.flow.sme.onboarding.it;

enum Step {
    WALKTHROUGH,
    ANCHOR_DATA,
    OTP,
    CHECK_CASE,
    PRODUCT_SELECTION,
    BUSINESS_STRUCTURE,
    COMPANY_LOOKUP,
    BUSINESS_DETAILS,
    BUSINESS_ADDRESS,
    DONE
}
