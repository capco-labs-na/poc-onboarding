package com.backbase.flow.sme.onboarding.it;

import lombok.Value;

@Value
class StepAction {

    Step step;
    Runnable action;
}
