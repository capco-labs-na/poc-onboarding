package com.backbase.flow.sme.onboarding.process;

import static com.backbase.flow.sme.onboarding.TestConstants.AML_PENDING_EVENT;
import static com.backbase.flow.sme.onboarding.TestConstants.AML_SUCCEED_EVENT;
import static com.backbase.flow.sme.onboarding.TestConstants.APPLICANT_ONBOARDING_FINISHED_EVENT;
import static com.backbase.flow.sme.onboarding.TestConstants.APPLICANT_ONBOARDING_PENDING_EVENT;
import static com.backbase.flow.sme.onboarding.constants.CaseDefinitionConstants.APPLICANT_ID;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.backbase.flow.iam.util.WithFlowAnonymousUser;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.builder.SmeCaseDefBuilder;
import com.backbase.flow.sme.onboarding.casedata.IdentityVerificationResult;
import com.backbase.flow.sme.onboarding.casedata.TermsAndConditions;
import com.backbase.flow.sme.onboarding.constants.ProcessConstants;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class AppOnboardingProcessIT extends BaseIntegrationIT {

    @Test
    @WithFlowAnonymousUser
    void startProcess_happyFlow_returnSuccess() {
        //given
        var caseDef = SmeCaseDefBuilder.getInstance().soleProp().build();
        final var mainApplicantId = UUID.randomUUID();

        caseDef.setApplicants(List.of(getMainApplicant(mainApplicantId)));
        caseDef.setTermsAndConditions(new TermsAndConditions()
            .withAccepted(true)
            .withVersion("1")
            .withAcceptanceDate(OffsetDateTime.now())
        );
        caseDef.setIdentityVerificationResult(new IdentityVerificationResult()
            .withAdditionalProperty("verificationId", "approved")
            .withAdditionalProperty("documentStatus", "documentStatus")
            .withAdditionalProperty("filesetNameSuffix", "filesetNameSuffix")
        );
        var exCase = startCase(caseDef);

        Map<String, Object> variables = new HashMap<>();
        variables.put(APPLICANT_ID, mainApplicantId);

        //when
        flowProcessService.startProcess(exCase.getKey(), ProcessConstants.BPM_APP_ONBOARDING, variables);

        //then
        assertThat(
            containsAllEvents(exCase.getKey(),
                List.of(APPLICANT_ONBOARDING_PENDING_EVENT,
                    AML_PENDING_EVENT,
                    AML_SUCCEED_EVENT,
                    APPLICANT_ONBOARDING_FINISHED_EVENT)))
            .isTrue();
    }
}
