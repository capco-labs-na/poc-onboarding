package com.backbase.flow.sme.onboarding.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.service.productselection.collection.ProductResponse;
import com.backbase.flow.service.productselection.interaction.dto.SelectProductRequest;
import com.backbase.flow.sme.onboarding.BaseIntegrationIT;
import com.backbase.flow.sme.onboarding.TestConstants;
import com.backbase.flow.sme.onboarding.casedata.SmeCaseDefinition;
import com.backbase.flow.sme.onboarding.interaction.handler.InBranchOnboardingStartHandler;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.Setter;
import lombok.Value;
import org.junit.jupiter.api.Test;

class InBranchOnboardingProcessTest extends BaseIntegrationIT {

    private static final String PRODUCT_1_REFERENCE = "product-1";
    private static final String PRODUCT_1_NAME = "Business Checking";
    private static final String PRODUCT_2_REFERENCE = "product-2";
    private static final InBranchOnboardingStartHandler.InBranchApplicantDto APPLICANT =
        InBranchOnboardingStartHandler.InBranchApplicantDto.builder()
            .firstName("firstName")
            .lastName("lastName")
            .email("email@backbase.com")
            .businessName("businessName")
            .numberAndStreet("numberAndStreet")
            .apt("apt")
            .city("city")
            .state("state")
            .zipCode("12345")
            .build();
    @Setter
    private UUID caseKey;
    @Setter
    private String interactionId;
    private final List<InBranchStepAction> happyPath = List.of(
        new InBranchStepAction(InBranchStep.START,
            this::startInBranchProcess
        ),
        new InBranchStepAction(InBranchStep.SELECT_PRODUCTS, () -> {
            performGetProductList();
            performProductSelectionRequest();
            checkCasePromoted();
            checkProductSelection();
        })
    );

    @Test
    void inBranchOnboarding_done_fullHappyPath() {
        assertDoesNotThrow(() -> happyPathUntil(InBranchStep.DONE));
    }

    @Test
    void inBranchOnboarding_error_invalidApplicantData() {
        var response = interaction(
            TestConstants.INTERACTION_IN_BRANCH_ONBOARDING_START, TestConstants.IN_BRANCH_ONBOARDING_START,
            null, InBranchOnboardingStartHandler.InBranchApplicantDto.builder().build()
        );
        checkError(response, "in-branch-onboarding-start",
            "firstName", "lastName", "email", "businessName", "numberAndStreet", "city", "state", "zipCode"
        );
    }

    void startInBranchProcess() {
        //when
        var response = interaction(
            TestConstants.INTERACTION_IN_BRANCH_ONBOARDING_START, TestConstants.IN_BRANCH_ONBOARDING_START,
            null, APPLICANT
        );

        var caseKey = ((Map) response.getBody()).get("caseKey").toString();
        setCaseKey(UUID.fromString(caseKey));

        var interactionId = response.getInteractionId();
        setInteractionId(interactionId);

        //then
        var aCase = getCase(caseKey);
        var caseData = aCase.getCaseData(SmeCaseDefinition.class);
        assertInBranchCaseData(aCase, caseData);
    }

    private void assertInBranchCaseData(Case aCase, SmeCaseDefinition caseData) {
        assertSoftly(softly -> {
            softly.assertThat(caseKey)
                .isEqualTo(aCase.getKey());
            softly.assertThat(aCase.isPreliminary())
                .isTrue();
            softly.assertThat(aCase.isArchived())
                .isFalse();
            softly.assertThat(aCase.getVersion())
                .isEqualTo(1);
            softly.assertThat(caseData.getApplicants())
                .hasSize(1);

            var applicant = caseData.getApplicants().get(0);
            softly.assertThat(applicant.getFirstName())
                .isEqualTo(APPLICANT.getFirstName());
            softly.assertThat(applicant.getLastName())
                .isEqualTo(APPLICANT.getLastName());
            softly.assertThat(applicant.getEmail())
                .isEqualTo(APPLICANT.getEmail());
            softly.assertThat(applicant.getPersonalAddress().getCity())
                .isEqualTo(APPLICANT.getCity());
            softly.assertThat(applicant.getPersonalAddress().getApt())
                .isEqualTo(APPLICANT.getApt());
            softly.assertThat(applicant.getPersonalAddress().getState())
                .isEqualTo(APPLICANT.getState());
            softly.assertThat(applicant.getPersonalAddress().getZipCode())
                .isEqualTo(APPLICANT.getZipCode());
            softly.assertThat(applicant.getPersonalAddress().getNumberAndStreet())
                .isEqualTo(APPLICANT.getNumberAndStreet());
        });
    }

    private void performGetProductList() {
        var response = interaction(TestConstants.INTERACTION_IN_BRANCH_ONBOARDING_START,
            TestConstants.ACTION_SME_ON_BOARDING_GET_PRODUCT_LIST, interactionId, null);
        var products = mapToList(response.getBody(), ProductResponse.class);
        assertThat(products)
            .extracting(ProductResponse::getReferenceId)
            .containsExactly(PRODUCT_1_REFERENCE, PRODUCT_2_REFERENCE);
    }

    private void performProductSelectionRequest() {
        var request = new SelectProductRequest();
        var selectedProduct = new SelectProductRequest.ProductItem();
        selectedProduct.setReferenceId(PRODUCT_1_REFERENCE);
        selectedProduct.setProductName(PRODUCT_1_NAME);
        request.setSelection(List.of(selectedProduct));

        interaction(TestConstants.INTERACTION_IN_BRANCH_ONBOARDING_START,
            TestConstants.ACTION_STEP_SME_ON_BOARDING_SELECT_PRODUCTS, interactionId, request);
    }

    private void checkProductSelection() {
        var caseData = getCaseData(caseKey);
        var products = caseData.getProductSelection().getSelectedProducts();
        assertThat(products).hasSize(1);
        var product = products.get(0);
        assertThat(product.getReferenceId()).isEqualTo(PRODUCT_1_REFERENCE);
        assertThat(product.getProductName()).isEqualTo(PRODUCT_1_NAME);
    }

    private void checkCasePromoted() {
        var aCase = getCase(caseKey);
        assertThat(aCase.isPreliminary()).isFalse();
    }

    private void happyPathUntil(InBranchStep step) {
        happyPath.stream()
            .takeWhile(stepAction -> !step.equals(stepAction.getStep()))
            .map(InBranchStepAction::getAction)
            .forEach(Runnable::run);
    }

    private enum InBranchStep {
        START,
        SELECT_PRODUCTS,
        DONE
    }

    @Value
    private static class InBranchStepAction {
        InBranchStep step;
        Runnable action;
    }
}
