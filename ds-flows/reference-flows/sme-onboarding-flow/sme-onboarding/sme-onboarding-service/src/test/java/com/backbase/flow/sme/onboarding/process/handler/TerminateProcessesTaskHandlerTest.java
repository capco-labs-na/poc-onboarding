package com.backbase.flow.sme.onboarding.process.handler;

import static com.backbase.flow.process.ProcessConstants.PROCESS_VARIABLE_CASE_KEY;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.junit.jupiter.api.Test;

class TerminateProcessesTaskHandlerTest {

    private static final String TERMINATION_REASON = "Data-gathering terminated";
    private final RuntimeService runtimeService = mock(RuntimeService.class);
    private final TerminateProcessesTaskHandler handler = new TerminateProcessesTaskHandler(runtimeService);

    @Test
    void execute_terminatesProcesses() throws Exception {
        var caseKey = UUID.randomUUID();
        var execution = execution(caseKey, caseKey.toString());
        var processList = List.of(mockedProcessInstance(UUID.randomUUID()),
            mockedProcessInstance(UUID.randomUUID()), mockedProcessInstance(UUID.randomUUID()));

        setupMockedQueryResults(caseKey.toString(), processList);

        handler.execute(execution);

        processList.forEach(p -> verify(runtimeService).deleteProcessInstance(p.getId(), TERMINATION_REASON));
    }

    @Test
    void execute_doesNotTerminateSelf() throws Exception {
        var caseKey = UUID.randomUUID();
        var ownProcessInstanceId = UUID.randomUUID();
        var execution = execution(caseKey, ownProcessInstanceId.toString());
        var processList = List.of(mockedProcessInstance(UUID.randomUUID()),
            mockedProcessInstance(ownProcessInstanceId), mockedProcessInstance(UUID.randomUUID()));

        setupMockedQueryResults(caseKey.toString(), processList);

        handler.execute(execution);

        verify(runtimeService, times(2)).deleteProcessInstance(anyString(), eq(TERMINATION_REASON));
        verify(runtimeService,
            times(0)).deleteProcessInstance(eq(ownProcessInstanceId.toString()), anyString());
    }

    private void setupMockedQueryResults(final String caseKey, List<ProcessInstance> processList) {
        var processInstanceQuery = mock(ProcessInstanceQuery.class);
        when(processInstanceQuery.variableValueEquals(PROCESS_VARIABLE_CASE_KEY, caseKey))
            .thenReturn(processInstanceQuery);

        when(processInstanceQuery.list()).thenReturn(processList);

        when(runtimeService.createProcessInstanceQuery()).thenReturn(processInstanceQuery);
    }

    private ProcessInstance mockedProcessInstance(UUID id) {
        return FakeProcessInstance.builder()
            .id(id.toString())
            .processInstanceId(id.toString())
            .build();
    }

    private DelegateExecution execution(UUID caseKey, String processInstanceId) {
        var execution = mock(DelegateExecution.class);

        when(execution.getId()).thenReturn(caseKey.toString());
        when(execution.getProcessInstanceId()).thenReturn(processInstanceId);
        when(execution.getVariable(PROCESS_VARIABLE_CASE_KEY)).thenReturn(caseKey.toString());

        return execution;
    }

    @Value
    @Builder
    static class FakeProcessInstance implements ProcessInstance {

        String processDefinitionId;
        String businessKey;
        String rootProcessInstanceId;
        String caseInstanceId;
        String id;
        boolean suspended;
        boolean ended;
        String processInstanceId;
        String tenantId;
    }

}
