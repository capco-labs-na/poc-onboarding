package com.backbase.flow.sme.onboarding.statustracking.provider;

import static java.util.UUID.randomUUID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.backbase.flow.casedata.status.CaseEngineEventPublisher;
import com.backbase.flow.events.JourneyEventMessage;
import com.backbase.flow.events.MetaDataKeys;
import com.backbase.flow.iam.FlowSecurityContext;
import com.backbase.flow.process.events.ProcessEngineEvent;
import com.backbase.flow.sme.onboarding.BaseTest;
import java.time.Instant;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.Callable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class BaseDocumentRequestsMilestoneProviderTest extends BaseTest {

    @Mock
    private CaseEngineEventPublisher publisher;
    @Mock
    private FlowSecurityContext flowSecurityContext;
    @InjectMocks
    private TestBaseDocumentRequestsMilestoneProvider milestoneProvider;

    @BeforeEach
    void setup() {
        when(flowSecurityContext.runWithoutAuthorization(any(Callable.class)))
            .thenAnswer(invocationOnMock -> ((Callable) invocationOnMock.getArgument(0)).call());
    }

    @Test
    void publishMilestoneEvent_interceptingProcessEngineEvents_eventIsPublished() {
        var caseKey = randomUUID();

        milestoneProvider.publishMilestoneEvent(eventMessage(event(caseKey), caseKey));

        verify(publisher, times(1)).publish(any(), any());
    }

    private JourneyEventMessage<ProcessEngineEvent> eventMessage(ProcessEngineEvent event, UUID caseKey) {
        return JourneyEventMessage.<ProcessEngineEvent>builder()
            .eventName("ProcessEngineEvent")
            .eventId(randomUUID())
            .eventTime(Instant.now())
            .caseKey(caseKey)
            .eventDescription(event.getDescription())
            .metadata(MetaDataKeys.ORIGINATED_FROM, "originatedFrom")
            .payload(event)
            .build();
    }

    private ProcessEngineEvent event(UUID caseKey) {
        return new ProcessEngineEvent(
            "Process engine test event", caseKey, randomUUID().toString(), Collections.emptyMap()
        );
    }
}
