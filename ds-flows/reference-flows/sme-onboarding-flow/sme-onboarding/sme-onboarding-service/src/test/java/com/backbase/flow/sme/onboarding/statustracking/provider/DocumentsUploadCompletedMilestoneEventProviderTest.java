package com.backbase.flow.sme.onboarding.statustracking.provider;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.backbase.flow.events.JourneyEventMessage;
import com.backbase.flow.events.MetaDataKeys;
import com.backbase.flow.integration.service.event.UploadDocumentsTaskCompletedEvent;
import com.backbase.flow.sme.onboarding.BaseTest;
import java.time.Instant;
import java.util.Collections;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

class DocumentsUploadCompletedMilestoneEventProviderTest extends BaseTest {

    @InjectMocks
    private DocumentsUploadCompletedMilestoneEventProvider testDocumentsUploadCompletedMilestoneEventProvider;

    private JourneyEventMessage<UploadDocumentsTaskCompletedEvent> eventMessage(
        UploadDocumentsTaskCompletedEvent event, UUID caseKey
    ) {
        return JourneyEventMessage.<UploadDocumentsTaskCompletedEvent>builder()
            .eventName("UploadDocumentsTaskCompletedEvent")
            .eventId(randomUUID())
            .eventTime(Instant.now())
            .caseKey(caseKey)
            .eventDescription(event.getDescription())
            .metadata(MetaDataKeys.ORIGINATED_FROM, "originatedFrom")
            .payload(event)
            .build();
    }

    @Test
    void getEventInstance_interceptingProcessEngineEvents_eventIsPublished() {

        var caseEvent = testDocumentsUploadCompletedMilestoneEventProvider
            .getEventInstance(randomUUID(), Collections.emptyMap());

        assertEquals("Documents Uploaded has been checked", caseEvent.getDescription());
    }
}
