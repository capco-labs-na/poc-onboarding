package com.backbase.flow.sme.onboarding.validation;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class DateFormatValidatorTest {

    private final DateFormatValidator dateFormatValidator = new DateFormatValidator();
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    void isValid_validInput_returnTrue() {
        assertTrue(dateFormatValidator.isValid("2007-12-03", constraintValidatorContext));
    }

    @Test
    void isValid_InvalidInput_returnFalse() {
        assertTrue(dateFormatValidator.isValid(null, constraintValidatorContext));
    }
}
