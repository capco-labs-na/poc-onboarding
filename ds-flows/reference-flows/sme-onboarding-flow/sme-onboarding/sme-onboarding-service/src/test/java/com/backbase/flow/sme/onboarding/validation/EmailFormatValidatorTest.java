package com.backbase.flow.sme.onboarding.validation;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class EmailFormatValidatorTest {

    private final EmailFormatValidator emailFormatValidator = new EmailFormatValidator();

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    void isValid_validInput_returnTrue() {
        assertTrue(emailFormatValidator.isValid("test@email.invalid", constraintValidatorContext));
    }

    @Test
    void isValid_InvalidInput_returnFalse() {
        assertTrue(emailFormatValidator.isValid(null, constraintValidatorContext));
    }
}
