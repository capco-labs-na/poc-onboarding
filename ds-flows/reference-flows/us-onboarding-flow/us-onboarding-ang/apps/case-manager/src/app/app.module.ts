import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';

import { ContainersModule } from '@backbase/universal-ang/containers';
import { LayoutContainerModule } from '@backbase/universal-ang/layouts';
import { NavigationSpaWidgetModule } from '@backbase/universal-ang/navigation';
import { ContentWidgetModule } from '@backbase/universal-ang/content';
import { HeadingWidgetModule } from '@backbase/heading-widget-ang';

// 3rd party and backbase dependencies
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WebSdkApiModule } from '@backbase/foundation-ang/web-sdk';
import { CmIdvResultsWidgetModule } from '@backbase/cm-idv-results-widget-ang';
import { CmNonResidentTaskWidgetModule } from '@backbase/cm-non-resident-task-widget-ang';

// Case manager dependencies
import {
  CaseOverviewListWidgetModule,
  CaseInfoWidgetModule,
  CaseDataViewWidgetModule,
  CaseTaskListWidgetModule,
  ProcessDiagramWidgetModule,
  TaskOverviewListWidgetModule,
  HeaderWidgetModule,
  UserDashboardWidgetModule,
  LiveInsightsWidgetModule,
  TaskCountSummaryWidgetModule,
  DmnInsightsWidgetModule,
  ProcessDiagramSelectorWidgetModule,
  ProcessInsightsCardWidgetModule,
  EventLogWidgetModule,
  CommentsWidgetModule,
  TaskDetailsWidgetModule,
  CaseFilesetListWidgetModule,
  EventsAndTasksWidgetModule,
  ViewInteractionTaskWidgetModule,
  LogoutWidgetModule,
} from '@backbase/case-management-ui-ang/widgets';
import {
  PlatformContainerModule,
  ProcessOverviewContainerModule,
  CaseOverviewContainerModule,
  SidebarOverlayContainerModule,
  ModalOverlayContainerModule,
} from '@backbase/case-management-ui-ang/containers';
import { CoreAngModule } from '@backbase/case-management-ui-ang/core';
import { CmAmlResultsWidgetModule } from '@backbase/cm-aml-results-widget';
import { environment } from '../environments/environment';
import { CmStartCaseAngModule } from '@backbase/cm-start-case-ang';
import { CmInBranchAddressValidationModule } from '@backbase/cm-in-branch-address-validation-ang';

const providers: any[] = environment.production ? [] : (environment.mockProviders as any);

const layoutModules = [
  ContainersModule,
  LayoutContainerModule,
  NavigationSpaWidgetModule,
  ContentWidgetModule,
  HeadingWidgetModule,
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BackbaseCoreModule.forRoot({
      features: {
        THEME_V2: true,
      },
    }),
    RouterModule.forRoot([], { initialNavigation: 'disabled', useHash: true, relativeLinkResolution: 'legacy' }),
    CoreAngModule.forRoot(),
    NgbModule,
    WebSdkApiModule,
    CaseOverviewListWidgetModule,
    CaseInfoWidgetModule,
    CaseDataViewWidgetModule,
    CaseTaskListWidgetModule,
    ProcessDiagramWidgetModule,
    TaskOverviewListWidgetModule,
    EventLogWidgetModule,
    CommentsWidgetModule,
    UserDashboardWidgetModule,
    TaskCountSummaryWidgetModule,
    DmnInsightsWidgetModule,
    PlatformContainerModule,
    LiveInsightsWidgetModule,
    CaseOverviewContainerModule,
    ModalOverlayContainerModule,
    ProcessInsightsCardWidgetModule,
    ProcessOverviewContainerModule,
    ProcessDiagramSelectorWidgetModule,
    CmAmlResultsWidgetModule,
    CmIdvResultsWidgetModule,
    CmNonResidentTaskWidgetModule,
    HeaderWidgetModule,
    SidebarOverlayContainerModule,
    TaskDetailsWidgetModule,
    CaseFilesetListWidgetModule,
    EventsAndTasksWidgetModule,
    LogoutWidgetModule,
    ...layoutModules,
    CmStartCaseAngModule,
    CmInBranchAddressValidationModule,
    ViewInteractionTaskWidgetModule.forRoot({
      'address-validation-idt': [
        {
          label: 'Your address',
          name: 'address',
        },
      ],
    }),
  ],
  providers: providers,
  bootstrap: [AppComponent],
})
export class AppModule {}
