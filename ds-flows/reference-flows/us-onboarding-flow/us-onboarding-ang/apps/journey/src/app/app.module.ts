import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { WebSdkApiModule } from '@backbase/foundation-ang/web-sdk';
import { FormlyFormBuilder } from '@ngx-formly/core';
import { OnoAnchorJourneyAngModule } from '@backbase/ono-anchor-journey-ang';
import { FormlyUiModule } from '@backbase/ds-shared-ang/ui';
import { OnoAddCoApplicantJourneyAngModule } from '@backbase/ono-add-co-applicant-journey-ang';
import { OnoCredentialsJourneyAngModule } from '@backbase/ono-credentials-journey-ang';
import { OnoKycJourneyAngModule } from '@backbase/ono-kyc-journey-ang';
import { OnoTacJourneyAngModule } from '@backbase/ono-tac-journey-ang';
import { FlowInteractionContainerModule, FlowInteractionCoreModule } from '@backbase/flow-interaction-sdk-ang/core';
import { DsLayoutContainerAngModule } from '@backbase/ds-shared-ang/layout';
import { Step } from '@backbase/ds-shared-ang/ui';
import { OnoResponseJourneyAngModule } from '@backbase/ono-response-journey-ang';
import { OnoResponseRetryJourneyAngModule } from '@backbase/ono-response-retry-journey-ang';
import { OnoResponseDeclinedJourneyAngModule } from '@backbase/ono-response-declined-journey-ang';
import {
  OnoOtpVerificationJourneyAngModule,
  OnoAddressJourneyAngModule,
  OnoIdvJumioJourneyAngModule,
  DsSsnJourneyAngModule,
  ProductSelectionJourneyAngModule,
} from '@backbase/ono-journey-collection-ang/journeys';
import { FormBuilder } from '@backbase/ono-journey-collection-ang/common';
import { OnoCommonAngModule } from '@backbase/ono-common-ang';
import { OnoCoApplicantWelcomeJourneyAngModule } from '@backbase/ono-co-applicant-welcome-journey-ang';
import { OnoLoadingWidgetAngModule } from '@backbase/ono-loading-widget-ang';
import { OnoCitizenshipSelectorWidgetAngModule } from '@backbase/ono-citizenship-selector-widget-ang';
import { OnoNonResidentDataWidgetAngModule } from '@backbase/ono-non-resident-data-widget-ang';
const providers: any[] = environment.production ? [] : (environment.mockProviders as any);

const PROSPECT_LABELS: Step[] = [
  { name: ['select-products'], label: `Saving account`, headline: `Start saving your money` },
  { name: ['anchor-data'], label: `Personal details`, headline: `Nice to meet you` },
  { name: ['otp-verification'], label: 'Account verification', headline: `Let's secure your account` },
  { name: ['identity-verification'], label: 'ID protection', headline: 'Smile for the camera' },
  { name: ['address'], label: 'Your address', headline: 'Your address' },
  {
    name: ['co-applicant'],
    label: 'Applicants',
    headline: 'Applicants',
    children: [
      { name: ['co-applicant-data'], label: 'Details', headline: 'Co-applicant details' },
      { name: ['co-applicant-address'], label: 'Address', headline: 'Co-applicant address' },
    ],
  },
  {
    name: ['citizenship'],
    label: 'Citizenship',
    headline: 'Let’s verify your citizenship',
    children: [
      { name: ['ssn', 'non-resident-data'], label: 'Citizenship details', headline: 'Let’s verify your citizenship' },
    ],
  },

  { name: ['kyc'], label: 'Additional information', headline: 'Your occupation' },
  { name: ['credentials'], label: 'Create a password', headline: 'Create a password' },
];
const CO_APPLICANT_EXCLUDE_STEPS_LIST = ['co-applicant', 'select-products'];

let labels: Step[] = PROSPECT_LABELS;
if (sessionStorage.getItem('coApplicantId')) {
  CO_APPLICANT_EXCLUDE_STEPS_LIST.forEach(
    excludeStep => (labels = labels.filter(label => !label.name.some(name => excludeStep === name))),
  );
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BackbaseCoreModule.forRoot({
      features: {
        THEME_V2: true,
      },
      assets: {
        assetsStaticItemName: environment.assetsStaticItemName,
      },
    }),
    WebSdkApiModule,
    RouterModule.forRoot([], { initialNavigation: 'disabled', useHash: true, relativeLinkResolution: 'legacy' }),
    FlowInteractionContainerModule,
    FlowInteractionCoreModule.forRoot({
      actionConfig: {
        'submit-anchor-data': { updateCdo: true },
        'submit-address': { updateCdo: true },
        'submit-ssn': { updateCdo: true },
        'account-type-selector': { updateCdo: true },
        'request-otp': { updateCdo: true },
        'verify-otp': { updateCdo: true },
        'setup-credentials': { updateCdo: true },
        'submit-kyc-data': { updateCdo: true },
        'agree-to-terms': { updateCdo: true },
        'empty-handler': { updateCdo: false },
        'kyc-questions-loader': { updateCdo: false },
        'identity-verification-initiation': { updateCdo: false },
        'identity-verification-result': { updateCdo: false },
        'fetch-co-applicant-data': { updateCdo: false },
        'select-products': { updateCdo: true },
        'citizenship-selector': { updateCdo: false },
        'fetch-citizenship-data': { updateCdo: false },
        'submit-non-resident-data': { updateCdo: true },
      },
      steps: {
        init: ['init'],
        'anchor-data': ['anchor-data'],
        address: ['address'],
        'co-applicant': ['co-applicant'],
        'co-applicant-data': ['co-applicant-data'],
        'co-applicant-address': ['co-applicant-address'],
        'otp-verification': ['otp-verification'],
        credentials: ['credentials'],
        welcome: ['fetch-co-applicant-data'],
        kyc: ['kyc'],
        'terms-and-conditions': ['terms-and-conditions'],
        'identity-verification': ['identity-verification'],
        'successfully-done': ['successfully-done'],
        'successfully-done-co-applicant': ['successfully-done-co-applicant'],
        'in-review-done': ['in-review-done'],
        retry: ['retry'],
        declined: ['declined'],
        'select-products': ['select-products'],
        citizenship: ['citizenship'],
        ssn: ['ssn'],
        'non-resident-data': ['non-resident-data'],
      },
    }),
    OnoCommonAngModule,
    OnoAnchorJourneyAngModule,
    FormlyUiModule,
    OnoAddressJourneyAngModule,
    OnoAddCoApplicantJourneyAngModule,
    OnoOtpVerificationJourneyAngModule,
    OnoCredentialsJourneyAngModule,
    OnoKycJourneyAngModule,
    OnoTacJourneyAngModule,
    DsLayoutContainerAngModule.forRoot({
      onboarding: labels,
    }),
    OnoResponseJourneyAngModule,
    OnoIdvJumioJourneyAngModule,
    DsSsnJourneyAngModule,
    OnoResponseDeclinedJourneyAngModule,
    OnoResponseRetryJourneyAngModule,
    OnoCoApplicantWelcomeJourneyAngModule,
    OnoLoadingWidgetAngModule,
    ProductSelectionJourneyAngModule,
    OnoCitizenshipSelectorWidgetAngModule,
    OnoNonResidentDataWidgetAngModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    ...providers,
    {
      provide: FormBuilder,
      useClass: FormlyFormBuilder,
    },
  ],
})
export class AppModule {}
