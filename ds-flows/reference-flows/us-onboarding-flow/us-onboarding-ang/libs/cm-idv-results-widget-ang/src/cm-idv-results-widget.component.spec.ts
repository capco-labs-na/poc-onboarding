import { CmIdvResultsWidgetComponent } from './cm-idv-results-widget.component';

describe('#CmIdvResultsWidgetComponent', () => {
  let component: CmIdvResultsWidgetComponent;
  let taskInteractorMock: any;

  const completeTaskInstance = {
    id: '1',
    taskData: { content: 'content' },
    name: '',
    key: '',
    caseKey: '',
    formKey: '',
    createdOn: '',
    dueDate: '',
  };

  const taskInstanceWithoutTaskData = { ...completeTaskInstance, taskData: {} };
  const taskInstanceWithoutId = { ...completeTaskInstance, id: '' };

  beforeEach(() => {
    taskInteractorMock = {
      completeTask: (decision: 'fail' | 'approved') => {},
    };

    component = new CmIdvResultsWidgetComponent(taskInteractorMock);
  });

  it('should assign the task instance', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    expect(component.taskData).toEqual({ content: 'content' });
  });

  it("shouldn't assign the task instance", () => {
    component.taskInstance = taskInstanceWithoutTaskData;
    component.ngOnInit();
    expect(component.taskData).toEqual({});
  });

  it('should complete the task with approved', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.approve();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('1', { decision: 'approved' });
  });

  it('should complete the task with fail', () => {
    component.taskInstance = completeTaskInstance;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.reject();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('1', { decision: 'fail' });
  });

  it('should complete the task without id', () => {
    component.taskInstance = taskInstanceWithoutId;
    component.ngOnInit();
    spyOn(taskInteractorMock, 'completeTask');
    component.reject();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('', { decision: 'fail' });
    component.approve();
    expect(taskInteractorMock.completeTask).toHaveBeenCalledWith('', { decision: 'approved' });
  });
});
