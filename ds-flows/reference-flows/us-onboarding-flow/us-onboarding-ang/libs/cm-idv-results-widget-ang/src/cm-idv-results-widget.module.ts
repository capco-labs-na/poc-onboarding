import { NgModule } from '@angular/core';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { CmIdvResultsWidgetComponent } from './cm-idv-results-widget.component';
import { CoreAngModule } from '@backbase/case-management-ui-ang/core';

import { LoadButtonModule } from '@backbase/ui-ang';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CmIdvResultsWidgetComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    BackbaseCoreModule.withConfig({
      classMap: { CmIdvResultsWidgetComponent },
    }),
    CoreAngModule.withConfig({
      viewMap: {
        CmIdvResultsWidgetComponent,
      },
    }),
  ],
  exports: [CmIdvResultsWidgetComponent],
  entryComponents: [CmIdvResultsWidgetComponent],
})
export class CmIdvResultsWidgetModule {}
