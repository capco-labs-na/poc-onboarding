import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CmStartCasePersonalInformationComponent } from './cm-start-case-personal-information/cm-start-case-personal-information.component';
import { CmStartCaseProductSelectionComponent } from './cm-start-case-product-selection/cm-start-case-product-selection.component';
import { CmStartCaseAngComponent } from './cm-start-case-ang.component';
import { ButtonModule, LoadButtonModule, ModalModule } from '@backbase/ui-ang';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { FormlyUiModule } from '@backbase/ds-shared-ang/ui';

@NgModule({
  declarations: [
    CmStartCasePersonalInformationComponent,
    CmStartCaseProductSelectionComponent,
    CmStartCaseAngComponent,
  ],
  imports: [
    CommonModule,
    ButtonModule,
    LoadButtonModule,
    ModalModule,
    FormlyUiModule,
    BackbaseCoreModule.withConfig({
      classMap: { CmStartCaseAngComponent },
    }),
  ],
})
export class CmStartCaseAngModule {}
