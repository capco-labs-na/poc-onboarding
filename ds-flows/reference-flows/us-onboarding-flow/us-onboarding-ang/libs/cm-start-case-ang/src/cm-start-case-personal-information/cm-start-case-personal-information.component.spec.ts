import { of } from 'rxjs';
import { CmStartCasePersonalInformationComponent } from './cm-start-case-personal-information.component';

describe('CmStartCasePersonalInformationComponent', () => {
  let component: CmStartCasePersonalInformationComponent;
  let flowInteractionMock: any;
  let datePipeMock: any;

  beforeEach(async () => {
    flowInteractionMock = {
      call: jasmine.createSpy('call').and.returnValue(of(undefined)),
    };
    datePipeMock = {};

    component = new CmStartCasePersonalInformationComponent(flowInteractionMock as any, datePipeMock as any);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
