import { of } from 'rxjs';
import { CmStartCaseProductSelectionComponent } from './cm-start-case-product-selection.component';

describe('CmStartCaseProductSelectionComponent', () => {
  let component: CmStartCaseProductSelectionComponent;
  let flowInteractionMock: any;
  let changeDetectorMock: any;

  beforeEach(async () => {
    flowInteractionMock = {
      call: jasmine.createSpy('call').and.returnValue(of(undefined)),
    };

    changeDetectorMock = {
      detectChanges: jasmine.createSpy('detectChanges'),
    };

    component = new CmStartCaseProductSelectionComponent(flowInteractionMock as any, changeDetectorMock as any);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
