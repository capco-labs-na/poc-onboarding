import { FormlyFieldConfig } from '@ngx-formly/core';
import { InputType } from '@backbase/ds-shared-ang/ui';

function getDateInThePast(yearsOffset: number) {
  const today = new Date();
  return new Date(today.getFullYear() - yearsOffset, today.getMonth(), today.getDate(), 23, 59, 59).toISOString();
}

export const AnchorFormMetadata: FormlyFieldConfig[] = [
  {
    type: InputType.Text,
    key: 'firstName',
    className: 'd-md-block w-50',
    templateOptions: {
      label: 'First name',
      required: true,
      maxLength: 50,
    },
  },
  {
    type: InputType.Text,
    key: 'lastName',
    className: 'd-md-block w-50',
    templateOptions: {
      label: 'Last name',
      required: true,
      maxLength: 50,
    },
  },
  {
    type: InputType.DividedDate,
    key: 'dateOfBirth',
    className: 'd-md-block w-50',
    templateOptions: {
      label: 'Date of birth',
      required: true,
      overrideDateFormat: 'MM/dd/yyyy',
      minDate: getDateInThePast(100),
      maxDate: getDateInThePast(18),
    },
    validators: [],
  },
  {
    type: InputType.Email,
    key: 'email',
    className: 'd-md-block w-75',
    templateOptions: {
      label: 'Email address',
      required: true,
    },
    validators: [],
  },
];
