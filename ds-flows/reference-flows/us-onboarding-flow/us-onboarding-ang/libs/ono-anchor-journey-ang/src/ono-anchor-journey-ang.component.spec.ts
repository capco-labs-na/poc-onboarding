import { of } from 'rxjs';
import { OnoAnchorJourneyAngComponent } from './ono-anchor-journey-ang.component';

class FakeHelper {
  form = {
    markAsUntouched() {
      return this;
    },
  };

  setFieldsConfig() {
    return this;
  }

  setModel() {
    return this;
  }

  setPayloadMapper() {
    return this;
  }
}

describe('#OnoAnchorJourneyAngComponent', () => {
  let component: OnoAnchorJourneyAngComponent;
  let datePipeMock: any;
  let flowInteractionMock: any;
  let formHelperFactoryMock: any;
  let formHelperMock: any;

  beforeEach(() => {
    datePipeMock = {};
    flowInteractionMock = {
      nav: { next: jasmine.createSpy('next') },
      cdo: {
        get: jasmine.createSpy('get').and.returnValue(of(undefined)),
      },
      call: jasmine.createSpy('call').and.returnValue(of(undefined)),
      steps: jasmine.createSpy('steps').and.returnValue(of(undefined)),
    };
    formHelperMock = new FakeHelper();
    spyOn(formHelperMock, 'setFieldsConfig').and.callThrough();
    spyOn(formHelperMock, 'setModel').and.callThrough();
    spyOn(formHelperMock, 'setPayloadMapper').and.callThrough();
    formHelperFactoryMock = {
      getHelper: () => formHelperMock,
    };

    component = new OnoAnchorJourneyAngComponent(formHelperFactoryMock, flowInteractionMock, datePipeMock);
  });

  it('should navigate if no errors', async () => {
    component.helper.submit = jasmine.createSpy('submit').and.returnValue(Promise.resolve({ actionErrors: [] }));
    await component.submitAction();
    expect(component.helper.submit).toHaveBeenCalled();
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should not navigate if errors', async () => {
    component.helper.submit = jasmine
      .createSpy('submit')
      .and.returnValue(Promise.resolve({ actionErrors: [{ code: 'foo', message: 'bar' }] }));
    await component.submitAction();
    expect(component.helper.submit).toHaveBeenCalled();
    expect(flowInteractionMock.nav.next).not.toHaveBeenCalled();
  });

  it('should init correctly', () => {
    component.ngOnInit();
    expect(component.helper.setModel).toHaveBeenCalled();
  });

  it('should init correctly on co-applicant', () => {
    component.isCoApplicant = true;
    component.ngOnInit();
    expect(component.helper.setModel).toHaveBeenCalled();
  });
});
