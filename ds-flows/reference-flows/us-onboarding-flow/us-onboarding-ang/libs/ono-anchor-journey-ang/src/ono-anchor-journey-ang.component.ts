import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { InteractionResponseDto } from '@backbase/flow-interaction-openapi-data';
import { FormHelperFactory } from '@backbase/ono-common-ang';
import { combineLatest, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AnchorFormMetadata } from './anchor-form';

@Component({
  selector: 'bb-ono-anchor-journey',
  templateUrl: './ono-anchor-journey-ang.component.html',
  providers: [DatePipe, FormHelperFactory],
})
export class OnoAnchorJourneyAngComponent implements OnInit {
  @Input() isCoApplicant?: boolean;
  @Input() header?: string;
  backStep$?: Observable<string | undefined>;
  submitted = false;

  readonly helper = this.helperFactory
    .getHelper()
    .setFieldsConfig(AnchorFormMetadata)
    .setModel({ dateOfBirth: undefined })
    .setPayloadMapper(formValue => {
      return {
        ...formValue,
        dateOfBirth: this._datePipe.transform(formValue.dateOfBirth, 'yyyy-MM-dd'),
      };
    });

  constructor(
    private readonly helperFactory: FormHelperFactory,
    private readonly _flowInteraction: FlowInteractionService,
    private readonly _datePipe: DatePipe,
  ) {
    if (sessionStorage.getItem('coApplicantId')) {
      this._flowInteraction
        .call({
          action: 'prefill-anchor-data',
          body: { payload: { coApplicantId: sessionStorage.getItem('coApplicantId') } },
        })
        .pipe(take(1))
        .subscribe((response: any) => {
          if (!response.body) return;
          this.helper.setModel({
            firstName: response.body.firstName || '',
            lastName: response.body.lastName || '',
            dateOfBirth: response.body.dateOfBirth ? new Date(response.body.dateOfBirth).toJSON() : '',
            email: response.body.email || '',
          });
        });
    }
  }
  ngOnInit(): void {
    this.backStep$ = this._flowInteraction.steps().pipe(
      map((steps: InteractionResponseDto['steps']) => {
        return steps ? steps[this._flowInteraction.nav.currentStep.name].back : '';
      }),
    );

    if (this.isCoApplicant) {
      // Get the co-applicant data from cdo -- MT
      this.helper.form.markAsUntouched();
    } else {
      combineLatest([
        this._flowInteraction.cdo.get<string>('firstName').pipe(take(1)),
        this._flowInteraction.cdo.get<string>('lastName').pipe(take(1)),
        this._flowInteraction.cdo.get<string>('dateOfBirth').pipe(take(1)),
        this._flowInteraction.cdo.get<string>('email').pipe(take(1)),
      ])
        .pipe(take(1))
        .subscribe(([firstName, lastName, dateOfBirth, email]) => {
          const dob = dateOfBirth ? new Date(dateOfBirth).toJSON() : '';
          this.helper.setModel({ firstName, lastName, dateOfBirth: dob, email });
        });
    }
  }

  async submitAction() {
    this.submitted = true;
    const response = await this.helper.submit('submit-anchor-data');
    if (response && response.actionErrors && response.actionErrors.length === 0) {
      this._flowInteraction.nav.next();
    }
  }

  async goBack(step: string) {
    await this._flowInteraction.navigate(step).toPromise();
    this._flowInteraction.nav.next();
  }
}
