import { of } from 'rxjs';
import {
  CitizenshipType,
  OnoCitizenshipSelectorWidgetAngComponent,
} from './ono-citizenship-selector-widget-ang.component';

describe('#OnoCitizenshipSelectorWidgetAngComponent', () => {
  let component: OnoCitizenshipSelectorWidgetAngComponent;
  let flowInteractionMock: {
    call: jasmine.Spy;
    nav: { next: jasmine.Spy };
    steps: jasmine.Spy;
    navigate: jasmine.Spy;
  };

  beforeEach(() => {
    flowInteractionMock = {
      nav: { next: jasmine.createSpy('next') },
      navigate: jasmine.createSpy('navigate').and.returnValue(of({})),
      steps: jasmine.createSpy('steps').and.returnValue(of(undefined)),
      call: jasmine.createSpy('call').and.returnValue(of({})),
    };

    component = new OnoCitizenshipSelectorWidgetAngComponent(flowInteractionMock as any);
  });

  it('should be initialized', () => {
    expect(component).toBeDefined();
  });

  it('should get the back step', () => {
    component.ngOnInit();
    expect(flowInteractionMock.steps).toHaveBeenCalled();
  });

  it('should navigate to defined step', async () => {
    await component.goBack('step');
    expect(flowInteractionMock.navigate).toHaveBeenCalled();
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should submit the citizenship type', async () => {
    component.form.setValue({ type: CitizenshipType.US_CITIZEN });
    await component.submitAction();
    expect(flowInteractionMock.call).toHaveBeenCalledWith({
      action: 'citizenship-selector',
      body: { payload: { type: 'US_CITIZEN' } },
    });
  });

  it('should not submit if the citizenship type is not selected', async () => {
    await component.submitAction();
    expect(flowInteractionMock.call).not.toHaveBeenCalledWith();
  });
});
