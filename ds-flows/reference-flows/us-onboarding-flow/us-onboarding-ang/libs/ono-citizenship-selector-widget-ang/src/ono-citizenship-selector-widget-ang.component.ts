import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { InteractionResponseDto } from '@backbase/flow-interaction-openapi-data';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export enum CitizenshipType {
  US_CITIZEN = 'US_CITIZEN',
  PERMANENT_RESIDENT = 'PERMANENT_RESIDENT',
  NON_RESIDENT_ALIEN = 'NON_RESIDENT_ALIEN',
}

@Component({
  selector: 'bb-us-ono-citizenship-selector-widget-ang',
  templateUrl: './ono-citizenship-selector-widget-ang.component.html',
})
export class OnoCitizenshipSelectorWidgetAngComponent implements OnInit {
  backStep$?: Observable<string | undefined>;
  loading$ = new BehaviorSubject(false);
  citizenshipTypeEnum = CitizenshipType;
  submitted = false;

  form = new FormGroup({
    type: new FormControl({ value: '', disabled: false }, Validators.required),
  });

  constructor(private readonly _flowInteraction: FlowInteractionService) {}

  ngOnInit(): void {
    this.backStep$ = this._flowInteraction
      .steps()
      .pipe(
        map(
          (steps: InteractionResponseDto['steps']) =>
            steps ? steps[this._flowInteraction.nav.currentStep.name].back : '',
        ),
      );

    this._flowInteraction
      .call({ action: 'fetch-citizenship-data', body: { payload: {} } })
      .toPromise()
      .then(response => {
        if (response && response.body && response.body.type) {
          this.form.setValue({ type: response.body.type });
        }
      });
  }

  submitAction() {
    this.submitted = true;

    this.form.markAsTouched();
    if (this.form.invalid) return;

    this.loading$.next(true);
    this._flowInteraction
      .call({ action: 'citizenship-selector', body: { payload: this.form.value } })
      .toPromise()
      .then(() => this._flowInteraction.nav.next())
      .finally(() => this.loading$.next(false));
  }

  async goBack(step: string) {
    await this._flowInteraction.navigate(step).toPromise();
    this._flowInteraction.nav.next();
  }
}
