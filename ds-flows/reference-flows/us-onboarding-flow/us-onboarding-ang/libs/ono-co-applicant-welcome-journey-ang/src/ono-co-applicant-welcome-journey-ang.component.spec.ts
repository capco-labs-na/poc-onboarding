import { of, Subscription } from 'rxjs';
import { OnoCoApplicantWelcomeJourneyAngComponent } from './ono-co-applicant-welcome-journey-ang.component';

describe('#OnoCoApplicantWelcomeJourneyAngComponent', () => {
  let component: OnoCoApplicantWelcomeJourneyAngComponent;

  let flowInteractionMock: {
    call: jasmine.Spy;
  };

  const activatedRouteMock = {
    snapshot: {
      queryParams: {
        coApplicantId: '123',
      },
    },
  };

  let document: {
    location: {
      href: string;
    };
  };
  const contentService = {
    getContent: () =>
      of({
        url: 'someImage.jpg',
      }),
  };

  let subscription: Subscription;

  beforeEach(() => {
    flowInteractionMock = {
      call: jasmine.createSpy('call').and.returnValue(
        of({
          interactionId: 123,
          body: {
            mainApplicant: { firstName: 'Jane', lastName: 'Doe' },
            coApplicant: { firstName: 'John', lastName: 'Doe' },
          },
        }),
      ),
    };
    document = {
      location: {
        href: 'http:thissite.com',
      },
    };
  });

  it('should be defined', () => {
    component = new OnoCoApplicantWelcomeJourneyAngComponent(
      activatedRouteMock as any,
      flowInteractionMock as any,
      contentService as any,
      document as Document,
    );
    expect(component).toBeDefined();
  });

  it('should fetch co-applicant data', () => {
    component = new OnoCoApplicantWelcomeJourneyAngComponent(
      activatedRouteMock as any,
      flowInteractionMock as any,
      contentService as any,
      document as Document,
    );
    expect(flowInteractionMock.call).toHaveBeenCalledWith({
      action: 'fetch-co-applicant-data',
      body: { payload: { coApplicantId: '123' } },
    });
  });

  it('should have main applicants information', () => {
    component = new OnoCoApplicantWelcomeJourneyAngComponent(
      activatedRouteMock as any,
      flowInteractionMock as any,
      contentService as any,
      document as Document,
    );
    subscription = component.applicants$.subscribe((applicants: any) => {
      expect(applicants.mainApplicant.firstName).toBe('Jane');
      expect(applicants.mainApplicant.lastName).toBe('Doe');
      expect(applicants.coApplicant.firstName).toBe('John');
      expect(applicants.coApplicant.lastName).toBe('Doe');
    });
  });

  it('should have main applicants information', () => {
    flowInteractionMock.call = jasmine.createSpy('call').and.returnValue(
      of({
        interactionId: 123,
      }),
    );
    component = new OnoCoApplicantWelcomeJourneyAngComponent(
      activatedRouteMock as any,
      flowInteractionMock as any,
      contentService as any,
      document as Document,
    );

    subscription = component.applicants$.subscribe((applicants: any) => {
      expect(applicants).toBeUndefined();
    });
  });

  it('should navigate to onboarding-us', () => {
    component = new OnoCoApplicantWelcomeJourneyAngComponent(
      activatedRouteMock as any,
      flowInteractionMock as any,
      contentService as any,
      document as Document,
    );
    component.getStartedUrl = 'http://somesite.com';
    component.goToOnboarding();
    expect(document.location.href).toBe('http://somesite.com');
  });

  afterAll(() => {
    subscription.unsubscribe();
  });
});
