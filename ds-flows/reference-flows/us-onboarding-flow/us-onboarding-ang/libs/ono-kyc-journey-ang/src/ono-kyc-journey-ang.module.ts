import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoKycJourneyAngComponent } from './ono-kyc-journey-ang.component';
import { LoadButtonModule } from '@backbase/ui-ang';
import { FormlyUiModule } from '@backbase/ds-shared-ang/ui';

@NgModule({
  declarations: [OnoKycJourneyAngComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    FormlyUiModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoKycJourneyAngComponent },
    }),
  ],
})
export class OnoKycJourneyAngModule {}
