import { Component, Input, OnInit } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'bb-us-ono-loading-widget-ang',
  template: `
  <div class="card">
    <div class="card-body">
      <bb-loading-indicator-ui
        loaderSize="lg"
        text="Loading..."
      ></bb-loading-indicator-ui>
    </div>
  </div>
  `,
  styles: [],
})
export class OnoLoadingWidgetAngComponent implements OnInit {
  @Input() fetchCustomer?: boolean;

  constructor(private readonly _flowInteraction: FlowInteractionService) {}

  async ngOnInit() {
    const coApplicantId = sessionStorage.getItem('coApplicantId');

    let response;

    response = await this._flowInteraction
      .call({
        action: 'fetch-co-applicant',
        body: { payload: { coApplicantId: coApplicantId || '' } },
      })
      .pipe(delay(500))
      .toPromise();

    if (!response) {
      return;
    }

    if (response.actionErrors && response.actionErrors.length !== 0) {
      return;
    }

    this._flowInteraction.nav.next();
  }
}
