import { of } from 'rxjs';
import { OnoNonResidentDataWidgetAngComponent } from './ono-non-resident-data-widget-ang.component';

describe('#OnoNonResidentDataWidgetAngComponent', () => {
  let component: OnoNonResidentDataWidgetAngComponent;
  let flowInteractionMock: {
    call: jasmine.Spy;
    nav: { next: jasmine.Spy };
    steps: jasmine.Spy;
    navigate: jasmine.Spy;
  };

  beforeEach(() => {
    flowInteractionMock = {
      nav: { next: jasmine.createSpy('next') },
      navigate: jasmine.createSpy('navigate').and.returnValue(of({})),
      steps: jasmine.createSpy('steps').and.returnValue(of(undefined)),
      call: jasmine.createSpy('call').and.returnValue(of({})),
    };

    component = new OnoNonResidentDataWidgetAngComponent(flowInteractionMock as any);
  });

  it('should be initialized', () => {
    expect(component).toBeDefined();
  });

  it('should get the back step', () => {
    component.ngOnInit();
    expect(flowInteractionMock.steps).toHaveBeenCalled();
  });

  it('should get the country list', async () => {
    flowInteractionMock.call.and.returnValue(
      of({
        NL: 'Netherlands (the)',
      }),
    ),
      component.ngOnInit();
    expect(flowInteractionMock.call).toHaveBeenCalledWith({
      action: 'country-list',
      body: { payload: {} },
    });
    console.log(component.countryListArray);
  });

  it('should navigate to defined step', async () => {
    await component.goBack('step');
    expect(flowInteractionMock.navigate).toHaveBeenCalled();
    expect(flowInteractionMock.nav.next).toHaveBeenCalled();
  });

  it('should submit the non resident data', async () => {
    const formValue = {
      citizenshipCountryCode: 'NL',
      residencyAddress: {
        countryCode: 'NL',
        numberAndStreet: '',
        city: '',
        zipCode: '',
      },
      nationalTin: '',
      foreignTin: '123',
      withholdingTaxAccepted: false,
      w8benAccepted: true,
    };
    component.form.setValue(formValue);
    await component.submitAction();
    expect(flowInteractionMock.call).toHaveBeenCalledWith({
      action: 'submit-non-resident-data',
      body: { payload: formValue },
    });
  });

  it('should not submit if the form is invalid', async () => {
    await component.submitAction();
    expect(flowInteractionMock.call).not.toHaveBeenCalledWith();
  });
});
