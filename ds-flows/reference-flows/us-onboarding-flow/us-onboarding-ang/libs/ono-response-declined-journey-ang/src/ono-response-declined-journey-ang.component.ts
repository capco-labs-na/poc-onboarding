import { Component, Input } from '@angular/core';
import { ContentService } from '@backbase/content-ang';

@Component({
  selector: 'bb-ono-response-journey-ang',
  templateUrl: './ono-response-declined-journey-ang.component.html',
  providers: [ContentService],
})
export class OnoResponseDeclinedJourneyAngComponent {
  @Input() customerServiceUrl = '';
  @Input() localBranchUrl = '';
  @Input() productsListhUrl = '';
}
