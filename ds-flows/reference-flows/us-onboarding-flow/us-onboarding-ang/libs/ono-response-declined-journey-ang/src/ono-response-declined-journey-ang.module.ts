import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { OnoResponseDeclinedJourneyAngComponent } from './ono-response-declined-journey-ang.component';
import { LoadButtonModule, ButtonModule, IconModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [OnoResponseDeclinedJourneyAngComponent],
  imports: [
    CommonModule,
    LoadButtonModule,
    ButtonModule,
    IconModule,
    BackbaseCoreModule.withConfig({
      classMap: { OnoResponseDeclinedJourneyAngComponent },
    }),
  ],
})
export class OnoResponseDeclinedJourneyAngModule {}
