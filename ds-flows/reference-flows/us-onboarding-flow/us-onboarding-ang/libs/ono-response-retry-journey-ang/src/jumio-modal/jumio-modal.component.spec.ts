import { JumioModalComponent } from './jumio-modal.component';

describe('#IntroductionModalComponent', () => {
  let component: JumioModalComponent;

  beforeEach(() => {
    component = new JumioModalComponent();
    component.isOpen = true;
    component.iFrameSaveUrl = '';
  });

  it('should emmit "close" event when iFrame returns success', () => {
    const spyOnCloseNext = spyOn(component.close, 'next');
    component.analyseIframeResponse({
      payload: {
        value: 'success',
      },
    });
    expect(spyOnCloseNext).toHaveBeenCalled();
  });

  it('should\'nt emmit "close" event when iFrame doesn\'nt return success', () => {
    const spyOnCloseNext = spyOn(component.close, 'next');
    component.analyseIframeResponse({
      payload: {
        value: 'error',
      },
    });
    expect(spyOnCloseNext).not.toHaveBeenCalled();
  });

  it('should\'nt emmit "close" event when iFrame doesn\'nt return response', () => {
    const spyOnCloseNext = spyOn(component.close, 'next');
    component.analyseIframeResponse({});
    expect(spyOnCloseNext).not.toHaveBeenCalled();
  });
});
