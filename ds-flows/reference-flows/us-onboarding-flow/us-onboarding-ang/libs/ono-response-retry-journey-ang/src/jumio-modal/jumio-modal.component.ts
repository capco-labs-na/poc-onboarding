import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'bb-ono-jumio-modal',
  templateUrl: './jumio-modal.component.html',
  styleUrls: ['./jumio-modal.component.scss'],
})
export class JumioModalComponent {
  @Input() isOpen = false;
  @Input() iFrameSaveUrl: SafeResourceUrl | undefined;
  @Output() readonly close = new EventEmitter();

  @HostListener('window:message', ['$event'])
  onMessage(e: any) {
    this.analyseIframeResponse(e.data ? JSON.parse(e.data) : '');
  }

  constructor() {}

  analyseIframeResponse(data: any) {
    const payload = (data.payload && data.payload.value) || '';

    if (payload === 'success') {
      this.close.next(data);
    }
  }
}
