import { OnoTacJourneyAngComponent } from './ono-tac-journey-ang.component';
import { of } from 'rxjs';

describe('#OnoTacJourneyAngComponent', () => {
  let component: OnoTacJourneyAngComponent;
  let flowMock: any;
  let widgetModel: any;

  beforeEach(() => {
    flowMock = {
      call: () => of({}),
      nav: { next: jasmine.createSpy('next') },
      navigate: () => of({}),
      steps: jasmine.createSpy('steps').and.returnValue(of(undefined)),
    };
    widgetModel = {
      properties: {
        pipe: jasmine.createSpy('pipe').and.returnValue(of(undefined)),
      },
    };

    component = new OnoTacJourneyAngComponent(flowMock, widgetModel);
  });

  it('should trigger interactionService on submit', async () => {
    component.agreeCtrl.setValue(true);
    await component.submitAction();
    expect(flowMock.nav.next).toHaveBeenCalled();
  });
});
