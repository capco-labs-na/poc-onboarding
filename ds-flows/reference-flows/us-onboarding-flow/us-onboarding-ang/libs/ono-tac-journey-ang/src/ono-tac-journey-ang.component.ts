import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ContentService } from '@backbase/content-ang';
import { FlowInteractionService } from '@backbase/flow-interaction-sdk-ang/core';
import { ItemModel } from '@backbase/foundation-ang/core';
import { map } from 'rxjs/operators';

@Component({
  selector: 'bb-ono-tac-journey-ang',
  templateUrl: './ono-tac-journey-ang.component.html',
  styleUrls: ['./ono-tac-journey-ang.component.scss'],
  providers: [ContentService],
})
export class OnoTacJourneyAngComponent {
  agreeCtrl = new FormControl(false, Validators.requiredTrue);

  readonly links$ = this.widgetModel.properties.pipe(
    map(props => ({
      tncLinkText: props.tncTermsConditionsLinkText,
      tncLink: props.tncTermsConditionsLinkUrl,
      privacyLinkText: props.tncPrivacyStatementLinkText,
      privacyLink: props.tncPrivacyStatementLinkUrl,
    })),
  );

  constructor(private readonly _interactionService: FlowInteractionService, private readonly widgetModel: ItemModel) {}

  async submitAction() {
    const response = await this._interactionService
      .call({
        action: 'agree-to-terms',
        body: { payload: { agreedToTerms: this.agreeCtrl.value } },
      })
      .toPromise();

    if (!response) {
      return;
    }

    if (response.actionErrors && response.actionErrors.length !== 0) {
      return;
    }

    this._interactionService.nav.next();
  }
}
