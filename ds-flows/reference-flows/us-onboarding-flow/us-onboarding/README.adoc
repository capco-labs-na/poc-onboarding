== US Onboarding BE Reference

This project could be run standalone without use of any other backbase projects. But to make this
project run smooth, minimal IPS is required, as gateway and registry are used by default
to make the connection between case-manager and the Flow core.

=== Building the journey

Start by running in this root folder the command to build the project.

[source,bash]
----
mvn clean install
----

In the folder us-onboarding-service-reference, you will find the project that needs to be started. Please use readme in that folder.