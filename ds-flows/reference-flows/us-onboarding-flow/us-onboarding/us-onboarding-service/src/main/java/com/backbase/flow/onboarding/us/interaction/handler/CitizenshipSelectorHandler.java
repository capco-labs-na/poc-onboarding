package com.backbase.flow.onboarding.us.interaction.handler;

import static com.backbase.flow.onboarding.us.util.OnboardingMapper.MAPPER;

import com.backbase.flow.application.uso.casedata.Applicant;
import com.backbase.flow.application.uso.casedata.CitizenshipInfo;
import com.backbase.flow.casedata.mapper.JourneyMapper;
import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.onboarding.us.interaction.dto.CitizenshipTypeDto;
import com.backbase.flow.onboarding.us.interaction.dto.OnboardingDto;
import com.backbase.flow.utils.CaseDataUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("citizenship-selector")
public class CitizenshipSelectorHandler extends ActionHandlerWithValidation<CitizenshipTypeDto, OnboardingDto> {

    private final CaseDataUtils caseDataUtils;
    private final JourneyMapper<Applicant> journeyMapper;

    @Override
    public ActionResult<OnboardingDto> handleWithValidation(CitizenshipTypeDto citizenshipTypeRequest,
        InteractionContext context) {
        final String caseKey = caseDataUtils.getOrCreateCaseKey(context).toString();
        final Applicant applicant = journeyMapper.read(null, null, caseKey);

        CitizenshipInfo citizenshipInfo = new CitizenshipInfo()
            .withCitizenshipType(citizenshipTypeRequest.getType());
        applicant.setCitizenship(citizenshipInfo);

        journeyMapper.write(applicant, null, null, caseKey);
        return new ActionResult<>(MAPPER.mapToDto(applicant));
    }
}
