package com.backbase.flow.onboarding.us.interaction.handler;

import static com.backbase.flow.onboarding.us.util.OnboardingMapper.MAPPER;

import com.backbase.flow.application.uso.casedata.AgreementInfo;
import com.backbase.flow.application.uso.casedata.AgreementInfo.PolicyType;
import com.backbase.flow.application.uso.casedata.Applicant;
import com.backbase.flow.casedata.mapper.JourneyMapper;
import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.onboarding.us.interaction.dto.OnboardingDto;
import com.backbase.flow.onboarding.us.interaction.dto.PoliciesAgreementDto;
import com.backbase.flow.utils.CaseDataUtils;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("agree-to-terms")
public class PoliciesAgreementsHandler extends ActionHandlerWithValidation<PoliciesAgreementDto, OnboardingDto> {

    private final CaseDataUtils caseDataUtils;
    private final JourneyMapper<Applicant> journeyMapper;

    @Override
    protected ActionResult<OnboardingDto> handleWithValidation(final PoliciesAgreementDto policiesAgreement,
        final InteractionContext context) {

        final UUID caseKey = caseDataUtils.getOrCreateCaseKey(context);
        final Applicant applicant = journeyMapper.read(null, null, caseKey.toString());

        List<AgreementInfo> agreements = applicant.getAgreements();
        for (PolicyType policyType : PolicyType.values()) {
            agreeToPolicy(policyType, policiesAgreement.isAgreedToTerms(), agreements);
        }

        sendDataToCbs(context.getCaseKey());

        journeyMapper.write(applicant, null, null, caseKey.toString());

        return new ActionResult<>(MAPPER.mapToDto(applicant));
    }

    private void agreeToPolicy(PolicyType policyType, boolean hasAgreed, List<AgreementInfo> agreements) {
        AgreementInfo agreement = getExistingOrCreateNewAgreement(policyType, agreements);

        agreement.getAcceptedAt().add(OffsetDateTime.now());
        agreement.setAccepted(hasAgreed);
    }

    private AgreementInfo getExistingOrCreateNewAgreement(PolicyType policyType, List<AgreementInfo> agreements) {
        Optional<AgreementInfo> any = agreements.stream()
            .filter(agreement -> policyType.equals(agreement.getPolicyType())).findAny();
        if (any.isPresent()) {
            return any.get();
        }
        AgreementInfo agreement = new AgreementInfo()
            .withPolicyType(policyType);
        agreements.add(agreement);
        return agreement;
    }

    // TODO: integration with CBS
    private void sendDataToCbs(final UUID caseKey) {
        log.info("Sending case data to Core Banking System for {}", caseKey);
    }
}
