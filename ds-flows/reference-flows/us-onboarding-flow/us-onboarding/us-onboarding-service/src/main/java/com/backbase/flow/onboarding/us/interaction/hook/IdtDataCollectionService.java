package com.backbase.flow.onboarding.us.interaction.hook;

import com.backbase.flow.application.uso.casedata.Onboarding;
import com.backbase.flow.casedata.mapper.JourneyMapper;
import com.backbase.flow.onboarding.us.util.OnboardingCaseDataUtils;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IdtDataCollectionService {

    private final JourneyMapper<Onboarding> onboardingMapper;

    public boolean addressIdtDataCollected(UUID caseKey) {
        final Onboarding caseData = getOnboarding(caseKey);
        return OnboardingCaseDataUtils.getApplicant(caseData).getAddress() != null;
    }

    private Onboarding getOnboarding(UUID caseKey) {
        return onboardingMapper.read(null, null, caseKey.toString());
    }
}
