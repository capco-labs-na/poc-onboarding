package com.backbase.flow.onboarding.us.util;

import com.backbase.flow.application.uso.casedata.Applicant;
import com.backbase.flow.application.uso.casedata.CitizenshipInfo;
import com.backbase.flow.application.uso.casedata.Onboarding;
import com.backbase.flow.onboarding.us.interaction.dto.OnboardingDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.CredentialsResponseDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.FetchCitizenshipDataResponseDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.FetchCoApplicantDataResponseDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.FetchCustomerDataResponseDto;
import com.backbase.flow.onboarding.us.process.dto.ReviewClientRejectedDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OnboardingMapper {

    OnboardingMapper MAPPER = Mappers.getMapper(OnboardingMapper.class);

    default OnboardingDto mapToDto(Applicant applicant) {
        return mapToDto(applicant, null, null);
    }

    default OnboardingDto mapToDto(Applicant applicant, Boolean isJointAccount) {
        return mapToDto(applicant, null, isJointAccount);
    }

    @Mapping(source = "applicant.citizenship.citizenshipType", target = "citizenshipType")
    @Mapping(source = "applicant.citizenship.ssn", target = "ssn")
    @Mapping(source = "applicant.citizenship.citizenshipReview", target = "citizenshipReview")
    OnboardingDto mapToDto(Applicant applicant, Boolean isMainApplicantFlow, Boolean isJointAccount);

    @Mapping(target = "kycInformation", source = "kycInformation.answers")
    ReviewClientRejectedDto mapForUserTask(Applicant applicant);

    FetchCustomerDataResponseDto mapToLoginResponseDto(Applicant applicant, Boolean isMainApplicant);

    CredentialsResponseDto mapToCredentialsResponseDto(Applicant applicant, Boolean isJointAccount,
        Boolean isMainApplicantFlow);

    FetchCoApplicantDataResponseDto mapToFetchCoApplicantDataResponseDto(Onboarding onboarding);

    @Mapping(source = "citizenship.citizenshipType", target = "type")
    @Mapping(source = "citizenship.ssn", target = "ssn")
    @Mapping(source = "citizenship", target = "nonResident")
    @Mapping(source = "citizenship.nonResident.citizenshipCountryCode", target = "nonResident.citizenshipCountryCode")
    @Mapping(source = "citizenship.nonResident.residencyAddress", target = "nonResident.residencyAddress")
    @Mapping(source = "citizenship.w8ben.accepted", target = "nonResident.w8benAccepted")
    FetchCitizenshipDataResponseDto mapToCitizenshipDto(Applicant applicant);

    @AfterMapping
    default void afterMapToCitizenshipDto(Applicant source, @MappingTarget FetchCitizenshipDataResponseDto target) {
        CitizenshipInfo citizenship = source.getCitizenship();

        // Preventing creation of empty nonResident object
        if (citizenship == null || citizenship.getNonResident() == null) {
            target.setNonResident(null);
            return;
        }

        // Workaround for generating sources through intellij
        target.getNonResident().setWithholdingTaxAccepted(citizenship.getWithholdingTaxAccepted());
    }
}
