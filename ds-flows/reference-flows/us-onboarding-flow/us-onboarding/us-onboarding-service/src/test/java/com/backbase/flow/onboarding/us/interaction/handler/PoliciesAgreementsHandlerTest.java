package com.backbase.flow.onboarding.us.interaction.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.backbase.buildingblocks.presentation.errors.Error;
import com.backbase.flow.application.uso.casedata.AgreementInfo;
import com.backbase.flow.application.uso.casedata.AgreementInfo.PolicyType;
import com.backbase.flow.application.uso.casedata.Applicant;
import com.backbase.flow.application.uso.casedata.Onboarding;
import com.backbase.flow.casedata.CaseDataService;
import com.backbase.flow.casedata.cases.Case;
import com.backbase.flow.casedata.mapper.JourneyMapper;
import com.backbase.flow.casedata.mapper.JourneyReader;
import com.backbase.flow.casedata.mapper.JourneyWriter;
import com.backbase.flow.interaction.engine.action.ActionResult;
import com.backbase.flow.interaction.engine.action.ErrorCodes;
import com.backbase.flow.interaction.engine.action.InteractionContext;
import com.backbase.flow.onboarding.us.interaction.dto.OnboardingDto;
import com.backbase.flow.onboarding.us.interaction.dto.PoliciesAgreementDto;
import com.backbase.flow.onboarding.us.mapper.applicant.CoApplicantAwareApplicantReader;
import com.backbase.flow.onboarding.us.mapper.applicant.CoApplicantAwareApplicantWriter;
import com.backbase.flow.utils.CaseDataUtils;
import com.google.common.collect.Lists;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import org.assertj.core.api.Condition;
import org.camunda.bpm.engine.runtime.MessageCorrelationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

class PoliciesAgreementsHandlerTest {

    private final MessageCorrelationBuilder messageCorrelationBuilder = mock(MessageCorrelationBuilder.class);
    private final InteractionContext context = mock(InteractionContext.class);
    private final CaseDataService caseDataService = mock(CaseDataService.class);

    private final CaseDataUtils caseDataUtils = new CaseDataUtils();
    private final JourneyReader<Applicant> journeyReader = new CoApplicantAwareApplicantReader();
    private final JourneyWriter<Applicant> journeyWriter = new CoApplicantAwareApplicantWriter(caseDataService);
    private final JourneyMapper<Applicant> journeyMapper = spy(new JourneyMapper<>(Applicant.class,
        List.of(journeyReader), List.of(journeyWriter), caseDataService));

    private final PoliciesAgreementsHandler policiesAgreementsHandler = new PoliciesAgreementsHandler(caseDataUtils,
        journeyMapper);

    @BeforeEach
    private void setup() {
        when(caseDataService.updateCase(any(Case.class)))
            .thenAnswer(a -> a.getArgument(0));
    }

    @Test
    void handleWorks() {
        final UUID caseKey = UUID.randomUUID();
        final PoliciesAgreementDto policiesAgreementDto = new PoliciesAgreementDto(true);
        final Onboarding caseData = new Onboarding().withMainApplicant(new Applicant()).withIsMainApplicantFlow(true);
        final Case sampleCase = new Case()
            .setKey(caseKey)
            .setCaseData(caseData);

        when(messageCorrelationBuilder.processInstanceVariableEquals(any(), any()))
            .thenReturn(messageCorrelationBuilder);
        when(context.getCaseKey()).thenReturn(caseKey);
        when(caseDataService.getCaseByKey(caseKey)).thenReturn(sampleCase);

        final ActionResult<OnboardingDto> actionResult = policiesAgreementsHandler
            .handle(policiesAgreementDto, context);

        assertThat(actionResult.isSuccessful()).isTrue();
        assertThat(actionResult.getErrors()).isEmpty();
        final var onboardingArgumentCaptor = ArgumentCaptor.forClass(Applicant.class);
        verify(journeyMapper).write(onboardingArgumentCaptor.capture(), isNull(), isNull(), anyString());
        Applicant capturedCaseData = onboardingArgumentCaptor.getValue();

        Condition<AgreementInfo> signedPrivacyPolicy = new Condition<>(
            getSignedAgreementPredicate(PolicyType.PRIVACY_POLICY),
            "Signed Privacy Policy");
        Condition<AgreementInfo> signedTermsAndConditions = new Condition<>(
            getSignedAgreementPredicate(PolicyType.TERMS_AND_CONDITIONS),
            "Signed Terms and Conditions");

        assertThat(capturedCaseData.getAgreements().size()).isEqualTo(PolicyType.values().length);
        assertThat(capturedCaseData.getAgreements()).haveExactly(1, signedPrivacyPolicy);
        assertThat(capturedCaseData.getAgreements()).haveExactly(1, signedTermsAndConditions);
    }

    private Predicate<AgreementInfo> getSignedAgreementPredicate(PolicyType policyType) {
        return agreementInfo -> policyType.equals(agreementInfo.getPolicyType())
            && agreementInfo.getAccepted()
            && agreementInfo.getAcceptedAt().size() == 1;
    }

    @Test
    void handleValidationWorks() {
        final PoliciesAgreementDto policiesAgreementDto = new PoliciesAgreementDto(false);

        final ActionResult<OnboardingDto> actionResult = policiesAgreementsHandler
            .handle(policiesAgreementDto, context);

        final List<Error> errors = actionResult.getErrors();
        assertThat(errors).isNotEmpty();
        assertThat(errors.size()).isEqualTo(1);
        assertThat(errors.get(0).getKey()).isEqualTo(ErrorCodes.FLOW_001.getKey());
        assertThat(errors.get(0).getMessage()).isEqualTo(ErrorCodes.FLOW_001.getMessage());
        assertThat(errors.get(0).getContext()).containsEntry("agreedToTerms", "must be true");
    }

    @Test
    void handlePoliciesAcceptedMultipleTimes() {
        final var caseKey = UUID.randomUUID();
        final PoliciesAgreementDto policiesAgreementDto = new PoliciesAgreementDto(true);
        final AgreementInfo privacyAgreementInfo = new AgreementInfo()
            .withAccepted(true)
            .withPolicyType(PolicyType.PRIVACY_POLICY)
            .withAcceptedAt(Lists.newArrayList(OffsetDateTime.now().minusHours(2)));
        final AgreementInfo tecAgreementInfo = new AgreementInfo()
            .withAccepted(true)
            .withPolicyType(PolicyType.TERMS_AND_CONDITIONS)
            .withAcceptedAt(Lists.newArrayList(OffsetDateTime.now().minusHours(2)));

        final Onboarding caseData = new Onboarding()
            .withMainApplicant(new Applicant()
                .withAgreements(Lists.newArrayList(privacyAgreementInfo, tecAgreementInfo)))
            .withIsMainApplicantFlow(true);
        final Case sampleCase = new Case()
            .setKey(caseKey)
            .setCaseData(caseData);

        when(context.getCaseKey()).thenReturn(caseKey);
        when(context.getCaseKey()).thenReturn(caseKey);
        when(caseDataService.getCaseByKey(caseKey)).thenReturn(sampleCase);

        final ActionResult<OnboardingDto> actionResult = policiesAgreementsHandler
            .handle(policiesAgreementDto, context);

        assertThat(actionResult.getErrors()).isEmpty();
        final var onboardingArgumentCaptor = ArgumentCaptor.forClass(Applicant.class);
        verify(journeyMapper).write(onboardingArgumentCaptor.capture(), isNull(), isNull(), anyString());

        assertThat(onboardingArgumentCaptor.getValue().getAgreements().get(0).getAcceptedAt()).hasSize(2);
        assertThat(onboardingArgumentCaptor.getValue().getAgreements().get(1).getAcceptedAt()).hasSize(2);
    }
}
