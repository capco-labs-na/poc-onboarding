package com.backbase.flow.onboarding.us.util;

import static org.assertj.core.api.Assertions.assertThat;

import com.backbase.flow.application.uso.casedata.Address;
import com.backbase.flow.application.uso.casedata.Applicant;
import com.backbase.flow.application.uso.casedata.CitizenshipInfo;
import com.backbase.flow.application.uso.casedata.CitizenshipInfo.CitizenshipType;
import com.backbase.flow.application.uso.casedata.NonResidentInfo;
import com.backbase.flow.application.uso.casedata.ResidencyAddress;
import com.backbase.flow.application.uso.casedata.W8ben;
import com.backbase.flow.onboarding.us.interaction.dto.NonResidentDataDto;
import com.backbase.flow.onboarding.us.interaction.dto.ResidencyAddressDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.FetchCitizenshipDataResponseDto;
import com.backbase.flow.onboarding.us.interaction.dto.response.FetchCustomerDataResponseDto;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;

class OnboardingMapperTest {

    private final OnboardingMapper mapper = OnboardingMapper.MAPPER;

    @Test
    void mapToLoginResponseDto_addressExists_addressIsMapped() {
        Applicant applicant = sampleOnboardingWithAddress();

        FetchCustomerDataResponseDto.Address expectedAddress = new FetchCustomerDataResponseDto.Address();
        expectedAddress.setNumberAndStreet("Number and Street");
        expectedAddress.setZipCode("Zip Code");
        expectedAddress.setCity("City");
        expectedAddress.setState("State");
        expectedAddress.setApt("Apartment");
        FetchCustomerDataResponseDto expectedResult = new FetchCustomerDataResponseDto();
        expectedResult.setAddress(expectedAddress);
        expectedResult.setIsMainApplicant(true);

        FetchCustomerDataResponseDto actualResult = mapper.mapToLoginResponseDto(applicant, true);

        assertThat(actualResult).isEqualTo(expectedResult);
    }

    private Applicant sampleOnboardingWithAddress() {
        final Address address = new Address();
        address.setNumberAndStreet("Number and Street");
        address.setZipCode("Zip Code");
        address.setCity("City");
        address.setState("State");
        address.setApt("Apartment");
        final Applicant applicant = new Applicant();
        applicant.setAddress(address);
        return applicant;
    }

    @Test
    void mapToCitizenshipDtoApplicantWithEmptyCitizenship() {
        FetchCitizenshipDataResponseDto fetchCitizenshipDataResponseDto = mapper.mapToCitizenshipDto(new Applicant());

        assertThat(fetchCitizenshipDataResponseDto.getType()).isNull();
        assertThat(fetchCitizenshipDataResponseDto.getSsn()).isNull();
        assertThat(fetchCitizenshipDataResponseDto.getNonResident()).isNull();
    }

    @Test
    void mapToCitizenshipDtoUsCitizen() {
        FetchCitizenshipDataResponseDto fetchCitizenshipDataResponseDto = mapper.mapToCitizenshipDto(
            new Applicant()
                .withCitizenship(new CitizenshipInfo()
                    .withCitizenshipType(CitizenshipType.US_CITIZEN)
                    .withSsn("123456789")));

        assertThat(fetchCitizenshipDataResponseDto.getType()).isEqualTo(CitizenshipType.US_CITIZEN);
        assertThat(fetchCitizenshipDataResponseDto.getSsn()).isEqualTo("123456789");
        assertThat(fetchCitizenshipDataResponseDto.getNonResident()).isNull();
    }

    @Test
    void mapToCitizenshipDtoNonResidentAlien() {
        Applicant toMap = new Applicant()
            .withCitizenship(new CitizenshipInfo()
                .withCitizenshipType(CitizenshipType.NON_RESIDENT_ALIEN)
                .withNonResident(new NonResidentInfo()
                    .withCitizenshipCountryCode("Citizenship country code")
                    .withResidencyAddress(new ResidencyAddress()
                        .withCountryCode("Residency country code")
                        .withCity("City")
                        .withZipCode("Zip code")
                        .withNumberAndStreet("Number and street")
                    )
                )
                .withForeignTin("Foreign tin")
                .withNationalTin("National tin")
                .withWithholdingTaxAccepted(true)
                .withW8ben(new W8ben().withAccepted(true).withAcceptedAt(OffsetDateTime.now()))
            );

        FetchCitizenshipDataResponseDto fetchCitizenshipDataResponseDto = mapper.mapToCitizenshipDto(toMap);

        assertThat(fetchCitizenshipDataResponseDto.getType()).isEqualTo(CitizenshipType.NON_RESIDENT_ALIEN);
        assertThat(fetchCitizenshipDataResponseDto.getSsn()).isNull();

        NonResidentDataDto nonResident = fetchCitizenshipDataResponseDto.getNonResident();
        assertThat(nonResident).isNotNull();
        assertThat(nonResident.getCitizenshipCountryCode()).isEqualTo("Citizenship country code");

        ResidencyAddressDto residencyAddress = nonResident.getResidencyAddress();
        assertThat(residencyAddress).isNotNull();
        assertThat(residencyAddress.getCountryCode()).isEqualTo("Residency country code");
        assertThat(residencyAddress.getCity()).isEqualTo("City");
        assertThat(residencyAddress.getZipCode()).isEqualTo("Zip code");
        assertThat(residencyAddress.getNumberAndStreet()).isEqualTo("Number and street");

        assertThat(nonResident.getForeignTin()).isEqualTo("Foreign tin");
        assertThat(nonResident.getNationalTin()).isEqualTo("National tin");
        assertThat(nonResident.getWithholdingTaxAccepted()).isTrue();
        assertThat(nonResident.getW8benAccepted()).isTrue();
    }

    @Test
    void mapToCitizenshipDtoEmptyResult() {
        FetchCitizenshipDataResponseDto fetchCitizenshipDataResponseDto = mapper.mapToCitizenshipDto(null);
        assertThat(fetchCitizenshipDataResponseDto).isNull();
    }

}
